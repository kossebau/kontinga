/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KCMKONTINGA_PROPERTYSERVICESCONTROL_HPP
#define KCMKONTINGA_PROPERTYSERVICESCONTROL_HPP

// KF
#include <KService>
// Qt
#include <QTabWidget>

class QStringList;
class KPropertyActionListControl;
class KPropertyDataActionListControl;
class KPropertyStatusListControl;

class KPropertyServicesControl : public QTabWidget
{
    Q_OBJECT

public:
    explicit KPropertyServicesControl(QWidget* parent = nullptr);
    ~KPropertyServicesControl() override;

public:
    void setProperty(const QString& propertyId);

public:
    QString propertyId() const;
    KPropertyActionListControl* actionControl() const;
    KPropertyDataActionListControl* dataActionControl() const;
    KPropertyStatusListControl* statusControl() const;

Q_SIGNALS:
    void changed(bool);
    void configChanged(int serviceTypeId, const QString& serviceId);

private:
    KPropertyActionListControl* m_actionControl;
    KPropertyDataActionListControl* m_dataActionControl;
    KPropertyStatusListControl* m_statusControl;

    QString m_propertyId;
};

inline QString KPropertyServicesControl::propertyId() const { return m_propertyId; }
inline KPropertyActionListControl* KPropertyServicesControl::actionControl() const { return m_actionControl; }
inline KPropertyDataActionListControl* KPropertyServicesControl::dataActionControl() const { return m_dataActionControl; }
inline KPropertyStatusListControl* KPropertyServicesControl::statusControl() const { return m_statusControl; }

#endif
