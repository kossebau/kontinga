/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "kserviceselectdialog.hpp"

// module
#include "kservicelistitem.hpp"
// KF
#include <KLocalizedString>
// Qt
#include <QVBoxLayout>
#include <QLabel>
#include <QDialogButtonBox>

KServiceSelectDialog::KServiceSelectDialog(const QString& titel, const QString& text, QWidget* parent)
    : QDialog(parent)
{
    setWindowTitle(titel);

    auto* formLayout = new QVBoxLayout();

    auto label = new QLabel(text, this);
    formLayout->addWidget(label);

    m_listBox = new QListWidget(this);
    m_listBox->setMinimumHeight(350);
    m_listBox->setMinimumWidth(300);
    connect(m_listBox, &QListWidget::doubleClicked, this, &QDialog::accept);
    formLayout->addWidget(m_listBox, 1);

    // dialog buttons
    auto dialogButtonBox = new QDialogButtonBox(this);
    dialogButtonBox->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(dialogButtonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(dialogButtonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    // main layout
    auto* layout = new QVBoxLayout;
    layout->addLayout(formLayout);
    layout->addStretch();
    layout->addWidget(dialogButtonBox);

    setLayout(layout);
}

KServiceSelectDialog::~KServiceSelectDialog() = default;

void KServiceSelectDialog::setServices(const KService::List& allServices)
{
    m_listBox->clear();

    for (const auto& service : allServices) {
        m_listBox->addItem(new KServiceListItem(service));
    }

//     m_listBox->sort();
}

KService::Ptr KServiceSelectDialog::service() const
{
    auto* item = static_cast<KServiceListItem*>(m_listBox->currentItem());
    return item->service();
}
