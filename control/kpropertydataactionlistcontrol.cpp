/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "kpropertydataactionlistcontrol.hpp"

// module
#include "kservicelistitem.hpp"
#include "kserviceselectdialog.hpp"
// KF
#include <KLocalizedString>
// Qt
#include <QListWidgetItem>
#include <QPushButton>
#include <QToolTip>
#include <QWhatsThis>

static inline QString serviceId(const KService::Ptr& service)
{
    return service->property(QString::fromLatin1("X-KDE-DataActionService")).toString();
}
static inline bool hasCategories2(const KService::Ptr& service)
{
    return !service->property(QString::fromLatin1("X-KDE-Categories")).toStringList().isEmpty();
}

KPropertyDataActionListControl::KPropertyDataActionListControl(QWidget* parent)
    : KSortedListBox(parent, Add | UpDown | Action1 | Action2 | Action3 | Remove)
{
    setButtonAction1(i18n("Global"));
    setButtonAction2(i18n("Local"));
    setButtonAction3(i18n("Configure..."));

    connect(m_listBox, SIGNAL(highlighted(int)), SLOT(onItemHighlighted(int)));

    const QString description = i18n(
        " This is a list of action services on data associated with the selected property."
        " This list is e.g. shown in a property's menu for some dropped data."
        " Set an action to \"Global\" if it should also be included"
        " in the entire person's menu for dropped data.");

//     QWhatsThis::add( this, description );
//     QWhatsThis::add( m_listBox, description );

//     QToolTip::add( m_newButton,     i18n( "Adds a new action.") );
//     QToolTip::add( m_upButton,      i18n( "Moves the current action up in the list.") );
//     QToolTip::add( m_downButton,    i18n( "Moves the current action down in the list.") );
//     QToolTip::add( m_action1Button, i18n( "Marks the current action to appear also in menus on the entire person.") );
//     QToolTip::add( m_action2Button, i18n( "Marks the current action to appear only in menus on the property.") );
//     QToolTip::add( m_removeButton,  i18n( "Removes the current action from the list.") );
}

KPropertyDataActionListControl::~KPropertyDataActionListControl() = default;

// TODO: to be optimized
void KPropertyDataActionListControl::setDataActionServices(const KService::List& services,
                                                           const QStringList& sortedIds, const QStringList& hiddenIds,
                                                           const QStringList& mainIds)
{
    m_hiddenDataActionServices = services;

    QVector<QListWidgetItem*> itemList;

    // add sorted items
    for (const auto& sortedActionId : sortedIds) {
        const auto endIt = m_hiddenDataActionServices.end();
        for (auto it = m_hiddenDataActionServices.begin(); it != endIt; ++it) {
            const QString actionId = serviceId(*it);
            if (actionId == sortedActionId) {
                itemList.append(new KServiceListItem(*it, mainIds.contains(actionId)));
                m_hiddenDataActionServices.erase(it);
                break;
            }
        }
    }

    // add unsorted items
    for (auto it = m_hiddenDataActionServices.begin(); it != m_hiddenDataActionServices.end();) {
        const QString actionId = serviceId(*it);
        // not hidden?
        if (!hiddenIds.contains(actionId)) {
            itemList.append(new KServiceListItem(*it, hasCategories2(*it)));
            it = m_hiddenDataActionServices.erase(it);
        } else {
            ++it;
        }
    }

    setItemList(itemList);

    m_newButton->setEnabled(!m_hiddenDataActionServices.isEmpty());
}

QStringList KPropertyDataActionListControl::sortedDataActionIds() const
{
    QStringList result;
    const unsigned int count = this->count();

    result.reserve(count);
    for (unsigned int i = 0; i < count; ++i) {
        auto* item = static_cast<KServiceListItem*>(m_listBox->item(i));
        result.append(serviceId(item->service()));
    }

    return result;
}

QStringList KPropertyDataActionListControl::hiddenDataActionIds() const
{
    QStringList result;

    result.reserve(m_hiddenDataActionServices.size());
    for (const auto& service : m_hiddenDataActionServices) {
        result.append(serviceId(service));
    }

    return result;
}

QStringList KPropertyDataActionListControl::mainDataActionIds() const
{
    QStringList result;
    const unsigned int count = this->count();

    for (unsigned int i = 0; i < count; ++i) {
        auto* item = static_cast<KServiceListItem*>(m_listBox->item(i));
        if (item->isMain()) {
            result.append(serviceId(item->service()));
        }
    }

    return result;
}

void KPropertyDataActionListControl::onItemHighlighted(int index)
{
    auto* item = static_cast<KServiceListItem*>(m_listBox->item(index));

    const bool isMain = item->isMain();
    m_action1Button->setEnabled(!isMain);
    m_action2Button->setEnabled(isMain);

    emit selected(serviceId(item->service()));
}

QListWidgetItem* KPropertyDataActionListControl::createItem()
{
    KService::Ptr service(nullptr);
    KServiceSelectDialog dialog(i18n("Action on Data Selection"), i18n("Select an action:"), this);
    dialog.setServices(m_hiddenDataActionServices);
    if (dialog.exec() != QDialog::Accepted) {
        return nullptr;
    }
    service = dialog.service();

    if (!service) {
        return nullptr;
    }

    m_hiddenDataActionServices.removeOne(service);

    return new KServiceListItem(service);
}

bool KPropertyDataActionListControl::deleteItem(QListWidgetItem* item)
{
    bool result = true;

    KServiceListItem* Item = static_cast<KServiceListItem*>(item);
    m_hiddenDataActionServices.append(Item->service());
    m_newButton->setEnabled(true);

    return result;
}

bool KPropertyDataActionListControl::action1Item(QListWidgetItem* item)
{
    bool result = true;

    KServiceListItem* serviceItem = static_cast<KServiceListItem*>(item);
    serviceItem->setMain(true);
    m_action1Button->setEnabled(false);
    m_action2Button->setEnabled(true);
    m_action3Button->setEnabled(serviceItem->isConfigurable());

    return result;
}

bool KPropertyDataActionListControl::action2Item(QListWidgetItem* item)
{
    bool result = true;

    auto* serviceItem = static_cast<KServiceListItem*>(item);
    serviceItem->setMain(false);
    m_action1Button->setEnabled(true);
    m_action2Button->setEnabled(false);

    return result;
}

bool KPropertyDataActionListControl::action3Item(QListWidgetItem* item)
{
    bool result = false;

    auto* serviceItem = static_cast<KServiceListItem*>(item);
    if (serviceItem->configure(this)) {
        emit configChanged(DataActionServiceId, serviceId(serviceItem->service()));
    }

    return result;
}
