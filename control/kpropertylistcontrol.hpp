/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KCMKONTINGA_PROPERTYLISTCONTROL_HPP
#define KCMKONTINGA_PROPERTYLISTCONTROL_HPP

// lib
#include <ksortedlistbox.hpp>
// KF
#include <KService>

class QStringList;

class KPropertyListControl : public KSortedListBox
{
    Q_OBJECT

public:
    explicit KPropertyListControl(QWidget* parent = nullptr);
    ~KPropertyListControl() override;

public:
    void setProperties(const KService::List& propertyServices,
                       const QStringList& sortedPropertyIds, const QStringList& hiddenPropertyIds);

public:
    QStringList sortedPropertyIds() const;
    QStringList hiddenPropertyIds() const;

Q_SIGNALS:
    void selected(const QString& propertyId);

protected: // KSortedListBox API
    QListWidgetItem* createItem() override;
    bool action1Item(QListWidgetItem* item) override;
    bool deleteItem(QListWidgetItem* item) override;

private Q_SLOTS:
    void onItemHighlighted(int index);

private:
    KService::List m_hiddenPropertyServices;
};

#endif
