/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "kstatuslistcontrol.hpp"

// module
#include "kservicelistitem.hpp"
#include "kserviceselectdialog.hpp"
// KF
#include <KLocalizedString>
// Qt
#include <QListWidgetItem>
#include <QPushButton>
#include <QToolTip>
#include <QWhatsThis>

static inline QString serviceId(const KService::Ptr& service)
{
    return service->property(QString::fromLatin1("X-KDE-StatusService")).toString();
}

KStatusListControl::KStatusListControl(QWidget* parent)
    : KSortedListBox(parent, Add | UpDown | Action1 | Remove)
{
    setButtonAction1(i18n("Configure..."));

    connect(m_listBox, SIGNAL(highlighted(int)), SLOT(onItemHighlighted(int)));

    const QString description = i18n(
        " This is a list of status services associated with entire persons.");

//     QWhatsThis::add( this, description );
//     QWhatsThis::add( m_listBox, description );

//     QToolTip::add( m_newButton,    i18n( "Adds a new status.") );
//     QToolTip::add( m_upButton,     i18n( "Moves the current status up in the list.") );
//     QToolTip::add( m_downButton,   i18n( "Moves the current status down in the list.") );
//     QToolTip::add( m_removeButton, i18n( "Removes the current status from the list.") );
}

KStatusListControl::~KStatusListControl() = default;

// TODO: to be optimized
void KStatusListControl::setServices(const KService::List& services,
                                     const QStringList& sortedIds, const QStringList& hiddenIds)
{
    m_hiddenServices = services;

    QVector<QListWidgetItem*> itemList;

    // add sorted items
    for (const auto& sortedId : sortedIds) {
        const auto endIt = m_hiddenServices.end();
        for (auto it = m_hiddenServices.begin(); it != endIt; ++it) {
            const QString statusId = serviceId(*it);
            if (statusId == sortedId) {
                itemList.append(new KServiceListItem(*it));
                m_hiddenServices.erase(it);
                break;
            }
        }
    }

    // add unsorted items
    for (auto it = m_hiddenServices.begin(); it != m_hiddenServices.end();) {
        const QString statusId = serviceId(*it);
        // not hidden?
        if (!hiddenIds.contains(statusId)) {
            itemList.append(new KServiceListItem(*it));
            it = m_hiddenServices.erase(it);
        } else {
            ++it;
        }
    }

    setItemList(itemList);

    m_newButton->setEnabled(!m_hiddenServices.isEmpty());
}

QStringList KStatusListControl::sortedIds() const
{
    QStringList result;
    const unsigned int count = this->count();

    result.reserve(count);
    for (unsigned int i = 0; i < count; ++i) {
        auto* item = static_cast<KServiceListItem*>(m_listBox->item(i));
        result.append(serviceId(item->service()));
    }

    return result;
}

QStringList KStatusListControl::hiddenIds() const
{
    QStringList result;

    result.reserve(m_hiddenServices.size());
    for (const auto& service : m_hiddenServices) {
        result.append(serviceId(service));
    }

    return result;
}

void KStatusListControl::onItemHighlighted(int Index)
{
    auto* item = static_cast<KServiceListItem*>(m_listBox->item(Index));

    m_action1Button->setEnabled(item->isConfigurable());

    emit selected(serviceId(item->service()));
}

QListWidgetItem* KStatusListControl::createItem()
{
    KService::Ptr service(nullptr);
    KServiceSelectDialog dialog(i18n("Status Selection"), i18n("Select a status:"), this);
    dialog.setServices(m_hiddenServices);
    if (dialog.exec() != QDialog::Accepted) {
        return nullptr;
    }
    service = dialog.service();

    if (!service) {
        return nullptr;
    }

    m_hiddenServices.removeOne(service);

    return new KServiceListItem(service);
}

bool KStatusListControl::action1Item(QListWidgetItem* item)
{
    bool result = false;

    auto* serviceItem = static_cast<KServiceListItem*>(item);
    if (serviceItem->configure(this)) {
        emit configChanged(StatusServiceId, serviceId(serviceItem->service()));
    }

    return result;
}

bool KStatusListControl::deleteItem(QListWidgetItem* item)
{
    bool result = true;

    auto* serviceItem = static_cast<KServiceListItem*>(item);
    m_hiddenServices.append(serviceItem->service());
    m_newButton->setEnabled(true);

    return result;
}
