/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "kservicescontrol.hpp"

// module
#include "kactionlistcontrol.hpp"
#include "kdataactionlistcontrol.hpp"
#include "kstatuslistcontrol.hpp"
// KF
#include <KLocalizedString>

KServicesControl::KServicesControl(QWidget* Parent)
    : QTabWidget(Parent)
{
    m_actionControl = new KActionListControl(this);
    addTab(m_actionControl, i18n("&Actions"));
    connect(m_actionControl, &KSortedListBox::changed,
            this, &KServicesControl::changed);
    connect(m_actionControl, &KActionListControl::configChanged,
            this, &KServicesControl::configChanged);

    m_dataActionControl = new KDataActionListControl(this);
    addTab(m_dataActionControl, i18n("&Actions on Data"));
    connect(m_dataActionControl, &KSortedListBox::changed,
            this, &KServicesControl::changed);
    connect(m_dataActionControl, &KDataActionListControl::configChanged,
            this, &KServicesControl::configChanged);

    m_statusControl = new KStatusListControl(this);
    addTab(m_statusControl, i18n("&States"));
    connect(m_statusControl, &KSortedListBox::changed,
            this, &KServicesControl::changed);
    connect(m_statusControl, &KStatusListControl::configChanged,
            this, &KServicesControl::configChanged);
}

KServicesControl::~KServicesControl() = default;
