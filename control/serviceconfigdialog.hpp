/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KCMKONTINGA_SERVICECONFIGDIALOG_HPP
#define KCMKONTINGA_SERVICECONFIGDIALOG_HPP

// Qt
#include <QDialog>

class QDialogButtonBox;
class QVBoxLayout;

class ServiceConfigDialog : public QDialog
{
    Q_OBJECT

public:
    ServiceConfigDialog(QWidget* parent, const QString& serviceName);

public:
    bool isDirty() const;
    void setConfigWidget(QWidget* configWidget);

private Q_SLOTS:
    void setDirty(bool dirty);

private:
    QVBoxLayout* m_mainWidgetLayout;
    QDialogButtonBox* m_dialogButtonBox;
    bool m_dirty;
};

#endif
