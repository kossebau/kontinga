/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "kcmkontinga.hpp"

// module
#include "aboutdata.hpp"
#include "kactionlistcontrol.hpp"
#include "kdataactionlistcontrol.hpp"
#include "kstatuslistcontrol.hpp"
#include "kservicescontrol.hpp"
#include "kpropertyactionlistcontrol.hpp"
#include "kpropertydataactionlistcontrol.hpp"
#include "kpropertystatuslistcontrol.hpp"
#include "kpropertylistcontrol.hpp"
#include "kpropertyservicescontrol.hpp"
// KF
#include <KLocalizedString>
#include <KSharedConfig>
#include <KConfigGroup>
#include <KServiceTypeTrader>
#include <KService>
#include <KSycoca>
// Qt
#include <QTimer>
#include <QHBoxLayout>
#include <QStackedWidget>
#include <QLabel>

static const char DCOPConfigObjectName[] = "KontingaConfig";
static const char DCOPChangeSignalName[] = "changed()";
static const char DCOPServiceConfigChangeSignalName[] = "changed(int,QString,QString)";

static const char ConfigFileName[] = "kontingarc";

static const char GeneralGroupId[] = "General";

static const char PropertiesGroupId[] =   "Properties";
static const char PropertiesSortingId[] = "Sorting";
static const char PropertiesHiddenId[] =  "Hidden";

static const char PropertyGroupIdTemplate[] = "Property:%1";

static const char ActionsSortingId[] =        "ActionsSorting";
static const char ActionsHiddenId[] =         "HiddenActions";
static const char ActionsMainId[] =           "MainActions";
static const char DataActionsSortingId[] =    "DataActionsSorting";
static const char DataActionsHiddenId[] =     "HiddenDataActions";
static const char DataActionsMainId[] =       "MainDataActions";
static const char StatesSortingId[] =         "StatesSorting";
static const char StatesHiddenId[] =          "HiddenStates";

static const char ABIVersionId[] =            "X-KDE-ABI-Version";
static const char ServiceABIVersion[] = "1";

static void emitDCOPChangeSignal()
{
    // TODO dbus
//     kapp->dcopClient()->emitDCOPSignal( DCOPConfigObjectName, DCOPChangeSignalName, QByteArray() );
}

static void emitDCOPServiceConfigChangeSignal(int serviceType, const QString& propertyId, const QString& serviceId)
{
    Q_UNUSED(serviceType);
    Q_UNUSED(propertyId);
    Q_UNUSED(serviceId);
    // TODO dbus
//     QByteArray SignalData;
//     QDataStream SignalDataStream( SignalData, IO_WriteOnly );
//     SignalDataStream << ServiceType << PropertyId << ServiceId;
//     kapp->dcopClient()->emitDCOPSignal( DCOPConfigObjectName, DCOPServiceConfigChangeSignalName, SignalData );
}

KontingaControlModule::KontingaControlModule(QWidget* Parent, const QVariantList&)
    : KCModule(Parent)
    , m_actionServicesDirty(false)
    , m_dataActionServicesDirty(false)
    , m_statusServicesDirty(false)
    , m_propertiesDirty(false)
    , m_propertyActionServicesDirty(false)
    , m_propertyDataActionServicesDirty(false)
    , m_propertyStatusServicesDirty(false)
{
    auto* topLayout = new QHBoxLayout(this);  // TODO spacing 0?

    m_tabWidget = new QTabWidget(this);

    // General
    m_servicesControl = new KServicesControl(m_tabWidget);
    connect(m_servicesControl, SIGNAL(changed(bool)), SLOT(setServicesDirty(bool)));
    connect(m_servicesControl, SIGNAL(configChanged(int,const QString&)),
            SLOT(signalServiceConfigChange(int,const QString&)));
    m_tabWidget->addTab(m_servicesControl, i18n("&General"));

    // Properties
    auto* propertiesPage = new QWidget(m_tabWidget);
    m_tabWidget->addTab(propertiesPage, i18n("&Properties"));

    auto* propertiesLayout = new QHBoxLayout(propertiesPage);      // TODO spacing 0 );

    // left: list of properties
    m_propertyListControl = new KPropertyListControl(propertiesPage);
    connect(m_propertyListControl, SIGNAL(selected(const QString&)), SLOT(onPropertySelect(const QString&)));
    connect(m_propertyListControl, SIGNAL(changed(bool)), SLOT(setm_propertiesDirty(bool)));

    // right: certain things
    m_widgetStack = new QStackedWidget(propertiesPage);

    // Property Services
    m_propertyServicesControl = new KPropertyServicesControl(m_widgetStack);
    connect(m_propertyServicesControl, SIGNAL(changed(bool)), SLOT(setPropertyServicesDirty(bool)));
    connect(m_propertyServicesControl, SIGNAL(configChanged(int,const QString&)),
            SLOT(signalPropertyServiceConfigChange(int,const QString&)));

    // Widget shown on startup
    auto* startLabel = new QLabel(i18n("Select a property"), m_widgetStack);
    startLabel->setAlignment(Qt::AlignCenter);

    m_widgetStack->addWidget(startLabel);
    m_widgetStack->addWidget(m_propertyServicesControl);
    m_widgetStack->setCurrentWidget(startLabel);

    connect(KSycoca::self(), SIGNAL(databaseChanged()), SLOT(onDatabaseChange()));

    propertiesLayout->addWidget(m_propertyListControl);
    propertiesLayout->addWidget(m_widgetStack);
    propertiesLayout->setStretchFactor(m_widgetStack, 2);

    topLayout->addWidget(m_tabWidget);

    setAboutData(new KontingaControlModuleAboutData);
    setButtons(/*Default|*/ Apply /*|Help*/);

    load();
}

KontingaControlModule::~KontingaControlModule() = default;

void KontingaControlModule::load()
{
    const QString propertyConstraint = QString::fromLatin1("[X-KDE-ABI-Version] == '%1'")
                                       .arg(QString::fromLatin1(ServiceABIVersion));

    auto config = KSharedConfig::openConfig(ConfigFileName);

    auto generalConfig = config->group(GeneralGroupId);
    const QStringList sortedActionIds = generalConfig.readEntry(ActionsSortingId, QStringList());
    const QStringList hiddenActionIds = generalConfig.readEntry(ActionsHiddenId, QStringList());

    const KService::List actionOffers = KServiceTypeTrader::self()->query("kontinga/actionservice", propertyConstraint);

    m_servicesControl->actionControl()->setServices(actionOffers, sortedActionIds, hiddenActionIds);

    const QStringList sortedDataActionIds = generalConfig.readEntry(DataActionsSortingId, QStringList());
    const QStringList hiddenDataActionIds = generalConfig.readEntry(DataActionsHiddenId, QStringList());

    const KService::List dataActionOffers = KServiceTypeTrader::self()->query("kontinga/dataactionservice", propertyConstraint);

    m_servicesControl->dataActionControl()->setServices(dataActionOffers, sortedDataActionIds, hiddenDataActionIds);

    const QStringList sortedStatusIds = generalConfig.readEntry(StatesSortingId, QStringList());
    const QStringList hiddenStatusIds = generalConfig.readEntry(StatesHiddenId, QStringList());

    const KService::List statusOffers = KServiceTypeTrader::self()->query("kontinga/statusservice", propertyConstraint);

    m_servicesControl->dataActionControl()->setServices(statusOffers, sortedStatusIds, hiddenStatusIds);

    auto propertiesConfig = config->group(PropertiesGroupId);
    const QStringList sortedPropertyIds = propertiesConfig.readEntry(PropertiesSortingId, QStringList());
    const QStringList hiddenPropertyIds = propertiesConfig.readEntry(PropertiesHiddenId, QStringList());

    const KService::List propertyOffers = KServiceTypeTrader::self()->query("kontinga/property", propertyConstraint);

    m_propertyListControl->setProperties(propertyOffers, sortedPropertyIds, hiddenPropertyIds);
}

void KontingaControlModule::defaults()
{
    // insert your default settings code here...
    emit changed(true);
}

void KontingaControlModule::save()
{
    auto config = KSharedConfig::openConfig(ConfigFileName);

    if (m_actionServicesDirty) {
        auto generalConfig = config->group(GeneralGroupId);
        generalConfig.writeEntry(ActionsSortingId, m_servicesControl->actionControl()->sortedIds());
        generalConfig.writeEntry(ActionsHiddenId, m_servicesControl->actionControl()->hiddenIds());

        m_actionServicesDirty = false;
    }

    if (m_dataActionServicesDirty) {
        auto generalConfig = config->group(GeneralGroupId);
        generalConfig.writeEntry(DataActionsSortingId, m_servicesControl->dataActionControl()->sortedIds());
        generalConfig.writeEntry(DataActionsHiddenId, m_servicesControl->dataActionControl()->hiddenIds());

        m_dataActionServicesDirty = false;
    }

    if (m_statusServicesDirty) {
        auto generalConfig = config->group(GeneralGroupId);
        generalConfig.writeEntry(StatesSortingId, m_servicesControl->statusControl()->sortedIds());
        generalConfig.writeEntry(StatesHiddenId, m_servicesControl->statusControl()->hiddenIds());

        m_statusServicesDirty = false;
    }

    if (m_propertiesDirty) {
        auto propertiesConfig = config->group(PropertiesGroupId);
        propertiesConfig.writeEntry(PropertiesSortingId, m_propertyListControl->sortedPropertyIds());
        propertiesConfig.writeEntry(PropertiesHiddenId, m_propertyListControl->hiddenPropertyIds());

        m_propertiesDirty = false;
    }

    storeDirtyServices();
    for (auto it = m_dirtyPropertyActionServiceIds.constBegin(), end = m_dirtyPropertyActionServiceIds.constEnd();
         it != end; ++it) {
        const QString& propertyId = it.key();
        const KMainedIds& ids = it.value();

        auto propertyGroupConfig = config->group(QString::fromLatin1(PropertyGroupIdTemplate).arg(propertyId));

        propertyGroupConfig.writeEntry(ActionsSortingId, ids.SortedIds);
        propertyGroupConfig.writeEntry(ActionsHiddenId, ids.HiddenIds);
        propertyGroupConfig.writeEntry(ActionsMainId, ids.MainIds);
    }

    m_dirtyPropertyActionServiceIds.clear();
    for (auto it = m_dirtyPropertyDataActionServiceIds.constBegin(), end = m_dirtyPropertyDataActionServiceIds.constEnd();
         it != end; ++it) {
        const QString& propertyId = it.key();
        const KMainedIds& ids = it.value();

        auto propertyGroupConfig = config->group(QString::fromLatin1(PropertyGroupIdTemplate).arg(propertyId));

        propertyGroupConfig.writeEntry(DataActionsSortingId, ids.SortedIds);
        propertyGroupConfig.writeEntry(DataActionsHiddenId, ids.HiddenIds);
        propertyGroupConfig.writeEntry(DataActionsMainId, ids.MainIds);
    }

    m_dirtyPropertyDataActionServiceIds.clear();
    for (auto it = m_dirtyPropertyStatusServiceIds.constBegin(), end = m_dirtyPropertyStatusServiceIds.constEnd();
         it != end; ++it) {
        const QString& propertyId = it.key();
        const KIds& ids = it.value();

        auto propertyGroupConfig = config->group(QString::fromLatin1(PropertyGroupIdTemplate).arg(propertyId));

        propertyGroupConfig.writeEntry(StatesSortingId, ids.SortedIds);
        propertyGroupConfig.writeEntry(StatesHiddenId, ids.HiddenIds);
    }

    m_dirtyPropertyStatusServiceIds.clear();

    config->sync();

    emitDCOPChangeSignal();
    emit changed(true);
}

void KontingaControlModule::configChanged()
{
    // insert your saving code here...
    emitDCOPChangeSignal();
    emit changed(true);
}

QString KontingaControlModule::quickHelp() const
{
    return i18n("<h1>Person Properties and Services</h1>"
                " This module allows you to choose which of the properties of the"
                " persons in you addressbook are used and which services are offered."
                " <p>There are three kind of service types:"
                " <ul><li>Actions, e.g. starting a chat</li>"
                " <li>Actions on Data, e.g. sending a file via the chat system</li>"
                " <li>States, e.g. showing the presence in the chat system</li></ul>"
                " Services are available for a property of persons"
                " (e.g. for the addresses in a chat system)"
                " or for an entire person. To have an action service for a property also shown"
                " in menus on the entire person, set it to \"Global\", otherwise \"Local\"."
                " <p>Currently not all programs support these settings yet.");
}

void KontingaControlModule::setServicesDirty(bool dirty)
{
    const QWidget* currentControl = m_servicesControl->currentWidget();
    if (currentControl == m_servicesControl->actionControl()) {
        m_actionServicesDirty = dirty;
    } else if (currentControl == m_servicesControl->dataActionControl()) {
        m_dataActionServicesDirty = dirty;
    } else if (currentControl == m_servicesControl->statusControl()) {
        m_statusServicesDirty = dirty;
    }

    emit changed(dirty);
}

void KontingaControlModule::setPropertiesDirty(bool dirty)
{
    m_propertiesDirty = dirty;
    emit changed(dirty);
}

void KontingaControlModule::setPropertyServicesDirty(bool dirty)
{
    const QWidget* currentControl = m_propertyServicesControl->currentWidget();
    if (currentControl == m_propertyServicesControl->actionControl()) {
        m_propertyActionServicesDirty = dirty;
    } else if (currentControl == m_propertyServicesControl->dataActionControl()) {
        m_propertyDataActionServicesDirty = dirty;
    } else if (currentControl == m_propertyServicesControl->statusControl()) {
        m_propertyStatusServicesDirty = dirty;
    }

    emit changed(dirty);
}

void KontingaControlModule::storeDirtyServices()
{
    if (m_propertyActionServicesDirty) {
        KMainedIds& ids = m_dirtyPropertyActionServiceIds[m_propertyServicesControl->propertyId()];

        ids.SortedIds = m_propertyServicesControl->actionControl()->sortedActionIds();
        ids.HiddenIds = m_propertyServicesControl->actionControl()->hiddenActionIds();
        ids.MainIds = m_propertyServicesControl->actionControl()->mainActionIds();

        m_propertyActionServicesDirty = false;
    }
    if (m_propertyDataActionServicesDirty) {
        KMainedIds& ids = m_dirtyPropertyDataActionServiceIds[m_propertyServicesControl->propertyId()];

        ids.SortedIds = m_propertyServicesControl->dataActionControl()->sortedDataActionIds();
        ids.HiddenIds = m_propertyServicesControl->dataActionControl()->hiddenDataActionIds();
        ids.MainIds = m_propertyServicesControl->dataActionControl()->mainDataActionIds();

        m_propertyDataActionServicesDirty = false;
    }
    if (m_propertyStatusServicesDirty) {
        KIds& ids = m_dirtyPropertyStatusServiceIds[m_propertyServicesControl->propertyId()];

        ids.SortedIds = m_propertyServicesControl->statusControl()->sortedStatusIds();
        ids.HiddenIds = m_propertyServicesControl->statusControl()->hiddenStatusIds();

        m_propertyStatusServicesDirty = false;
    }
}

void KontingaControlModule::signalServiceConfigChange(int serviceTypeId, const QString& serviceId)
{
    emitDCOPServiceConfigChangeSignal(serviceTypeId, QString(), serviceId);
}

void KontingaControlModule::signalPropertyServiceConfigChange(int serviceTypeId, const QString& serviceId)
{
    emitDCOPServiceConfigChangeSignal(serviceTypeId, m_propertyServicesControl->propertyId(), serviceId);
}

void KontingaControlModule::onPropertySelect(const QString& propertyId)
{
    storeDirtyServices();
    m_widgetStack->setCurrentWidget(m_propertyServicesControl);

    const QString propertyConstraint =
        QString::fromLatin1("([X-KDE-KontingaProperty] == '%1') and ([X-KDE-ABI-Version] == '%2')")
        .arg(propertyId, QString::fromLatin1(ServiceABIVersion));

    auto config = KSharedConfig::openConfig(ConfigFileName);
    auto propertyGroupConfig = config->group(QString::fromLatin1(PropertyGroupIdTemplate).arg(propertyId));

    // action services
    const auto ait = m_dirtyPropertyActionServiceIds.constFind(propertyId);
    const KMainedIds* actionIds = (ait != m_dirtyPropertyActionServiceIds.constEnd()) ? &ait.value() : nullptr;

    const QStringList sortedActionIds = actionIds ? actionIds->SortedIds : propertyGroupConfig.readEntry(ActionsSortingId, QStringList());
    const QStringList hiddenActionIds = actionIds ? actionIds->HiddenIds : propertyGroupConfig.readEntry(ActionsHiddenId, QStringList());
    const QStringList mainActionIds =   actionIds ? actionIds->MainIds : propertyGroupConfig.readEntry(ActionsMainId, QStringList());

    const KService::List actionOffers =
        KServiceTypeTrader::self()->query("kontinga/propertyactionservice", propertyConstraint);

    m_propertyServicesControl->actionControl()->setActionServices(actionOffers, sortedActionIds, hiddenActionIds,
                                                                mainActionIds);

    // data action services
    const auto dait = m_dirtyPropertyDataActionServiceIds.constFind(propertyId);
    const KMainedIds* dataActionIds = (dait != m_dirtyPropertyDataActionServiceIds.constEnd()) ? &dait.value() : nullptr;

    const QStringList sortedDataActionIds = dataActionIds ? dataActionIds->SortedIds : propertyGroupConfig.readEntry(DataActionsSortingId, QStringList());
    const QStringList hiddenDataActionIds = dataActionIds ? dataActionIds->HiddenIds : propertyGroupConfig.readEntry(DataActionsHiddenId, QStringList());
    const QStringList mainDataActionIds =   dataActionIds ? dataActionIds->MainIds : propertyGroupConfig.readEntry(DataActionsMainId, QStringList());

    const KService::List dataActionOffers =
        KServiceTypeTrader::self()->query("kontinga/propertydataactionservice", propertyConstraint);

    m_propertyServicesControl->dataActionControl()->setDataActionServices(dataActionOffers,
                                                                        sortedDataActionIds, hiddenDataActionIds,
                                                                        mainDataActionIds);

    // status services
    const auto sit = m_dirtyPropertyStatusServiceIds.constFind(propertyId);
    const KIds* stateIds = (sit != m_dirtyPropertyStatusServiceIds.constEnd()) ? &sit.value() : nullptr;

    const QStringList sortedStatusIds = stateIds ? stateIds->SortedIds : propertyGroupConfig.readEntry(StatesSortingId, QStringList());
    const QStringList hiddenStatusIds = stateIds ? stateIds->HiddenIds : propertyGroupConfig.readEntry(StatesHiddenId, QStringList());

    const KService::List statusOffers =
        KServiceTypeTrader::self()->query("kontinga/propertystatusservice", propertyConstraint);

    m_propertyServicesControl->statusControl()->setStatusServices(statusOffers, sortedStatusIds, hiddenStatusIds);

    m_propertyServicesControl->setProperty(propertyId);
}

void KontingaControlModule::onDatabaseChange()
{
#if 0
    if (KSycoca::self()->isChanged("services")) {
        // ksycoca has new KService objects for us, make sure to update
        // our 'copies' to be in sync with it. Not important for OK, but
        // important for Apply (how to differentiate those 2?).
        // See BR 35071.
        QValueList<TypesListItem*>::Iterator it = m_itemsModified.begin();
        for (; it != m_itemsModified.end(); ++it) {
            QString name = (*it)->name();
            if (removedList.find(name) == removedList.end()) { // if not deleted meanwhile
                (*it)->refresh();
            }
        }

        m_itemsModified.clear();
    }
#endif
}
