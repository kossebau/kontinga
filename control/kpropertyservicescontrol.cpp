/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "kpropertyservicescontrol.hpp"

// module
#include "kpropertyactionlistcontrol.hpp"
#include "kpropertydataactionlistcontrol.hpp"
#include "kpropertystatuslistcontrol.hpp"
// KF
#include <KLocalizedString>

KPropertyServicesControl::KPropertyServicesControl(QWidget* parent)
    : QTabWidget(parent)
{
    m_actionControl = new KPropertyActionListControl(this);
    addTab(m_actionControl, i18n("&Actions"));
    connect(m_actionControl, &KSortedListBox::changed,
            this, &KPropertyServicesControl::changed);
    connect(m_actionControl, &KPropertyActionListControl::configChanged,
            this, &KPropertyServicesControl::configChanged);

    m_dataActionControl = new KPropertyDataActionListControl(this);
    addTab(m_dataActionControl, i18n("Actions on &Data"));
    connect(m_dataActionControl, &KSortedListBox::changed,
            this, &KPropertyServicesControl::changed);
    connect(m_dataActionControl, &KPropertyDataActionListControl::configChanged,
            this, &KPropertyServicesControl::configChanged);

    m_statusControl = new KPropertyStatusListControl(this);
    addTab(m_statusControl, i18n("&States"));
    connect(m_statusControl, &KSortedListBox::changed,
            this, &KPropertyServicesControl::changed);
    connect(m_statusControl, &KPropertyStatusListControl::configChanged,
            this, &KPropertyServicesControl::configChanged);
}

KPropertyServicesControl::~KPropertyServicesControl() = default;

void KPropertyServicesControl::setProperty(const QString& propertyId)
{
    m_propertyId = propertyId;
}
