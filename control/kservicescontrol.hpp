/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KCMKONTINGA_SERVICESCONTROL_HPP
#define KCMKONTINGA_SERVICESCONTROL_HPP

// KF
#include <KService>
// Qt
#include <QTabWidget>

class KActionListControl;
class KDataActionListControl;
class KStatusListControl;

class KServicesControl : public QTabWidget
{
    Q_OBJECT

public:
    explicit KServicesControl(QWidget* parent = nullptr);
    ~KServicesControl() override;

public:
    KActionListControl* actionControl() const;
    KDataActionListControl* dataActionControl() const;
    KStatusListControl* statusControl() const;

Q_SIGNALS:
    void changed(bool);
    void configChanged(int serviceTypeId, const QString& serviceId);

private:
    KActionListControl* m_actionControl;
    KDataActionListControl* m_dataActionControl;
    KStatusListControl* m_statusControl;
};

inline KActionListControl* KServicesControl::actionControl() const { return m_actionControl; }
inline KDataActionListControl* KServicesControl::dataActionControl() const { return m_dataActionControl; }
inline KStatusListControl* KServicesControl::statusControl() const { return m_statusControl; }

#endif
