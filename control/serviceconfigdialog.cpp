/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "serviceconfigdialog.hpp"

// KF
#include <KLocalizedString>
// Qt
#include <QDialogButtonBox>
#include <QPushButton>
#include <QVBoxLayout>

ServiceConfigDialog::ServiceConfigDialog(QWidget* parent, const QString& serviceName)
    : QDialog(parent)
    , m_dirty(false)
{
    setWindowTitle(i18n("Configure %1", serviceName));

    // main widget
    auto* mainWidget = new QWidget;
    m_mainWidgetLayout = new QVBoxLayout(mainWidget);
    m_mainWidgetLayout->setContentsMargins(0, 0, 0, 0);

    // dialog buttons
    m_dialogButtonBox = new QDialogButtonBox(this);
    m_dialogButtonBox->setStandardButtons(QDialogButtonBox::Ok | QDialogButtonBox::Cancel | QDialogButtonBox::RestoreDefaults);
    connect(m_dialogButtonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(m_dialogButtonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    // main layout
    auto* layout = new QVBoxLayout;
    layout->addWidget(mainWidget);
    layout->addStretch();
    layout->addWidget(m_dialogButtonBox);

    setLayout(layout);
}

void ServiceConfigDialog::setConfigWidget(QWidget* configWidget)
{
    // TODO: remove previous?
    m_mainWidgetLayout->addWidget(configWidget);

    connect(configWidget, SIGNAL(changed(bool)), SLOT(setDirty(bool)));
    connect(m_dialogButtonBox->button(QDialogButtonBox::RestoreDefaults), SIGNAL(clicked(bool)),
            configWidget, SLOT(defaults()));
    connect(m_dialogButtonBox, SIGNAL(accepted()),
            configWidget, SLOT(save()));
}

bool ServiceConfigDialog::isDirty() const
{
    return m_dirty;
}

void ServiceConfigDialog::setDirty(bool dirty)
{
    m_dirty = dirty;
}
