/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KCMKONTINGA_SERVICELISTITEML_HPP
#define KCMKONTINGA_SERVICELISTITEML_HPP

// KF
#include <KService>
// Qt
#include <QListWidgetItem>

enum
{
    ActionServiceId = 0,
    DataActionServiceId = 1,
    StatusServiceId = 2,
};

class KServiceListItem : public QListWidgetItem
{
public:
    explicit KServiceListItem(const KService::Ptr& service, bool isMain = false);
    ~KServiceListItem() override;

public: // QListWidgetItem API
//     virtual void paint( QPainter *Painter );
//     virtual int width( const QListBox* ListBox ) const;

public:
    void setMain(bool isMain);
    bool configure(QWidget* parent);

public:
    KService::Ptr service() const;
    bool isMain() const;
    bool isConfigurable() const;

private:
    KService::Ptr m_service;
    bool m_isConfigurable : 1;
    bool m_isMain : 1;
};

inline KService::Ptr KServiceListItem::service() const { return m_service; }
inline bool KServiceListItem::isConfigurable() const { return m_isConfigurable; }

inline bool KServiceListItem::isMain() const { return m_isMain; }
inline void KServiceListItem::setMain(bool isMain) { m_isMain = isMain; }

#endif
