/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "kservicelistitem.hpp"

// module
#include "serviceconfigdialog.hpp"
// Qt
#include <QApplication>
#include <QPainter>

static inline QString configLibrary(const KService::Ptr& service)
{
    return service->property(QString::fromLatin1("X-KDE-ConfigLibrary")).toString();
}

KServiceListItem::KServiceListItem(const KService::Ptr& service, bool isMain)
    : QListWidgetItem(service->name())
    , m_service(service)
    , m_isMain(isMain)
{
    m_isConfigurable = !configLibrary(service).isEmpty();
}

KServiceListItem::~KServiceListItem() = default;

bool KServiceListItem::configure(QWidget* parent)
{
    bool result = false;

    ServiceConfigDialog dialog(parent, m_service->name());

    QWidget* configWidget = nullptr;
//         KParts::ComponentFactory::createInstanceFromLibrary<QWidget>( configLibrary(Service).local8Bit().data(), &Dialog );

    if (configWidget) {
        dialog.setConfigWidget(configWidget);

        if (dialog.exec() == QDialog::Accepted) {
            result = dialog.isDirty();
        }
    }

    return result;
}

#if 0
void KServiceListItem::paint(QPainter* Painter)
{
    if (IsMain) {
        QFont Font = Painter->font();
        Font.setBold(true);
        Painter->setFont(Font);
    }
    const int itemHeight = height(listBox());
    const QFontMetrics FontMetrics = Painter->fontMetrics();
    const int yPos = ((itemHeight - FontMetrics.height()) / 2) + FontMetrics.ascent();
    Painter->drawText(3, yPos, text());
}

int KServiceListItem::width(const QListBox* ListBox) const
{
    const int Width = ListBox ? ListBox->fontMetrics().width(text()) + 6 : 0;
    return QMAX(Width, QApplication::globalStrut().width());
}
#endif
