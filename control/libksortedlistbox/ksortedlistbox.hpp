/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KCMKONTINGA_SORTEDLISTBOX_HPP
#define KCMKONTINGA_SORTEDLISTBOX_HPP

// Qt
#include <QVector>
#include <QWidget>

class QListWidget;
class QListWidgetItem;
class QPushButton;

class KSortedListBox : public QWidget
{
    Q_OBJECT

public:
    enum Button
    {
        Add = 1,
        UpDown = 2,
        Action1 = 4,
        Action2 = 8,
        Action3 = 16,
        Remove = 32,
    };

public:
    explicit KSortedListBox(QWidget* parent = nullptr, int buttons = Add | UpDown | Remove);
    ~KSortedListBox() override;

public:

Q_SIGNALS:
    void changed(bool);

protected:
    virtual QListWidgetItem* createItem();
    virtual bool action1Item(QListWidgetItem* Item);
    virtual bool action2Item(QListWidgetItem* Item);
    virtual bool action3Item(QListWidgetItem* Item);
    virtual bool deleteItem(QListWidgetItem* Item);

protected:
    void setItemList(const QVector<QListWidgetItem*>& itemList);
    int count() const;
    void setButtonAction1(const QString& text);
    void setButtonAction2(const QString& text);
    void setButtonAction3(const QString& text);

protected Q_SLOTS:
    void addItem();
    void action1Item();
    void action2Item();
    void action3Item();
    void removeItem();
    void moveUp();
    void moveDown();

private Q_SLOTS:
    void onItemHighlighted(int index);

protected:
    QListWidget* m_listBox;
    QPushButton* m_newButton;
    QPushButton* m_upButton;
    QPushButton* m_downButton;
    QPushButton* m_action1Button;
    QPushButton* m_action2Button;
    QPushButton* m_action3Button;
    QPushButton* m_removeButton;
};

#endif
