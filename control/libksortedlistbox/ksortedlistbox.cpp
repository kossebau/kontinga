/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "ksortedlistbox.hpp"

// KF
// #include <knotifyclient.h>
#include <KLocalizedString>
// Qt
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QWhatsThis>
#include <QListWidget>
#include <QListWidgetItem>

KSortedListBox::KSortedListBox(QWidget* parent, int buttons)
    : QWidget(parent)
{
    QHBoxLayout* topLayout = new QHBoxLayout(this);   // TODO: margin 0?
    QVBoxLayout* buttonsLayout = new QVBoxLayout();

    m_listBox = new QListWidget(this);
    m_listBox->addItem(i18n("None"));
    m_listBox->setEnabled(false);
    connect(m_listBox, SIGNAL(highlighted(int)), SLOT(onItemHighlighted(int)));
//     connect( m_listBox, SIGNAL(executed( QListWidgetItem * )), SLOT(action1Item()) );

    if (buttons & Add) {
        m_newButton = new QPushButton(i18n("Add..."), this);
        m_newButton->setEnabled(false);
        connect(m_newButton, SIGNAL(clicked()), SLOT(addItem()));
        buttonsLayout->addWidget(m_newButton);
    } else {
        m_newButton = nullptr;
    }

    if (buttons & UpDown) {
        m_upButton = new QPushButton(i18n("Move &Up"), this);
        m_upButton->setEnabled(false);
        connect(m_upButton, SIGNAL(clicked()), SLOT(moveUp()));
        buttonsLayout->addWidget(m_upButton);
        m_downButton = new QPushButton(i18n("Move &Down"), this);
        m_downButton->setEnabled(false);
        connect(m_downButton, SIGNAL(clicked()), SLOT(moveDown()));
        buttonsLayout->addWidget(m_downButton);
    } else {
        m_upButton = nullptr;
        m_downButton = nullptr;
    }

    if (buttons & Action1) {
        m_action1Button = new QPushButton(this);
        m_action1Button->setEnabled(false);
        connect(m_action1Button, SIGNAL(clicked()), SLOT(action1Item()));
        buttonsLayout->addWidget(m_action1Button);
    } else {
        m_action1Button = nullptr;
    }

    if (buttons & Action2) {
        m_action2Button = new QPushButton(this);
        m_action2Button->setEnabled(false);
        connect(m_action2Button, SIGNAL(clicked()), SLOT(action2Item()));
        buttonsLayout->addWidget(m_action2Button);
    } else {
        m_action2Button = nullptr;
    }

    if (buttons & Action3) {
        m_action3Button = new QPushButton(this);
        m_action3Button->setEnabled(false);
        connect(m_action3Button, SIGNAL(clicked()), SLOT(action3Item()));
        buttonsLayout->addWidget(m_action3Button);
    } else {
        m_action3Button = nullptr;
    }

    if (buttons & Remove) {
        m_removeButton = new QPushButton(i18n("Remove"), this);
        m_removeButton->setEnabled(false);
        connect(m_removeButton, SIGNAL(clicked()), SLOT(removeItem()));
        buttonsLayout->addWidget(m_removeButton);
    } else {
        m_removeButton = nullptr;
    }
    buttonsLayout->addStretch();

    topLayout->addWidget(m_listBox);
    topLayout->addLayout(buttonsLayout);
}

KSortedListBox::~KSortedListBox() = default;

QListWidgetItem* KSortedListBox::createItem() { return nullptr; }
bool KSortedListBox::action1Item(QListWidgetItem*/*Item*/) { return false; }
bool KSortedListBox::action2Item(QListWidgetItem*/*Item*/) { return false; }
bool KSortedListBox::action3Item(QListWidgetItem*/*Item*/) { return false; }
bool KSortedListBox::deleteItem(QListWidgetItem*/*Item*/) { return false; }

int KSortedListBox::count() const
{
    return (m_listBox->item(0)->text() == i18n("None")) ? 0 : m_listBox->count();
}

void KSortedListBox::setItemList(const QVector<QListWidgetItem*>& itemList)
{
    m_listBox->clear();

    const bool hasItems = !itemList.isEmpty();
    if (hasItems) {
        for (const auto& item : itemList) {
            m_listBox->addItem(item);
        }
    } else {
        m_listBox->addItem(i18n("None"));
    }

    m_listBox->setEnabled(hasItems);
}

void KSortedListBox::setButtonAction1(const QString& text)
{
    if (m_action1Button) {
        m_action1Button->setText(text);
    }
}

void KSortedListBox::setButtonAction2(const QString& text)
{
    if (m_action2Button) {
        m_action2Button->setText(text);
    }
}

void KSortedListBox::setButtonAction3(const QString& text)
{
    if (m_action3Button) {
        m_action3Button->setText(text);
    }
}

void KSortedListBox::addItem()
{
    QListWidgetItem* item = createItem();
    if (!item) {
        return;
    }

    // if None is the only item, then there currently is no default
    if ((m_listBox->count() == 1) && (m_listBox->item(0)->text() == i18n("None"))) {
        delete m_listBox->takeItem(0);
        m_listBox->setEnabled(true);
    }

    m_listBox->insertItem(0, item);
    m_listBox->setCurrentRow(0);

    emit changed(true);
}

void KSortedListBox::removeItem()
{
    const int currentIndex = m_listBox->currentRow();
    QListWidgetItem* currentItem = m_listBox->item(currentIndex);

    if (currentItem && deleteItem(currentItem)) {
        delete m_listBox->takeItem(currentIndex);

        if (m_listBox->currentRow() == -1) {
            m_listBox->addItem(i18n("None"));
            m_listBox->setEnabled(false);

            m_removeButton->setEnabled(false);
            if (m_action1Button) {
                m_action1Button->setEnabled(false);
            }
        }
        emit changed(true);
    }
}

void KSortedListBox::action1Item()
{
    QListWidgetItem* currentItem = m_listBox->currentItem();

    if (currentItem && action1Item(currentItem)) {
//         updateItem( currentItem );
//         m_listBox->triggerUpdate( true );
        emit changed(true);
    }
}

void KSortedListBox::action2Item()
{
    QListWidgetItem* currentItem = m_listBox->currentItem();

    if (currentItem && action2Item(currentItem)) {
//         updateItem( currentItem );
//         m_listBox->triggerUpdate( true );
        emit changed(true);
    }
}

void KSortedListBox::action3Item()
{
    QListWidgetItem* currentItem = m_listBox->currentItem();

    if (currentItem && action3Item(currentItem)) {
//         updateItem( currentItem );
//         m_listBox->triggerUpdate( true );
        emit changed(true);
    }
}

void KSortedListBox::moveUp()
{
    if (!m_listBox->isEnabled()) {
//         KNotifyClient::beep();
        return;
    }

    const unsigned int currentIndex = m_listBox->currentRow();
    // already first? TODO: up button should not be available
    if (currentIndex == 0) {
//         KNotifyClient::beep();
        return;
    }

    QListWidgetItem* currentItem = m_listBox->takeItem(currentIndex);
    m_listBox->insertItem(currentIndex - 1, currentItem);
    m_listBox->setCurrentRow(currentIndex - 1);

    emit changed(true);
}

void KSortedListBox::moveDown()
{
    if (!m_listBox->isEnabled()) {
//         KNotifyClient::beep();
        return;
    }

    const int currentIndex = m_listBox->currentRow();
    // already last? TODO: down button should not be available
    if (currentIndex == m_listBox->count() - 1) {
//         KNotifyClient::beep();
        return;
    }

    QListWidgetItem* currentItem = m_listBox->takeItem(currentIndex);
    m_listBox->insertItem(currentIndex + 1, currentItem);
    m_listBox->setCurrentRow(currentIndex + 1);

    emit changed(true);
}

void KSortedListBox::onItemHighlighted(int index)
{
    if (m_listBox->count() <= 1) {
        m_upButton->setEnabled(false);
        m_downButton->setEnabled(false);
    } else if (index == (m_listBox->count() - 1)) {
        m_upButton->setEnabled(true);
        m_downButton->setEnabled(false);
    } else if (index == 0) {
        m_upButton->setEnabled(false);
        m_downButton->setEnabled(true);
    } else {
        m_upButton->setEnabled(true);
        m_downButton->setEnabled(true);
    }

    m_removeButton->setEnabled(true);
    if (m_action1Button) {
        m_action1Button->setEnabled(true);
    }
}
