/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KCMKONTINGA_PROPERTYDATAACTIONLISTCONTROL_HPP
#define KCMKONTINGA_PROPERTYDATAACTIONLISTCONTROL_HPP

// lib
#include <ksortedlistbox.hpp>
// KF
#include <KService>

class QStringList;

class KPropertyDataActionListControl : public KSortedListBox
{
    Q_OBJECT

public:
    explicit KPropertyDataActionListControl(QWidget* parent = nullptr);
    ~KPropertyDataActionListControl() override;

public:
    void setDataActionServices(const KService::List& actionServices,
                               const QStringList& sortedActionIds, const QStringList& hiddenActionIds,
                               const QStringList& mainActionIds);

public:
    QStringList sortedDataActionIds() const;
    QStringList hiddenDataActionIds() const;
    QStringList mainDataActionIds() const;

Q_SIGNALS:
    void selected(const QString& propertyId);
    void configChanged(int serviceTypeId, const QString& serviceId);

protected: // KSortedListBox API
    QListWidgetItem* createItem() override;
    bool action1Item(QListWidgetItem* item) override;
    bool action2Item(QListWidgetItem* item) override;
    bool action3Item(QListWidgetItem* item) override;
    bool deleteItem(QListWidgetItem* item) override;

private Q_SLOTS:
    void onItemHighlighted(int index);

private:
    KService::List m_hiddenDataActionServices;
};

#endif
