/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KCMKONTINGA_KCMKONTINGA_HPP
#define KCMKONTINGA_KCMKONTINGA_HPP

// KF
#include <KCModule>

class QStackedWidget;
class QTabWidget;
class KServicesControl;
class KPropertyListControl;
class KPropertyServicesControl;

class KMainedIds
{
public:
    QStringList SortedIds;
    QStringList HiddenIds;
    QStringList MainIds;
};
class KIds
{
public:
    QStringList SortedIds;
    QStringList HiddenIds;
};

class KontingaControlModule : public KCModule
{
    Q_OBJECT

public:
    KontingaControlModule(QWidget* Parent, const QVariantList& args);
    ~KontingaControlModule() override;

public: // KCModule API
    void load() override;
    void save() override;
    void defaults() override;
    QString quickHelp() const override;

protected:
    void storeDirtyServices();

private Q_SLOTS:
    void setServicesDirty(bool dirty);
    void setPropertiesDirty(bool dirty);
    void setPropertyServicesDirty(bool dirty);
    void signalServiceConfigChange(int serviceTypeId, const QString& serviceId);
    void signalPropertyServiceConfigChange(int serviceTypeId, const QString& serviceId);
    void onPropertySelect(const QString& propertyId);
    void configChanged();
    void onDatabaseChange();

private:
    QTabWidget* m_tabWidget;
    KServicesControl* m_servicesControl;
    KPropertyListControl* m_propertyListControl;
    QStackedWidget* m_widgetStack;
    KPropertyServicesControl* m_propertyServicesControl;

    QMap<QString, KMainedIds> m_dirtyPropertyActionServiceIds;
    QMap<QString, KMainedIds> m_dirtyPropertyDataActionServiceIds;
    QMap<QString, KIds> m_dirtyPropertyStatusServiceIds;
    bool m_actionServicesDirty;
    bool m_dataActionServicesDirty;
    bool m_statusServicesDirty;
    bool m_propertiesDirty;
    bool m_propertyActionServicesDirty;
    bool m_propertyDataActionServicesDirty;
    bool m_propertyStatusServicesDirty;
};

#endif
