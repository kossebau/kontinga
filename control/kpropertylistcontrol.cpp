/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "kpropertylistcontrol.hpp"

// module
#include "kservicelistitem.hpp"
#include "kserviceselectdialog.hpp"
// KF
#include <KLocalizedString>
// Qt
#include <QListWidgetItem>
#include <QPushButton>
#include <QToolTip>
#include <QWhatsThis>

static inline QString propertyId(const KService::Ptr& service)
{
    return service->property(QString::fromLatin1("X-KDE-KontingaProperty")).toString();
}

KPropertyListControl::KPropertyListControl(QWidget* parent)
    : KSortedListBox(parent)
{
    connect(m_listBox, SIGNAL(highlighted(int)), SLOT(onItemHighlighted(int)));

    const QString description = i18n(
        " This is a list of person properties."
        " This list is used for limiting and sorting when displaying"
        " a person's properties, also offering the associated actions in the menus.");

//     QWhatsThis::add( this, description );
//     QWhatsThis::add( m_listBox, description );

//     QToolTip::add( m_newButton,     i18n( "Adds a new property.") );
//     QToolTip::add( m_upButton,      i18n( "Moves the current property up in the list.") );
//     QToolTip::add( m_downButton,    i18n( "Moves the current property down in the list.") );
//     QToolTip::add( m_removeButton,  i18n( "Removes the current property from the list.") );
}

KPropertyListControl::~KPropertyListControl() = default;

// TODO: to be optimized
void KPropertyListControl::setProperties(const KService::List& propertyServices,
                                         const QStringList& sortedPropertyIds, const QStringList& hiddenPropertyIds)
{
    m_hiddenPropertyServices = propertyServices;

    QVector<QListWidgetItem*> itemList;

    // add sorted items
    for (const auto& sortedPropertyId : sortedPropertyIds) {
        const auto endIt = m_hiddenPropertyServices.end();
        for (auto it = m_hiddenPropertyServices.begin(); it != endIt; ++it) {
            const QString propertyId = ::propertyId(*it);
            if (propertyId == sortedPropertyId) {
                itemList.append(new KServiceListItem(*it));
                m_hiddenPropertyServices.erase(it);
                break;
            }
        }
    }

    // add unsorted items
    for (auto it = m_hiddenPropertyServices.begin(); it != m_hiddenPropertyServices.end();) {
        const QString propertyId = ::propertyId(*it);
        // not hidden?
        if (!hiddenPropertyIds.contains(propertyId)) {
            itemList.append(new KServiceListItem(*it));
            it = m_hiddenPropertyServices.erase(it);
        } else {
            ++it;
        }
    }

    setItemList(itemList);

    m_newButton->setEnabled(!m_hiddenPropertyServices.isEmpty());
}

QStringList KPropertyListControl::sortedPropertyIds() const
{
    QStringList result;
    const unsigned int count = this->count();

    result.reserve(count);
    for (unsigned int i = 0; i < count; ++i) {
        auto* item = static_cast<KServiceListItem*>(m_listBox->item(i));
        result.append(propertyId(item->service()));
    }

    return result;
}

QStringList KPropertyListControl::hiddenPropertyIds() const
{
    QStringList result;

    result.reserve(m_hiddenPropertyServices.size());
    for (const auto& service : m_hiddenPropertyServices) {
        result.append(propertyId(service));
    }

    return result;
}

void KPropertyListControl::onItemHighlighted(int index)
{
    auto* item = static_cast<KServiceListItem*>(m_listBox->item(index));

    emit selected(propertyId(item->service()));
}

QListWidgetItem* KPropertyListControl::createItem()
{
    KService::Ptr service(nullptr);
    KServiceSelectDialog dialog(i18n("Property Selection"), i18n("Select a property:"), this);
    dialog.setServices(m_hiddenPropertyServices);
    if (dialog.exec() != QDialog::Accepted) {
        return nullptr;
    }
    service = dialog.service();

    if (!service) {
        return nullptr;
    }

    m_hiddenPropertyServices.removeOne(service);

    return new KServiceListItem(service);
}

bool KPropertyListControl::deleteItem(QListWidgetItem* item)
{
    bool result = true;

    auto* serviceItem = static_cast<KServiceListItem*>(item);
    m_hiddenPropertyServices.append(serviceItem->service());
    m_newButton->setEnabled(true);

    return result;
}

bool KPropertyListControl::action1Item(QListWidgetItem* item)
{
    Q_UNUSED(item);

    return true;
}
