/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_PROPERTYSTATUSSERVICE_HPP
#define KONTINGA_PROPERTYSTATUSSERVICE_HPP

// lib
#include "status.hpp"
#include "propertyservice.hpp"

namespace KContacts {
class Addressee;
}

namespace Kontinga {

class PropertyStatusServiceClient;
class PropertyStatusServicePrivate;

// TODO: add possibility to determine if/how dynamic service can be invoked on the type
// perhaps by adding return bool to fillMenu
// better show unavailability instead of ignoring? Or both, chosen by service?
class KONTINGACORE_EXPORT PropertyStatusService : public PropertyService
{
    Q_OBJECT

public:
    PropertyStatusService(QObject* parent, const QVariantList& arguments);
    ~PropertyStatusService() override;

public: // interface
    virtual Status status(const KContacts::Addressee& person, int itemIndex, int flags) const = 0;

    /** @returns if the service is possible for the item; defaults to return true */
    virtual bool supports(const KContacts::Addressee& person, int itemIndex) const;

    /** if ItemIndex = -1 register all items */
    virtual void registerClient(PropertyStatusServiceClient* client, int itemIndex = -1);
    virtual void unregisterClient(PropertyStatusServiceClient* client, int itemIndex = -1);

private:
    Q_DECLARE_PRIVATE(PropertyStatusService)
};

}

#endif
