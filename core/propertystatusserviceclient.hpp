/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_PROPERTYSTATUSSERVICECLIENT_HPP
#define KONTINGA_PROPERTYSTATUSSERVICECLIENT_HPP

namespace KContacts {
class Addressee;
}
class QString;
template <class C> class QVector;

namespace Kontinga {

class PropertyStatusService;
class Status;
class StatusChange;

class PropertyStatusServiceClient
{
public:
    PropertyStatusServiceClient();
    virtual ~PropertyStatusServiceClient();

public: // interface
    /** returns the person for which the services are requested */
    virtual const KContacts::Addressee& person() const = 0;

public: // slots interface
    /** called if the service switched */
    virtual void onStateChange(const PropertyStatusService& service, const StatusChange& change,
                               const Status& newStatus, int itemIndex) = 0;
    virtual void onPropertyManagerChange() = 0;
};

inline PropertyStatusServiceClient::PropertyStatusServiceClient() = default;
inline PropertyStatusServiceClient::~PropertyStatusServiceClient() = default;

using PropertyStatusServiceClientList = QVector<PropertyStatusServiceClient*>;

}

#endif
