/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_PROPERTYITEM_HPP
#define KONTINGA_PROPERTYITEM_HPP

// lib
#include "roles.hpp"
#include "propertyitemadapter.hpp"
#include <kontinga/kontingacore_export.hpp>
// Qt
#include <QVariant>
#include <QExplicitlySharedDataPointer>

namespace Kontinga {

class KONTINGACORE_EXPORT PropertyItem
{
public:
    PropertyItem(PropertyItemAdapter* adapter);

public:
    QVariant data(int role = DisplayTextRole) const;

private:
    QExplicitlySharedDataPointer<PropertyItemAdapter> m_adapter;
};

inline PropertyItem::PropertyItem(PropertyItemAdapter* adapter) : m_adapter(adapter) {}
inline QVariant PropertyItem::data(int Role) const { return m_adapter->data(Role); }

}

#endif
