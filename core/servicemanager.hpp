/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_SERVICEMANAGER_HPP
#define KONTINGA_SERVICEMANAGER_HPP

// lib
#include "idptrvector.hpp"
#include "statusservice.hpp"
#include "statusserviceclient.hpp"
#include "actionservice.hpp"
#include "actionserviceclient.hpp"
#include "listactionserviceclient.hpp"
#include "dataactionservice.hpp"
#include "dataactionserviceclient.hpp"
#include "listdataactionserviceclient.hpp"
// Qt
#include <QVector>

class QMimeData;

namespace Kontinga {

using ActionServiceList = IdPtrVector<ActionService>;
using DataActionServiceList = IdPtrVector<DataActionService>;
using StatusServiceList = IdPtrVector<StatusService>;
using ActionServiceClientList = QVector<ActionServiceClient*>;
using ListActionServiceClientList = QVector<ListActionServiceClient*>;
using DataActionServiceClientList = QVector<DataActionServiceClient*>;
using ListDataActionServiceClientList = QVector<ListDataActionServiceClient*>;
using StatusServiceClientList = QVector<StatusServiceClient*>;

// TODO: hide by iterators in services API
class KONTINGACORE_EXPORT ServiceManager
{
    friend class Services;
    friend class ServicesPrivate;

public:
    ServiceManager();
    ~ServiceManager();

public: // status interface
    void registerClient(StatusServiceClient* client);
    void unregisterClient(StatusServiceClient* client);

    const StatusServiceList& statusServices() const;

public: // service interface
    void registerClient(ActionServiceClient* client);
    void unregisterClient(ActionServiceClient* client);
    void registerClient(ListActionServiceClient* client);
    void unregisterClient(ListActionServiceClient* client);

    void execute(const KContacts::Addressee& person, const QString& serviceId) const;
    void execute(const KContacts::AddresseeList& list, const QString& serviceId) const;

    const ActionServiceList& actionServices() const;

public: // data service interface
    void registerClient(DataActionServiceClient* client);
    void unregisterClient(DataActionServiceClient* client);
    void registerClient(ListDataActionServiceClient* client);
    void unregisterClient(ListDataActionServiceClient* client);

    void execute(const KContacts::Addressee& person, const QMimeData* data, const QString& serviceId) const;
    void execute(const KContacts::AddresseeList& list, const QMimeData* data, const QString& serviceId) const;

    const DataActionServiceList& dataActionServices() const;

public: // management interface
    void addStatusService(StatusService* statusService);
    void removeStatusService(const QString& statusServiceId, bool doDelete = false);

    void addActionService(ActionService* service /*, bool AsDefault*/);  // TODO: addActionServices(), whole list in one step?
    // TODO: does ServiceId do it? Better pointer to the service?
    /** deletes the service with the id
     * @param ServiceId the id of the service. If -1 all services are deleted
     * @param Delete If true also delete the removed service(s)
     */
    void removeActionService(const QString& serviceId, bool doDelete = false);

    void addDataActionService(DataActionService* S /*, bool AsDefault*/);
    void removeDataActionService(const QString& serviceId, bool doDelete = false);

private:
    void setActionServiceSorting(const QStringList& serviceIds);
    void setDataActionServiceSorting(const QStringList& serviceIds);
    void setStatusServiceSorting(const QStringList& serviceIds);

    void reloadConfig(int ServiceType, const QString& serviceId);

private:
//     void informStatusServiceClients();
    void informActionServiceClients(); // TODO: move to ClientForItemList
    void informDataActionServiceClients();
    void informStatusServiceClients();

private:
    /** */
    StatusServiceList m_statusServices;
    /** list of all clients interested in status services */
    StatusServiceClientList m_statusServiceClients;

    /** */
    ActionServiceList m_actionServices;
    /** */
    ActionServiceClientList m_actionServiceClients;
    ListActionServiceClientList m_listActionServiceClients;

    /** */
    DataActionServiceList m_dataActionServices;
    /** */
    DataActionServiceClientList m_dataActionServiceClients;
    ListDataActionServiceClientList m_listDataActionServiceClients;
};

}

#endif
