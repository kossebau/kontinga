/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_LISTPROPERTYACTIONSERVICE_HPP
#define KONTINGA_LISTPROPERTYACTIONSERVICE_HPP

// lib
#include "propertyactionservice.hpp"

namespace KContacts {
class AddresseeList;
}

namespace Kontinga {

class ListPropertyActionServiceClient;
class ListPropertyActionServicePrivate;

// TODO: show unavailability instead of ignoring? Or both, chosen by service?
/**
 * Base class for all action services.
 *
 * A service operates on an item of a given property of a person.
 *
 */
class KONTINGACORE_EXPORT ListPropertyActionService : public PropertyActionService
{
    Q_OBJECT

protected:
    ListPropertyActionService(QObject* parent, const QVariantList& arguments);

public:
    ~ListPropertyActionService() override;

public: // interface
    /** @return action which describes the service */
    virtual ServiceAction action(const KContacts::AddresseeList& personList, int flags) const = 0;
    using PropertyActionService::action;

    /** does the service on the item with the given index from the supported person type */
    virtual void execute(const KContacts::AddresseeList& personList) = 0;
// TODO: best would be a list with pairs of person and item index...
    using PropertyActionService::execute;
    /**
     * Reports the general support of the service for the given item of the supported property.
     * Default implementation returns true.
     * @param Person
     * @param ItemIndex index of the item. If -1 the query is for any item
     * @returns if the service is generally possible for the given or any item, otherwise false
     */
    virtual bool supports(const KContacts::AddresseeList& personList) const;
    using PropertyActionService::supports;

    /**
     * Reports the current availability of the service for the given item of the supported property.
     * Default implementation returns "supports( Person, ItemIndex ) && isAvailable()".
     * @param PersonList
     * @returns true if the service is currently available for the item, otherwise false
     */
    virtual bool isAvailableFor(const KContacts::AddresseeList& personList) const;
    using PropertyActionService::isAvailableFor;

    /** if ItemIndex = -1 register all items
     * only items for which the service is possible are registered
     * if client data changes it has to unregister and register again
     */
    virtual void registerClient(ListPropertyActionServiceClient* client);
    virtual void unregisterClient(ListPropertyActionServiceClient* client);
    using PropertyActionService::registerClient;
    using PropertyActionService::unregisterClient;
    // KDE4: think about adding support for updating registration on data change
    // virtual void updateClient( ActionServiceClient* Client, int ItemIndex = -1 );

private:
    Q_DECLARE_PRIVATE(ListPropertyActionService)
};

}

#endif
