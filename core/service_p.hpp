/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_SERVICE_P_HPP
#define KONTINGA_SERVICE_P_HPP

#include "service.hpp"
// Qt
#include <QStringList>
// #include <QString>

namespace Kontinga {

class ServicePrivate
{
public:
    explicit ServicePrivate(const QVariantList& arguments);
    virtual ~ServicePrivate();

public:
    bool fitsIn(const QString& context) const;
    const QString& id() const;

private:
    QString m_id;
    QStringList m_onlyShowInContext;
    QStringList m_notShowInContext;
};

inline ServicePrivate::ServicePrivate(const QVariantList& arguments)
{
    const int argumentsCount = arguments.size();

    if (argumentsCount > 0) {
        m_id = arguments[0].toString();
        if (argumentsCount > 1) {
            m_onlyShowInContext = arguments[1].toString().split(';', QString::SkipEmptyParts);
            if (argumentsCount > 2) {
                m_notShowInContext = arguments[2].toString().split(';', QString::SkipEmptyParts);
            }
        }
    }
}

inline ServicePrivate::~ServicePrivate() = default;

inline bool ServicePrivate::fitsIn(const QString& context) const
{
    return !m_notShowInContext.contains(context) &&
           (m_onlyShowInContext.isEmpty() || m_onlyShowInContext.contains(context));
}

inline const QString& ServicePrivate::id() const { return m_id; }

}

#endif
