/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_PROPERTYALLACTIONSERVICECLIENT_HPP
#define KONTINGA_PROPERTYALLACTIONSERVICECLIENT_HPP

// lib
#include "propertyactionserviceclient.hpp"

class QString;

namespace Kontinga {

class PropertyAllActionServiceClient : public PropertyActionServiceClient
{
public:
    PropertyAllActionServiceClient();
    ~PropertyAllActionServiceClient() override;

public: // interface
    /** */
    virtual QString propertyId() const = 0;
    virtual int itemIndex() const = 0;

public: // slots interface
    /** called if the default switched */
    virtual void onAllActionServiceChange() = 0;
};

inline PropertyAllActionServiceClient::PropertyAllActionServiceClient() = default;
inline PropertyAllActionServiceClient::~PropertyAllActionServiceClient() = default;

}

#endif
