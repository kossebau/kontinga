/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "propertyadapter.hpp"

// lib
#include "listpropertyactionservice.hpp"
#include "listpropertydataactionservice.hpp"
#include "propertymanager.hpp"
// Qt
#include <QString>

namespace Kontinga {

PropertyManager::PropertyManager(PropertyAdapter* propertyAdapter)
    : m_adapter(propertyAdapter)
{}

PropertyManager::~PropertyManager()
{
    removeActionService(QString(), true);
    removeDataActionService(QString(), true);
    removeStatusService(QString(), true);
}

const PropertyAdapter* PropertyManager::propertyAdapter() const
{
    return m_adapter;
}

const PropertyStatusServiceList& PropertyManager::statusServices() const
{
    return m_statusServices;
}

const PropertyActionServiceList& PropertyManager::actionServices() const
{
    return m_actionServices;
}

const PropertyActionServiceList& PropertyManager::mainActionServices() const
{
    return m_mainActionServices;
}

const PropertyDataActionServiceList& PropertyManager::dataActionServices() const
{
    return m_dataActionServices;
}

const PropertyDataActionServiceList& PropertyManager::mainDataActionServices() const
{
    return m_mainDataActionServices;
}

QString PropertyManager::id() const
{
    return m_adapter->id();
}

void PropertyManager::registerClient(PropertyStatusServiceClient* client)
{
    // TODO: if( m_adapter->isContained(Client->person()) )
    for (auto* service : qAsConst(m_statusServices)) {
        service->registerClient(client);
    }

    m_statusServiceClients.append(client);
}

void PropertyManager::unregisterClient(PropertyStatusServiceClient* client)
{
    for (auto* service : qAsConst(m_statusServices)) {
        service->unregisterClient(client);
    }

    m_statusServiceClients.removeAll(client);
}

void PropertyManager::addStatusService(PropertyStatusService* statusService)
{
    m_statusServices.append(statusService);
    statusService->setAdapter(m_adapter);

    for (auto* client : qAsConst(m_statusServiceClients)) {
        statusService->registerClient(client);
    }

    informStatusServiceClients();
}

void PropertyManager::removeStatusService(const QString& statusServiceId, bool doDelete)
{
    m_statusServices.remove(statusServiceId, doDelete);

    informStatusServiceClients();
}

void PropertyManager::informStatusServiceClients()
{
// TODO:
    // inform clients
//    for (autp* client : qAsConst(m_statusServiceClients)) {
//        client->onAllServiceChange();
//     }
}

void PropertyManager::execute(const KContacts::Addressee& person, int itemIndex, const QString& serviceId) const
{
    PropertyActionService* service = m_actionServices[serviceId];

    if (service) {
        service->execute(person, itemIndex);
    }
}

void PropertyManager::execute(const KContacts::AddresseeList& personList, const QString& serviceId) const
{
    ListPropertyActionService* service = qobject_cast<ListPropertyActionService*>(m_actionServices[serviceId]);

    if (service) {
        service->execute(personList);
    }
}

void PropertyManager::addActionService(PropertyActionService* service, bool asMain)
{
    m_actionServices.append(service);
    if (asMain) {
        m_mainActionServices.append(service);
    }
    service->setAdapter(m_adapter);

    for (auto* client : qAsConst(m_propertyAllActionServiceClients)) {
        service->registerClient(client);
    }

    informPropertyAllActionServiceClients();
}

void PropertyManager::removeActionService(const QString& serviceId, bool doDelete)
{
//     bool GlobalChanged = false;

    m_actionServices.remove(serviceId, doDelete);

//    if( GlobalChanged )
//        setGlobalService( -1 );
    informPropertyAllActionServiceClients();
}

void PropertyManager::informPropertyAllActionServiceClients()
{
    // inform clients
    for (auto* client : qAsConst(m_propertyAllActionServiceClients)) {
        client->onAllActionServiceChange();
    }
}

void PropertyManager::registerClient(PropertyAllActionServiceClient* client)
{
    for (auto* service : qAsConst(m_actionServices)) {
        service->registerClient(client, client->itemIndex());
    }

    m_propertyAllActionServiceClients.append(client);
}

void PropertyManager::unregisterClient(PropertyAllActionServiceClient* client)
{
    for (auto* service : qAsConst(m_actionServices)) {
        service->unregisterClient(client, client->itemIndex());
    }

    m_propertyAllActionServiceClients.removeOne(client);
}

/*
   void PropertyManager::setGlobalService( int ServiceId )
   {
    PropertyActionService *NewGlobalService = (ServiceId==-1) ? 0 : m_actionServices[ServiceId];

    // no change?
    if( GlobalService == NewGlobalService )
        return;

    // unregister all defaultclients from old default service
    if( GlobalService )
        for (auto* client : qAsConst(m_allPropertiesGlobalActionServiceClients)) {
            GlobalService->unregisterClient(client);
        }

    // set new default service
    GlobalService = NewGlobalService;

    // register
    if( GlobalService )
        for (auto* client : qAsConst(m_allPropertiesGlobalActionServiceClients)) {
            GlobalService->registerClient(client);
        }

    // inform clients
    for (auto* client : qAsConst(m_allPropertiesGlobalActionServiceClients)) {
        client->onGlobalServiceSwitch(propertyId());
    }
   }


   int PropertyManager::defaultServiceId() const
   {
    return m_actionServices.findIndex( GlobalService );
   }
 */

void PropertyManager::registerClient(AllPropertiesGlobalActionServiceClient* client)
{
    for (auto* service : qAsConst(m_mainActionServices)) {
        service->registerClient(client);
    }

    m_allPropertiesGlobalActionServiceClients.append(client);
}

void PropertyManager::unregisterClient(AllPropertiesGlobalActionServiceClient* client)
{
    for (auto* service : qAsConst(m_mainActionServices)) {
        service->unregisterClient(client);
    }

    m_allPropertiesGlobalActionServiceClients.removeOne(client);
}

void PropertyManager::registerClient(ListAllPropertiesGlobalActionServiceClient* client)
{
    for (auto* service : qAsConst(m_mainActionServices)) {
        auto* listService = qobject_cast<ListPropertyActionService*>(service);
        if (listService) {
            listService->registerClient(client);
        }
    }

    m_listAllPropertiesGlobalActionServiceClients.append(client);
}

void PropertyManager::unregisterClient(ListAllPropertiesGlobalActionServiceClient* client)
{
    for (auto* service : qAsConst(m_mainActionServices)) {
        auto* listService = qobject_cast<ListPropertyActionService*>(service);
        if (listService) {
            listService->unregisterClient(client);
        }
    }

    m_listAllPropertiesGlobalActionServiceClients.removeOne(client);
}

void PropertyManager::addDataActionService(PropertyDataActionService* service,
                                           bool asMain)
{
    m_dataActionServices.append(service);
    if (asMain) {
        m_mainDataActionServices.append(service);
    }
    service->setAdapter(m_adapter);

    for (auto* client : qAsConst(m_propertyAllDataActionServiceClients)) {
        service->registerClient(client);
    }

    informPropertyAllDataActionServiceClients();
}

void PropertyManager::removeDataActionService(const QString& serviceId, bool doDelete)
{
//     bool GlobalChanged = false;
    m_dataActionServices.remove(serviceId, doDelete);

//     if( GlobalChanged )
//         setGlobalDropService( -1 );

    informPropertyAllDataActionServiceClients();
}

void PropertyManager::informPropertyAllDataActionServiceClients()
{
    // inform clients
    for (auto* client : qAsConst(m_propertyAllDataActionServiceClients)) {
        client->onAllDataActionServiceChange();
    }
}

void PropertyManager::registerClient(PropertyAllDataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_dataActionServices)) {
        service->registerClient(client, client->itemIndex());
    }

    m_propertyAllDataActionServiceClients.append(client);
}

void PropertyManager::unregisterClient(PropertyAllDataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_dataActionServices)) {
        service->unregisterClient(client, client->itemIndex());
    }

    m_propertyAllDataActionServiceClients.removeOne(client);
}

void PropertyManager::registerClient(AllPropertiesGlobalDataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_mainDataActionServices)) {
        auto* listService = qobject_cast<ListPropertyDataActionService*>(service);
        if (listService) {
            listService->registerClient(client);
        }
    }

    m_allPropertiesGlobalDataActionServiceClients.append(client);
}

void PropertyManager::unregisterClient(AllPropertiesGlobalDataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_mainDataActionServices)) {
        auto* listService = qobject_cast<ListPropertyDataActionService*>(service);
        if (listService) {
            listService->unregisterClient(client);
        }
    }

    m_allPropertiesGlobalDataActionServiceClients.removeOne(client);
}

void PropertyManager::registerClient(ListAllPropertiesGlobalDataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_mainDataActionServices)) {
        auto* listService = qobject_cast<ListPropertyDataActionService*>(service);
        if (listService) {
            listService->registerClient(client);
        }
    }

    m_listAllPropertiesGlobalDataActionServiceClients.append(client);
}

void PropertyManager::unregisterClient(ListAllPropertiesGlobalDataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_mainDataActionServices)) {
        auto* listService = qobject_cast<ListPropertyDataActionService*>(service);
        if (listService) {
            listService->unregisterClient(client);
        }
    }

    m_listAllPropertiesGlobalDataActionServiceClients.removeOne(client);
}

/*
   void PropertyManager::setGlobalDropService( int DropServiceId )
   {
    PropertyDataActionService *NewGlobalDropService = (DropServiceId==-1) ? 0 : m_dataActionServices[DropServiceId];

    // no change?
    if( GlobalDropService == NewGlobalDropService )
        return;

    // unregister all defaultclients from old default service
    if( GlobalDropService )
        for( GlobalDropServiceClientList::Iterator ClientIt = AllPropertiesMainDataActionServices.begin();
             ClientIt != AllPropertiesMainDataActionServices.end(); ++ClientIt )
            GlobalDropService->unregisterDropClient( *ClientIt );

    // set new default service
    GlobalDropService = NewGlobalDropService;

    // register
    if( GlobalDropService )
        for( GlobalDropServiceClientList::Iterator ClientIt = AllPropertiesMainDataActionServices.begin();
             ClientIt != AllPropertiesMainDataActionServices.end(); ++ClientIt )
            GlobalDropService->registerDropClient( *ClientIt );

    // inform clients
    for( GlobalDropServiceClientList::Iterator ClientIt = AllPropertiesMainDataActionServices.begin();
         ClientIt != AllPropertiesMainDataActionServices.end(); ++ClientIt )
        (*ClientIt)->onGlobalDropServiceSwitch( propertyId() );
   }
 */

void PropertyManager::execute(const KContacts::Addressee& person, int itemIndex, const QMimeData* data,
                              const QString& serviceId) const
{
    PropertyDataActionService* service = m_dataActionServices[serviceId];

    if (service) {
        service->execute(person, itemIndex, data);
    }
}

void PropertyManager::execute(const KContacts::AddresseeList& personList, const QMimeData* data,
                              const QString& serviceId) const
{
    auto* service = qobject_cast<ListPropertyDataActionService*>(m_dataActionServices[serviceId]);

    if (service) {
        service->execute(personList, data);
    }
}

void PropertyManager::setActionServiceSorting(const QStringList& serviceIds)
{
    m_actionServices.setOrder(serviceIds);
    m_mainActionServices.setOrder(serviceIds);
}

void PropertyManager::setDataActionServiceSorting(const QStringList& serviceIds)
{
    m_dataActionServices.setOrder(serviceIds);
    m_mainDataActionServices.setOrder(serviceIds);
}

void PropertyManager::setStatusServiceSorting(const QStringList& serviceIds)
{
    m_statusServices.setOrder(serviceIds);
}

enum
{
    ActionServiceId = 0,
    DataActionServiceId = 1,
    StatusServiceId = 2,
};

void PropertyManager::reloadConfig(int serviceType, const QString& serviceId)
{
    Service* service = nullptr;
    switch (serviceType)
    {
    case ActionServiceId:
        service = m_actionServices[serviceId];
        break;
    case DataActionServiceId:
        service = m_dataActionServices[serviceId];
        break;
    case StatusServiceId:
        service = m_statusServices[serviceId];
    }
    if (service) {
        service->reloadConfig();
    }
}

}
