/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "allpropertiesglobaldataactionserviceclient.hpp"

// lib
#include "propertyadapter.hpp"
#include "services.hpp"
#include "propertydataactionservice.hpp"

namespace Kontinga {

bool AllPropertiesGlobalDataActionServiceClient::serviceAvailableForData() const
{
    const PropertyManagerList& propertyManagers = Services::self()->propertyManagers();

    bool result = false;
    for (const auto* manager : propertyManagers) {
        const PropertyDataActionServiceList& dataActionServices = manager->mainDataActionServices();
        const PropertyAdapter* adapter = manager->propertyAdapter();
        const int itemsSize = adapter->numberOfItems(person());

        for (const auto* service : dataActionServices) {
            for (int i = 0; i < itemsSize; ++i) {
                if (service->isAvailableFor(data(), person(), i)) {
                    result = true;
                    break;
                }
            }
        }
    }

    return result;
}

}
