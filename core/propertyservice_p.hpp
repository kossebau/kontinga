/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2019 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_PROPERTYSERVICE_P_HPP
#define KONTINGA_PROPERTYSERVICE_P_HPP

#include "service_p.hpp"

namespace Kontinga {

class PropertyAdapter;

class PropertyServicePrivate : public ServicePrivate
{
public:
    explicit PropertyServicePrivate(const QVariantList& arguments);

public:
    PropertyAdapter* adapter() const;
    void setAdapter(PropertyAdapter* adapter);

private:
    PropertyAdapter* m_adapter = nullptr;
};

inline PropertyServicePrivate::PropertyServicePrivate(const QVariantList& arguments)
    : ServicePrivate(arguments)
{
}

inline PropertyAdapter* PropertyServicePrivate::adapter() const { return m_adapter; }

inline void PropertyServicePrivate::setAdapter(PropertyAdapter* adapter) { m_adapter = adapter; }

}

#endif
