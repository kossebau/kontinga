/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_SERVICE_HPP
#define KONTINGA_SERVICE_HPP

// lib
#include <kontinga/kontingacore_export.hpp>
// Qt
#include <QObject>
#include <QVariantList>

namespace Kontinga {
class ServicePrivate;

// TODO: show unavailability instead of ignoring? Or both, chosen by service?
/**
 * Base class for all services.
 *
 * A service operates on an item of a given property of a person.
 *
 */
class KONTINGACORE_EXPORT Service : public QObject
{
public:
    enum EntryFlags
    {
        NoFlags = 0,
        /**  */
        ReferItem = 1,
        /** */
        Always = 2,
    };

protected:
    Service(QObject* parent, const QVariantList& arguments);
    Service(QObject* parent, ServicePrivate* d);

public:
    ~Service() override;

public:
    /** this function gets called if the service gets changed
     * reimplement it if your service is configurable
     * The function defaults to a noop.
     * TODO: should services handle this themselves? what if they depend on third parties configuration?
     */
    virtual void reloadConfig();

public:
    /** @return the servicetype unique identifier of the service */
    const QString& id() const;
    bool fitsIn(const QString& context) const;

private:
    const QScopedPointer<class ServicePrivate> d_ptr;
    Q_DECLARE_PRIVATE(Service)

    friend class ActionService;
    friend class DataActionService;
    friend class ListActionService;
    friend class ListDataActionService;
    friend class StatusService;
    friend class PropertyService;
    friend class PropertyStatusService;
    friend class PropertyActionService;
    friend class PropertyDataActionService;
    friend class ListPropertyActionService;
    friend class ListPropertyDataActionService;
};

}

#endif
