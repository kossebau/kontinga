/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_ACTIONSERVICE_HPP
#define KONTINGA_ACTIONSERVICE_HPP

// lib
#include "serviceaction.hpp"
#include "service.hpp"

namespace KContacts {
class Addressee;
}

namespace Kontinga {

class ActionServiceClient;
class ActionServicePrivate;

// TODO: show unavailability instead of ignoring? Or both, chosen by service?
/**
 * Base class for all action services.
 *
 * A service operates on an item of a given property of a person.
 *
 */
class KONTINGACORE_EXPORT ActionService : public Service
{
    Q_OBJECT

protected:
    ActionService(QObject* parent, const QVariantList& arguments);

public:
    ~ActionService() override;

public: // interface
    /** @return action which describes the service */
    virtual ServiceAction action(const KContacts::Addressee& person, int flags = NoFlags) const = 0;

    /** does the service */
    virtual void execute(const KContacts::Addressee& person) = 0;

    /**
     * Reports the general support of the service for the given item of the supported property.
     * Default implementation returns true.
     * @param Person
     * @param ItemIndex index of the item. If -1 the query is for any item
     * @returns if the service is generally possible for the given or any item, otherwise false
     */
    virtual bool supports(const KContacts::Addressee& person) const;

    /**
     * Reports the current availability of the service
     * Default implementation returns true.
     * @returns true if the service is currently available, otherwise false
     */
    virtual bool isAvailable() const;
    /**
     * Reports the current availability of the service for the given item of the supported property.
     * Default implementation returns true.
     * @param Person
     * @returns true if the service is currently available for the item, otherwise false
     */
    virtual bool isAvailableFor(const KContacts::Addressee& person) const;

    /** if ItemIndex = -1 register all items
     * only items for which the service is possible are registered
     * if client data changes it has to unregister and register again
     */
    virtual void registerClient(ActionServiceClient* client);
    virtual void unregisterClient(ActionServiceClient* client);

private:
    Q_DECLARE_PRIVATE(ActionService)
};

}

#endif
