/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_STATUSSERVICECLIENT_HPP
#define KONTINGA_STATUSSERVICECLIENT_HPP

class StatusService;
namespace KContacts {
class Addressee;
}
class QString;
template <class T> class QVector;

namespace Kontinga {

class StatusServiceClient
{
public:
    enum ChangeType
    {
        NoChange = 0,
        Available = 1,
        Unavailable = 2,
        Unknown = 3,
        CustomBase = 1024,
        _Max = 1 << 31
    };

public:
    StatusServiceClient();
    virtual ~StatusServiceClient();

public: // interface
    /** returns the person for which the services are requested */
    virtual const KContacts::Addressee& person() const = 0;

public: // slots interface
    /** called if the service switched */
    virtual void onStateChange(const StatusService& service, int change) = 0;
};

inline StatusServiceClient::StatusServiceClient() = default;
inline StatusServiceClient::~StatusServiceClient() = default;

using StatusServiceClientList = QVector<StatusServiceClient*>;

}

#endif
