/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_PROPERTYMANAGER_HPP
#define KONTINGA_PROPERTYMANAGER_HPP

// lib
#include "idptrvector.hpp"
#include "propertyitem.hpp"
#include "propertyitemadapter.hpp"
#include "propertystatusservice.hpp"
#include "propertyactionservice.hpp"
#include "propertydataactionservice.hpp"
#include "propertystatusserviceclient.hpp"
#include "propertyallactionserviceclient.hpp"
#include "allpropertiesglobalactionserviceclient.hpp"
#include "listallpropertiesglobalactionserviceclient.hpp"
#include "propertyalldataactionserviceclient.hpp"
#include "allpropertiesglobaldataactionserviceclient.hpp"
#include "listallpropertiesglobaldataactionserviceclient.hpp"
// Qt
#include <QVector>
#include <QString>

namespace KContacts {
class Addressee;
class AddresseeList;
}
class QMimeData;

namespace Kontinga {

class PropertyAdapter;

using PropertyStatusServiceList = IdPtrVector<PropertyStatusService>;
using PropertyActionServiceList = IdPtrVector<PropertyActionService>;
using PropertyDataActionServiceList = IdPtrVector<PropertyDataActionService>;
using PropertyAllActionServiceClientList = QVector<PropertyAllActionServiceClient*>;
using PropertyAllDataActionServiceClientList = QVector<PropertyAllDataActionServiceClient*>;

class KONTINGACORE_EXPORT PropertyManager
{
    friend class Services;
    friend class ServicesPrivate;

public:
    explicit PropertyManager(PropertyAdapter* propertyAdapter);
    ~PropertyManager();

public: // status interface
    void registerClient(PropertyStatusServiceClient* client);
    void unregisterClient(PropertyStatusServiceClient* client);

    const PropertyStatusServiceList& statusServices() const;

public: // action service interface
    void registerClient(PropertyAllActionServiceClient* client);
    void unregisterClient(PropertyAllActionServiceClient* client);

    void registerClient(AllPropertiesGlobalActionServiceClient* client);
    void unregisterClient(AllPropertiesGlobalActionServiceClient* client);

    void registerClient(ListAllPropertiesGlobalActionServiceClient* client);
    void unregisterClient(ListAllPropertiesGlobalActionServiceClient* client);

    void execute(const KContacts::Addressee& person, int itemIndex, const QString& serviceId) const;
    void execute(const KContacts::AddresseeList& list, const QString& serviceId) const;

    const PropertyActionServiceList& actionServices() const;
    const PropertyActionServiceList& mainActionServices() const;

public: // data action service interface
    void registerClient(PropertyAllDataActionServiceClient* client);
    void unregisterClient(PropertyAllDataActionServiceClient* client);

    void registerClient(AllPropertiesGlobalDataActionServiceClient* client);
    void unregisterClient(AllPropertiesGlobalDataActionServiceClient* client);

    void registerClient(ListAllPropertiesGlobalDataActionServiceClient* client);
    void unregisterClient(ListAllPropertiesGlobalDataActionServiceClient* client);

    void execute(const KContacts::Addressee& person, int itemIndex, const QMimeData* data, const QString& serviceId) const;
    void execute(const KContacts::AddresseeList& list, const QMimeData* data, const QString& serviceId) const;

    const PropertyDataActionServiceList& dataActionServices() const;
    const PropertyDataActionServiceList& mainDataActionServices() const;

public: // status service interface
    const PropertyAdapter* propertyAdapter() const;
    QString id() const;

public: // management interface
    void addStatusService(PropertyStatusService* statusService);
    void removeStatusService(const QString& statusServiceId, bool doDelete = false);

    void addActionService(PropertyActionService* service, bool AsMain);   // TODO: addActionServices(), whole list in one step?
    // TODO: does ServiceId do it? Better pointer to the service?
    /** deletes the service with the id
     * @param serviceId the id of the service. If -1 all services are deleted
     * @param doDelete If true also delete the removed service(s)
     */
    void removeActionService(const QString& serviceId, bool doDelete = false);

    void addDataActionService(PropertyDataActionService* service, bool asMain);
    void removeDataActionService(const QString& serviceId, bool doDelete = false);

private:
    void setActionServiceSorting(const QStringList& serviceIds);
    void setDataActionServiceSorting(const QStringList& serviceIds);
    void setStatusServiceSorting(const QStringList& serviceIds);

    void reloadConfig(int serviceType, const QString& serviceId);

private:
    void informStatusServiceClients();
    void informPropertyAllActionServiceClients(); // TODO: move to ClientForItemList
    void informPropertyAllDataActionServiceClients();

private:
    /** the adapter for the supported property */
    PropertyAdapter* m_adapter;

    /** */
    PropertyStatusServiceList m_statusServices;
    /** list of all clients interested in status services */
    PropertyStatusServiceClientList m_statusServiceClients;

    /** */
    PropertyActionServiceList m_mainActionServices;
    PropertyActionServiceList m_actionServices;
    /** */
    AllPropertiesGlobalActionServiceClientList m_allPropertiesGlobalActionServiceClients;
    /** */
    PropertyAllActionServiceClientList m_propertyAllActionServiceClients;
    ListAllPropertiesGlobalActionServiceClientList m_listAllPropertiesGlobalActionServiceClients;
    /** */
    PropertyDataActionServiceList m_mainDataActionServices;
    PropertyDataActionServiceList m_dataActionServices;

    /** */
    AllPropertiesGlobalDataActionServiceClientList m_allPropertiesGlobalDataActionServiceClients;
    ListAllPropertiesGlobalDataActionServiceClientList m_listAllPropertiesGlobalDataActionServiceClients;
    /** */
    PropertyAllDataActionServiceClientList m_propertyAllDataActionServiceClients;
};

}

#endif
