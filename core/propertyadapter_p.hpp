/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_PROPERTYADAPTER_P_HPP
#define KONTINGA_PROPERTYADAPTER_P_HPP

// Qt
#include <QVariantList>
#include <QString>

namespace Kontinga {

class PropertyAdapterPrivate
{
public:
    explicit PropertyAdapterPrivate(const QVariantList& arguments);
    virtual ~PropertyAdapterPrivate();

public:
    const QString& id() const;

private:
    const QString m_id;
};

inline PropertyAdapterPrivate::PropertyAdapterPrivate(const QVariantList& arguments)
    : m_id(!arguments.empty() ? arguments[0].toString() : QString())
{}

inline PropertyAdapterPrivate::~PropertyAdapterPrivate() = default;

inline const QString& PropertyAdapterPrivate::id() const { return m_id; }

}

#endif
