/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_LISTALLPROPERTIESGLOBALDATAACTIONSERVICECLIENT_HPP
#define KONTINGA_LISTALLPROPERTIESGLOBALDATAACTIONSERVICECLIENT_HPP

// lib
#include <kontinga/kontingacore_export.hpp>
#include "listpropertydataactionserviceclient.hpp"

template <class C> class QVector;
class QString;

namespace Kontinga {

class KONTINGACORE_EXPORT ListAllPropertiesGlobalDataActionServiceClient : public ListPropertyDataActionServiceClient
{
public:
    ListAllPropertiesGlobalDataActionServiceClient();
    ~ListAllPropertiesGlobalDataActionServiceClient() override;

public:
    /** @return true if at least one service is available for the data */
    bool serviceAvailableForData() const;

public: // slots interface
    /** called if the default switched */
    virtual void onGlobalDataActionServiceSwitch(const QString& propertyId) = 0;
    virtual void onPropertyManagerChange() = 0;
};

inline ListAllPropertiesGlobalDataActionServiceClient::ListAllPropertiesGlobalDataActionServiceClient() = default;
inline ListAllPropertiesGlobalDataActionServiceClient::~ListAllPropertiesGlobalDataActionServiceClient() = default;

using ListAllPropertiesGlobalDataActionServiceClientList = QVector<ListAllPropertiesGlobalDataActionServiceClient*>;

}

#endif
