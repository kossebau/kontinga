/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_LISTPROPERTYDATAACTIONSERVICE_HPP
#define KONTINGA_LISTPROPERTYDATAACTIONSERVICE_HPP

// lib
#include "propertydataactionservice.hpp"

namespace KContacts {
class AddresseeList;
}
class QMimeData;

namespace Kontinga {

class ListPropertyDataActionServiceClient;
class ListPropertyDataActionServicePrivate;

class KONTINGACORE_EXPORT ListPropertyDataActionService : public PropertyDataActionService
{
    Q_OBJECT

public:
    ListPropertyDataActionService(QObject* parent, const QVariantList& arguments);
    ~ListPropertyDataActionService() override;

public: // interface
    virtual ServiceAction action(const KContacts::AddresseeList& personList,
                                 const QMimeData* data, int Flags) const = 0;
    using PropertyDataActionService::action;

    /** does the service on the item with the given index from the supported person type */
    virtual void execute(const KContacts::AddresseeList& personList, const QMimeData* data) = 0;
    using PropertyDataActionService::execute;

    /** @returns if the service is possible for the item; defaults to return true */
    virtual bool supports(const QMimeData* data, const KContacts::AddresseeList& personList) const;
    using PropertyDataActionService::supports;

    /** @returns if the service is available currently for the item; defaults to return true */
    virtual bool isAvailableFor(const QMimeData* data, const KContacts::AddresseeList& personList) const;
    using PropertyDataActionService::isAvailableFor;

    virtual void registerClient(ListPropertyDataActionServiceClient* client);
    virtual void unregisterClient(ListPropertyDataActionServiceClient* client);
    using PropertyDataActionService::registerClient;
    using PropertyDataActionService::unregisterClient;

private:
    Q_DECLARE_PRIVATE(ListPropertyDataActionService)
};

}

#endif
