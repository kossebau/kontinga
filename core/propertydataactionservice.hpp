/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_PROPERTYDATAACTIONSERVICE_HPP
#define KONTINGA_PROPERTYDATAACTIONSERVICE_HPP

// lib
#include "serviceaction.hpp"
#include "propertyservice.hpp"

namespace KContacts {
class Addressee;
}
class QMimeData;

namespace Kontinga {

class PropertyDataActionServiceClient;
class PropertyDataActionServicePrivate;

class KONTINGACORE_EXPORT PropertyDataActionService : public PropertyService
{
    Q_OBJECT

public:
    PropertyDataActionService(QObject* parent, const QVariantList& arguments);
    ~PropertyDataActionService() override;

public: // interface
    virtual ServiceAction action(const KContacts::Addressee& person, int itemIndex,
                                 const QMimeData* data, int flags) const = 0;

    /** does the service on the item with the given index from the supported person type */
    virtual void execute(const KContacts::Addressee& Person, int itemIndex, const QMimeData* data) = 0;

    /** @returns if the service is possible for the item; defaults to return true */
    virtual bool supports(const QMimeData* data) const;
    virtual bool supports(const QMimeData* data, const KContacts::Addressee& person, int itemIndex) const;

    /** @returns if the service is available currently; defaults to return true */
    virtual bool isAvailableFor(const QMimeData* data) const;
    /** @returns if the service is available currently for the item; defaults to return true */
    virtual bool isAvailableFor(const QMimeData* data, const KContacts::Addressee& person, int itemIndex) const;

    /** if ItemIndex = -1 register all items */
    virtual void registerClient(PropertyDataActionServiceClient* client, int itemIndex = -1);
    virtual void unregisterClient(PropertyDataActionServiceClient* client, int itemIndex = -1);

private:
    Q_DECLARE_PRIVATE(PropertyDataActionService)
};

}

#endif
