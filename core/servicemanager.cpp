/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

// lib
#include "listactionservice.hpp"
#include "listdataactionservice.hpp"
#include "servicemanager.hpp"
// Qt
#include <QString>

namespace Kontinga {

ServiceManager::ServiceManager() = default;

ServiceManager::~ServiceManager()
{
    removeActionService(QString(), true);
    removeDataActionService(QString(), true);
//     removeStatusService( QString(), true );
}

// const StatusServiceList &ServiceManager::statusServices()                const { return m_statusServices; }
const ActionServiceList& ServiceManager::actionServices() const
{
    return m_actionServices;
}

const DataActionServiceList& ServiceManager::dataActionServices() const
{
    return m_dataActionServices;
}

void ServiceManager::registerClient(StatusServiceClient* client)
{
    for (auto* service : qAsConst(m_statusServices)) {
        service->registerClient(client);
    }

    m_statusServiceClients.append(client);
}

void ServiceManager::unregisterClient(StatusServiceClient* client)
{
    for (auto* service : qAsConst(m_statusServices)) {
        service->unregisterClient(client);
    }

    m_statusServiceClients.removeAll(client);
}

void ServiceManager::addStatusService(StatusService* statusService)
{
    m_statusServices.append(statusService);

    for (auto* client : qAsConst(m_statusServiceClients)) {
        statusService->registerClient(client);
    }

    informStatusServiceClients();
}

void ServiceManager::removeStatusService(const QString& serviceId, bool doDelete)
{
    m_statusServices.remove(serviceId, doDelete);

    informStatusServiceClients();
}

void ServiceManager::setStatusServiceSorting(const QStringList& serviceIds)
{
    m_statusServices.setOrder(serviceIds);
}

void ServiceManager::informStatusServiceClients()
{
// TODO:
    // inform clients
//     for (auto* client : qAsConst(m_statusServiceClients)) {
//        client->onAllServiceChange();
//     }
}

void ServiceManager::execute(const KContacts::Addressee& person, const QString& serviceId) const
{
    ActionService* service = m_actionServices[serviceId];

    if (service) {
        service->execute(person);
    }
}

void ServiceManager::execute(const KContacts::AddresseeList& list, const QString& serviceId) const
{
    auto* service = qobject_cast<ListActionService*>(m_actionServices[serviceId]);

    if (service) {
        service->execute(list);
    }
}

void ServiceManager::addActionService(ActionService* service /*, bool AsDefault*/)
{
    m_actionServices.append(service);
//     if( AsDefault ) DefaultActionServices[Service->id()] = Service;

    for (auto* client : qAsConst(m_actionServiceClients)) {
        service->registerClient(client);
    }

    auto* listService = qobject_cast<ListActionService*>(service);
    if (listService) {
        for (auto* client : qAsConst(m_listActionServiceClients)) {
            listService->registerClient(client);
        }
    }

    informActionServiceClients();
}

void ServiceManager::removeActionService(const QString& serviceId, bool doDelete)
{
//     bool DefaultChanged = false;

    m_actionServices.remove(serviceId, doDelete);

//    if( DefaultChanged )
//        setDefaultService( -1 );
    informActionServiceClients();
}

void ServiceManager::setActionServiceSorting(const QStringList& serviceIds)
{
    m_actionServices.setOrder(serviceIds);
}

void ServiceManager::informActionServiceClients()
{
#if 0
    // inform clients
    for (auto* client : qAsConst(m_actionServiceClients)) {
        client->onAllActionServiceChange();
    }

#endif
}

void ServiceManager::registerClient(ActionServiceClient* client)
{
    for (auto* service : qAsConst(m_actionServices)) {
        service->registerClient(client);
    }

    m_actionServiceClients.append(client);
}

void ServiceManager::unregisterClient(ActionServiceClient* client)
{
    for (auto* service : qAsConst(m_actionServices)) {
        service->unregisterClient(client);
    }

    m_actionServiceClients.removeAll(client);
}

void ServiceManager::registerClient(ListActionServiceClient* client)
{
    for (auto* service : qAsConst(m_actionServices)) {
        auto* listService = qobject_cast<ListActionService*>(service);
        if (listService) {
            listService->registerClient(client);
        }
    }

    m_listActionServiceClients.append(client);
}

void ServiceManager::unregisterClient(ListActionServiceClient* client)
{
    for (auto* service : qAsConst(m_actionServices)) {
        auto* listService = qobject_cast<ListActionService*>(service);
        if (listService) {
            listService->unregisterClient(client);
        }
    }

    m_listActionServiceClients.removeAll(client);
}

/*
   void ServiceManager::setDefaultService( int ServiceId )
   {
    PropertyActionService *NewDefaultService = (ServiceId==-1) ? 0 : m_actionServices[ServiceId];

    // no change?
    if( DefaultService == NewDefaultService )
        return;

    // unregister all defaultclients from old default service
    if( DefaultService )
        for( DefaultServiceClientList::Iterator ClientIt = AllPropertiesDefaultActionServiceClients.begin();
             ClientIt != AllPropertiesDefaultActionServiceClients.end(); ++ClientIt )
            DefaultService->unregisterClient( *ClientIt );

    // set new default service
    DefaultService = NewDefaultService;

    // register
    if( DefaultService )
        for( DefaultServiceClientList::Iterator ClientIt = AllPropertiesDefaultActionServiceClients.begin();
             ClientIt != AllPropertiesDefaultActionServiceClients.end(); ++ClientIt )
            DefaultService->registerClient( *ClientIt );

    // inform clients
    for( DefaultServiceClientList::Iterator ClientIt = AllPropertiesDefaultActionServiceClients.begin();
         ClientIt != AllPropertiesDefaultActionServiceClients.end(); ++ClientIt )
        (*ClientIt)->onDefaultServiceSwitch( propertyId() );
   }


   int ServiceManager::defaultServiceId() const
   {
    return m_actionServices.findIndex( DefaultService );
   }
 */
#if 0

void ServiceManager::registerClient(AllPropertiesDefaultActionServiceClient* Client)
{
    for (ActionServiceList::Iterator ServiceIt = DefaultActionServices.begin(); ServiceIt != DefaultActionServices.end(); ++ServiceIt) {
        (*ServiceIt)->registerClient(Client);
    }

    AllPropertiesDefaultActionServiceClients.append(Client);
}

void ServiceManager::unregisterClient(AllPropertiesDefaultActionServiceClient* Client)
{
    for (ActionServiceList::Iterator ServiceIt = DefaultActionServices.begin(); ServiceIt != DefaultActionServices.end(); ++ServiceIt) {
        (*ServiceIt)->unregisterClient(Client);
    }

    AllPropertiesDefaultActionServiceClients.remove(Client);
}
#endif

void ServiceManager::addDataActionService(DataActionService* service /*, bool asDefault*/)
{
    m_dataActionServices.append(service);
//     if( AsDefault ) DefaultDataActionServices[Service->id()] = Service;

    for (auto* client : qAsConst(m_dataActionServiceClients)) {
        service->registerClient(client);
    }

    auto* listService = qobject_cast<ListDataActionService*>(service);
    if (listService) {
        for (auto* client : qAsConst(m_listDataActionServiceClients)) {
            listService->registerClient(client);
        }
    }

    informDataActionServiceClients();
}

void ServiceManager::removeDataActionService(const QString& serviceId, bool doDelete)
{
//     bool DefaultChanged = false;

    m_dataActionServices.remove(serviceId, doDelete);

//     if( DefaultChanged )
//         setDefaultDropService( -1 );

    informDataActionServiceClients();
}

void ServiceManager::informDataActionServiceClients()
{
    // inform clients
//     for( PropertyAllDataActionServiceClientList::Iterator ClientIt = PropertyAllDataActionServiceClients.begin();
//          ClientIt != PropertyAllDataActionServiceClients.end(); ++ClientIt )
//         (*ClientIt)->onAllDataActionServiceChange();
}

void ServiceManager::setDataActionServiceSorting(const QStringList& serviceIds)
{
    m_dataActionServices.setOrder(serviceIds);
}

void ServiceManager::registerClient(DataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_dataActionServices)) {
        service->registerClient(client);
    }

    m_dataActionServiceClients.append(client);
}

void ServiceManager::unregisterClient(DataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_dataActionServices)) {
        service->unregisterClient(client);
    }

    m_dataActionServiceClients.removeAll(client);
}

void ServiceManager::registerClient(ListDataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_dataActionServices)) {
        auto* listService = qobject_cast<ListDataActionService*>(service);
        if (listService) {
            listService->registerClient(client);
        }
    }

    m_listDataActionServiceClients.append(client);
}

void ServiceManager::unregisterClient(ListDataActionServiceClient* client)
{
    for (auto* service : qAsConst(m_dataActionServices)) {
        auto* listService = qobject_cast<ListDataActionService*>(service);
        if (listService) {
            listService->unregisterClient(client);
        }
    }

    m_listDataActionServiceClients.removeAll(client);
}

/*
   void ServiceManager::setDefaultDropService( int DropServiceId )
   {
    PropertyDataActionService *NewDefaultDropService = (DropServiceId==-1) ? 0 : m_dataActionServices[DropServiceId];

    // no change?
    if( DefaultDropService == NewDefaultDropService )
        return;

    // unregister all defaultclients from old default service
    if( DefaultDropService )
        for( DefaultDropServiceClientList::Iterator ClientIt = AllPropertiesDefaultDataActionServices.begin();
             ClientIt != AllPropertiesDefaultDataActionServices.end(); ++ClientIt )
            DefaultDropService->unregisterDropClient( *ClientIt );

    // set new default service
    DefaultDropService = NewDefaultDropService;

    // register
    if( DefaultDropService )
        for( DefaultDropServiceClientList::Iterator ClientIt = AllPropertiesDefaultDataActionServices.begin();
             ClientIt != AllPropertiesDefaultDataActionServices.end(); ++ClientIt )
            DefaultDropService->registerDropClient( *ClientIt );

    // inform clients
    for( DefaultDropServiceClientList::Iterator ClientIt = AllPropertiesDefaultDataActionServices.begin();
         ClientIt != AllPropertiesDefaultDataActionServices.end(); ++ClientIt )
        (*ClientIt)->onDefaultDropServiceSwitch( propertyId() );
   }
 */

void ServiceManager::execute(const KContacts::Addressee& person, const QMimeData* data,
                             const QString& serviceId) const
{
    DataActionService* service = m_dataActionServices[serviceId];

    if (service) {
        service->execute(person, data);
    }
}

void ServiceManager::execute(const KContacts::AddresseeList& list, const QMimeData* data,
                             const QString& serviceId) const
{
    auto* service = qobject_cast<ListDataActionService*>(m_dataActionServices[serviceId]);

    if (service) {
        service->execute(list, data);
    }
}

enum
{
    ActionServiceId = 0,
    DataActionServiceId = 1,
    StatusServiceId = 2,
};

void ServiceManager::reloadConfig(int serviceType, const QString& serviceId)
{
    Service* service = nullptr;
    switch (serviceType)
    {
    case ActionServiceId:
        service = m_actionServices[serviceId];
        break;
    case DataActionServiceId:
        service = m_dataActionServices[serviceId];
        break;
    case StatusServiceId:
        service = m_statusServices[serviceId];
    }
    if (service) {
        service->reloadConfig();
    }
}

}
