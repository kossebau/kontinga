/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_LISTACTIONSERVICECLIENT_HPP
#define KONTINGA_LISTACTIONSERVICECLIENT_HPP

namespace KContacts {
class AddresseeList;
}

namespace Kontinga {
class ListActionService;

class ListActionServiceClient
{
public:
    enum ChangeType
    {
        NoChange = 0,
        Available = 1,
        Unavailable = 2,
        Unknown = 3,
        CustomBase = 1024,
        _Max = 1 << 31
    };

public:
    ListActionServiceClient();
    virtual ~ListActionServiceClient();

public: // interface
    /** returns the person for which the services are requested */
    virtual const KContacts::AddresseeList& personList() const = 0;

public: // slots interface
    /** called if the service switched */
    virtual void onActionServiceStateChange(const ListActionService& service, int change) = 0;
};

inline ListActionServiceClient::ListActionServiceClient() = default;
inline ListActionServiceClient::~ListActionServiceClient() = default;

}

#endif
