/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_STATUSCHANGE_HPP
#define KONTINGA_STATUSCHANGE_HPP

// lib
#include "roles.hpp"
#include "statusadapter.hpp"
// Qt
#include <QVariant>
#include <QExplicitlySharedDataPointer>

namespace Kontinga {

class KONTINGACORE_EXPORT StatusChange
{
public:
    StatusChange(StatusAdapter* adapter);

public:
    QVariant data(int role = DisplayTextRole) const;

private:
    QExplicitlySharedDataPointer<StatusAdapter> m_adapter;
};

inline StatusChange::StatusChange(StatusAdapter* adapter) : m_adapter(adapter) {}
inline QVariant StatusChange::data(int role) const { return m_adapter->data(role); }

}

#endif
