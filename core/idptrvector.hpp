/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_IDPTRVECTOR_HPP
#define KONTINGA_IDPTRVECTOR_HPP

// Qt
#include <QVector>
#include <QString>
#include <QStringList>

namespace Kontinga {

template <class T>
class IdPtrVector : public QVector<T*>
{
public:
    IdPtrVector() = default;
    IdPtrVector(const IdPtrVector<T>& other) : QVector<T*>(other) {}

public:
    typename QVector<T*>::Iterator find(const QString& id);
    typename QVector<T*>::ConstIterator constFind(const QString& id) const;
    T* operator[](const QString& id) const;
    void setOrder(const QStringList& ids);
    void remove(const QString& id, bool autoDelete);
};

template <class T>
T* IdPtrVector<T>::operator[](const QString& id) const
{
    T* result = nullptr;
    for (typename QVector<T*>::ConstIterator it = this->begin(); it != this->end(); ++it) {
        if ((*it)->id() == id) {
            result = *it;
            break;
        }
    }

    return result;
}

template <class T>
typename QVector<T*>::Iterator IdPtrVector<T>::find(const QString& id)
{
    typename QVector<T*>::Iterator it = this->begin();
    for (; it != this->end(); ++it) {
        if ((*it)->id() == id) {
            break;
        }
    }

    return it;
}

template <class T>
typename QVector<T*>::ConstIterator IdPtrVector<T>::constFind(const QString& id) const
{
    auto it = this->constBegin();
    for (auto end = this->constEnd(); it != end; ++it) {
        if ((*it)->id() == id) {
            break;
        }
    }

    return it;
}

template <class T>
inline void IdPtrVector<T>::setOrder(const QStringList& ids)
{
    // insert in the order of Ids
    typename QVector<T*>::Iterator startIt = this->begin();
    typename QVector<T*>::Iterator endIt = this->end();
    for (QStringList::ConstIterator sit = ids.begin(); sit != ids.end(); ++sit) {
        // find in the left the matching id and swap to current place
        for (typename QVector<T*>::Iterator it = startIt; it != endIt; ++it) {
            if ((*it)->id() == *sit) {
                if (it != startIt) {
                    qSwap(*it, *startIt);
                }
                ++startIt;
                break;
            }
        }
    }
}

template <class T>
inline void IdPtrVector<T>::remove(const QString& id, bool autoDelete)
{
    // remove all?
    if (id.isNull()) {
        if (autoDelete) {
            for (typename QVector<T*>::Iterator it = this->begin(); it != this->end(); ++it) {
                delete *it;
            }
        }
        this->clear();
    } else {
        typename QVector<T*>::Iterator it = this->find(id);
        T* t = *it;

        this->erase(it);
        if (autoDelete) {
            delete t;
        }
    }
}

}

#endif
