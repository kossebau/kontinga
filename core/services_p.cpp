/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "services_p.hpp"

// lib
#include "propertyadapter.hpp"
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KConfigGroup>
#include <KConfig>
#include <KService>
#include <KServiceTypeTrader>
#include <KSycoca>
// Qt
#include <QDebug>

namespace Kontinga {

static const char DCOPName[] = "Services";
static const char DCOPConfigObjectName[] = "KontingaConfig";
static const char DCOPChangeSignalName[] = "changed()";
static const char DCOPServiceConfigChangeSignalName[] = "changed(int,QString,QString)";

static const char ConfigFileName[] =      "kontingarc";
static const char GeneralGroupId[] =      "General";
static const char PropertiesGroupId[] =   "Properties";
static const char PropertiesSortingId[] = "Sorting";
static const char PropertiesHiddenId[] =  "Hidden";

static const char PropertyGroupIdTemplate[] = "Property:%1";
static const char ActionsSortingId[] =        "ActionsSorting";
static const char ActionsHiddenId[] =         "HiddenActions";
static const char ActionsMainId[] =           "MainActions";
static const char DataActionsSortingId[] =    "DataActionsSorting";
static const char DataActionsHiddenId[] =     "HiddenDataActions";
static const char DataActionsMainId[] =       "MainDataActions";
static const char StatesSortingId[] =         "StatesSorting";
static const char StatesHiddenId[] =          "HiddenStates";

static const char KontingaActionServiceId[] =      "kontinga/actionservice";
static const char KontingaDataActionServiceId[] =  "kontinga/dataactionservice";
static const char KontingaStatusServiceId[] =      "kontinga/statusservice";

static const char ActionServiceNameId[] =     "X-KDE-ActionService";
static const char DataActionServiceNameId[] = "X-KDE-DataActionService";
static const char StatusServiceNameId[] =     "X-KDE-StatusService";

static const char ABIVersionId[] =            "X-KDE-ABI-Version";
static const char OnlyShowInContextId[] =     "X-KDE-OnlyShowInContext";
static const char NotShowInContextId[] =      "X-KDE-NotShowInContext";
static const char CategoriesId[] =            "X-KDE-Categories";

static const char ServiceABIVersion[] = "1";

ServicesPrivate::ServicesPrivate(Services* q)
    :
// DCOPObject( DCOPName ),
    q_ptr(q)
{
//     connectDCOPSignal( 0, DCOPConfigObjectName, DCOPChangeSignalName,
//                        "onConfigChange()", false );
//     connectDCOPSignal( 0, DCOPConfigObjectName, DCOPServiceConfigChangeSignalName,
//                        "onServiceConfigChange(int,QString,QString)", false );

    connect(KSycoca::self(), SIGNAL(databaseChanged()), SLOT(onKSyCoCaChange()));
}

ServicesPrivate::~ServicesPrivate() = default;

void ServicesPrivate::onConfigChange()
{
    m_manager.removeActionService(QString(), true);
    m_manager.removeDataActionService(QString(), true);

    for (auto* manager : qAsConst(propertyManagers)) {
        manager->removeActionService(QString(), true);
        manager->removeDataActionService(QString(), true);
        manager->removeStatusService(QString(), true);
    }

    propertyManagers.clear();// TODO: clear client lists, too?

    load();

    emit q_ptr->changed();
}

void ServicesPrivate::onServiceConfigChange(int serviceType, const QString& propertyId, const QString& serviceId)
{
    if (propertyId.isEmpty()) {
        m_manager.reloadConfig(serviceType, serviceId);
    } else {
        PropertyManager* manager = propertyManagers[propertyId];

        if (manager) {
            manager->reloadConfig(serviceType, serviceId);
        }
    }
}

void ServicesPrivate::loadActionServices(const QStringList& sortingIds,
                                         const QStringList& hiddenIds)
{
    const KService::List serviceOffers = KServiceTypeTrader::self()->query(KontingaActionServiceId);
    for (const auto& service : serviceOffers) {
        const QString serviceId = service->property(QString::fromLatin1(ActionServiceNameId)).toString();
        qDebug() << "ActionService " << serviceId << " found: " << service->library() << endl;

        const QString version = service->property(QString::fromLatin1(ABIVersionId)).toString();
        if (version != QString::fromLatin1(ServiceABIVersion)) {
            qDebug() << "ActionService version incompatible: "
                     << version << "!=" << QString::fromLatin1(ServiceABIVersion) << endl;
            continue;
        }

        if (hiddenIds.contains(serviceId)) {
            qDebug() << "ActionService hidden: " << serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append(serviceId);
        arguments.append(service->property(QString::fromLatin1(OnlyShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));
        arguments.append(service->property(QString::fromLatin1(NotShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));

        ActionService* actionService =
            service->createInstance<ActionService>(nullptr, arguments);

        if (actionService) {
            qDebug() << "Plugin loaded with Arguments: " << arguments << endl;
//             bool AsGlobal = MainActionIds[PropertyId].contains( ServiceId );
            m_manager.addActionService(actionService /*, AsGlobal*/);
        } else {
            qDebug() << "Plugin not loadable: " << serviceId << endl;
        }
    }

    m_manager.setActionServiceSorting(sortingIds);
}

void ServicesPrivate::loadDataActionServices(const QStringList& sortingIds,
                                             const QStringList& hiddenIds)
{
    const KService::List serviceOffers = KServiceTypeTrader::self()->query(KontingaDataActionServiceId);
    for (const auto& service : serviceOffers) {
        const QString serviceId = service->property(QString::fromLatin1(DataActionServiceNameId)).toString();
        qDebug() << "DataActionService " << serviceId << " found: " << service->library() << endl;

        const QString version = service->property(QString::fromLatin1(ABIVersionId)).toString();
        if (version != QString::fromLatin1(ServiceABIVersion)) {
            qDebug() << "DataActionService version incompatible: "
                     << version << "!=" << QString::fromLatin1(ServiceABIVersion) << endl;
            continue;
        }

        if (hiddenIds.contains(serviceId)) {
            qDebug() << "DataActionService hidden: " << serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append(serviceId);
        arguments.append(service->property(QString::fromLatin1(OnlyShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));
        arguments.append(service->property(QString::fromLatin1(NotShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));

        DataActionService* dataActionService =
            service->createInstance<DataActionService>(nullptr, arguments);

        if (dataActionService) {
            qDebug() << "Plugin loaded: " << dataActionService->id() << endl;
//             bool AsGlobal = MainActionIds[PropertyId].contains( ServiceId );
            m_manager.addDataActionService(dataActionService /*, AsGlobal*/);
        } else {
            qDebug() << "Plugin not loadable: " << serviceId << endl;
        }
    }

    m_manager.setDataActionServiceSorting(sortingIds);
}

void ServicesPrivate::loadStatusServices(const QStringList& sortingIds,
                                         const QStringList& hiddenIds)
{
    const KService::List serviceOffers = KServiceTypeTrader::self()->query(KontingaStatusServiceId);
    for (const auto& service : serviceOffers) {
        const QString serviceId = service->property(QString::fromLatin1(StatusServiceNameId)).toString();
        qDebug() << "StatusService " << serviceId << " found: " << service->library() << endl;

        const QString version = service->property(QString::fromLatin1(ABIVersionId)).toString();
        if (version != QString::fromLatin1(ServiceABIVersion)) {
            qDebug() << "StatusService version incompatible: "
                     << version << "!=" << QString::fromLatin1(ServiceABIVersion) << endl;
            continue;
        }

        if (hiddenIds.contains(serviceId)) {
            qDebug() << "StatusService hidden: " << serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append(serviceId);
        arguments.append(service->property(QString::fromLatin1(OnlyShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));
        arguments.append(service->property(QString::fromLatin1(NotShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));

        StatusService* statusService =
            service->createInstance<StatusService>(nullptr, arguments);

        if (statusService) {
            qDebug() << "Plugin loaded: " << statusService->id() << endl;
//             bool AsGlobal = MainActionIds[PropertyId].contains( ServiceId );
            m_manager.addStatusService(statusService /*, AsGlobal*/);
        } else {
            qDebug() << "Plugin not loadable: " << serviceId << endl;
        }
    }

    m_manager.setStatusServiceSorting(sortingIds);
}

// TODO: versioning support, by writing the v no from the header into a call, which gets tested
void ServicesPrivate::load()
{
    KConfig config(ConfigFileName);

    KConfigGroup propertiesConfigGroup = config.group(PropertiesGroupId);
    const QStringList sortedPropertyIds =
        propertiesConfigGroup.readEntry<QStringList>(QString::fromLatin1(PropertiesSortingId), QStringList());
    const QStringList hiddenPropertyIds =
        propertiesConfigGroup.readEntry<QStringList>(QString::fromLatin1(PropertiesHiddenId), QStringList());
    loadProperties(sortedPropertyIds, hiddenPropertyIds);

    const KConfigGroup generalConfigGroup = config.group(GeneralGroupId);
    const QStringList actionSortingIds =
        generalConfigGroup.readEntry<QStringList>(QString::fromLatin1(ActionsSortingId), QStringList());
    const QStringList hiddenActionIds =
        generalConfigGroup.readEntry<QStringList>(QString::fromLatin1(ActionsHiddenId), QStringList());
    loadActionServices(actionSortingIds, hiddenActionIds);
    const QStringList dataActionSortingIds =
        generalConfigGroup.readEntry<QStringList>(QString::fromLatin1(DataActionsSortingId), QStringList());
    const QStringList hiddenDataActionIds =
        generalConfigGroup.readEntry<QStringList>(QString::fromLatin1(DataActionsHiddenId), QStringList());
    loadDataActionServices(dataActionSortingIds, hiddenDataActionIds);
    const QStringList statusSortingIds =
        generalConfigGroup.readEntry<QStringList>(QString::fromLatin1(StatesSortingId), QStringList());
    const QStringList hiddenStatusIds =
        generalConfigGroup.readEntry<QStringList>(QString::fromLatin1(StatesHiddenId), QStringList());
    loadStatusServices(statusSortingIds, hiddenStatusIds);

    QMap<QString, QStringList> actionSortingIdsMap;
    QMap<QString, QStringList> hiddenActionIdsMap;
    QMap<QString, QStringList> mainActionIdsMap;

    QMap<QString, QStringList> dataActionSortingIdsMap;
    QMap<QString, QStringList> hiddenDataActionIdsMap;
    QMap<QString, QStringList> mainDataActionIdsMap;

    QMap<QString, QStringList> statesSortingIdsMap;
    QMap<QString, QStringList> hiddenStatesIdsMap;

    for (auto* manager : qAsConst(propertyManagers)) {
        const QString& propertyId = manager->id();

        const KConfigGroup propertyConfigGroup =
            config.group(QString::fromLatin1(PropertyGroupIdTemplate).arg(propertyId));

        actionSortingIdsMap.insert(propertyId,
            propertyConfigGroup.readEntry<QStringList>(QString::fromLatin1(ActionsSortingId), QStringList()));
        hiddenActionIdsMap.insert(propertyId,
            propertyConfigGroup.readEntry<QStringList>(QString::fromLatin1(ActionsHiddenId), QStringList()));
        mainActionIdsMap.insert(propertyId,
            propertyConfigGroup.readEntry<QStringList>(QString::fromLatin1(ActionsMainId), QStringList()));

        dataActionSortingIdsMap.insert(propertyId,
            propertyConfigGroup.readEntry<QStringList>(QString::fromLatin1(DataActionsSortingId), QStringList()));
        hiddenDataActionIdsMap.insert(propertyId,
            propertyConfigGroup.readEntry<QStringList>(QString::fromLatin1(DataActionsHiddenId), QStringList()));
        mainDataActionIdsMap.insert(propertyId,
            propertyConfigGroup.readEntry<QStringList>(QString::fromLatin1(DataActionsMainId), QStringList()));

        statesSortingIdsMap.insert(propertyId,
            propertyConfigGroup.readEntry<QStringList>(QString::fromLatin1(StatesSortingId), QStringList()));
        hiddenStatesIdsMap.insert(propertyId,
            propertyConfigGroup.readEntry<QStringList>(QString::fromLatin1(StatesHiddenId), QStringList()));
    }

    loadActionServices(actionSortingIdsMap, hiddenActionIdsMap, mainActionIdsMap);
    loadDataActionServices(dataActionSortingIdsMap, hiddenDataActionIdsMap, mainDataActionIdsMap);
    loadStatusServices(statesSortingIdsMap, hiddenStatesIdsMap);
}

void ServicesPrivate::loadProperties(const QStringList& sortedPropertyIds, const QStringList& hiddenPropertyIds)
{
    const KService::List serviceOffers = KServiceTypeTrader::self()->query("kontinga/property");
    for (const auto& service : serviceOffers) {
        qDebug() << "Property found: " << service->library() << endl;

        const QString propertyId = service->property(QString::fromLatin1("X-KDE-KontingaProperty")).toString();

        const QString version = service->property(QString::fromLatin1(ABIVersionId)).toString();
        if (version != QString::fromLatin1(ServiceABIVersion)) {
            qDebug() << "Property version incompatible: "
                     << version << "!=" << QString::fromLatin1(ServiceABIVersion) << endl;
            continue;
        }

        if (hiddenPropertyIds.contains(propertyId)) {
            qDebug() << "Property hidden: " << propertyId << endl;
        } else {
            QVariantList arguments;
            arguments.append(propertyId);
            PropertyAdapter* propertyAdapter =
                service->createInstance<PropertyAdapter>(nullptr, arguments);

            if (propertyAdapter) {
                qDebug() << "Property loaded: " << propertyAdapter->id() << endl;
                propertyManagers.append(new PropertyManager(propertyAdapter));
            } else {
                qDebug() << "Property not loadable: " << propertyId << endl;
            }
        }
    }

    propertyManagers.setOrder(sortedPropertyIds);
}

void ServicesPrivate::loadActionServices(const QMap<QString, QStringList>& actionSortingIds,
                                         const QMap<QString, QStringList>& HiddenActionIds,
                                         const QMap<QString, QStringList>& mainActionIds)
{
    const KService::List serviceOffers = KServiceTypeTrader::self()->query("kontinga/propertyactionservice");
    for (const auto& service : serviceOffers) {
        const QString propertyId = service->property(QString::fromLatin1("X-KDE-KontingaProperty")).toString();
        const QString serviceId = service->property(QString::fromLatin1("X-KDE-ActionService")).toString();
        qDebug() << "ActionService " << serviceId << " found: " << service->library() << " for " << propertyId << endl;

        const QString version = service->property(QString::fromLatin1(ABIVersionId)).toString();
        if (version != QString::fromLatin1(ServiceABIVersion)) {
            qDebug() << "ActionService version incompatible: "
                     << version << "!=" << QString::fromLatin1(ServiceABIVersion) << endl;
            continue;
        }

        auto managerIt = propertyManagers.constFind(propertyId);
        if (managerIt == propertyManagers.constEnd()) {
            qDebug() << "Property not loaded: " << propertyId << endl;
            continue;
        }
        if (HiddenActionIds[propertyId].contains(serviceId)) {
            qDebug() << "ActionService hidden: " << serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append(serviceId);
        arguments.append(service->property(QString::fromLatin1(OnlyShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));
        arguments.append(service->property(QString::fromLatin1(NotShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));

        PropertyActionService* propertyActionService =
            service->createInstance<PropertyActionService>(nullptr, arguments);

        if (propertyActionService) {
            // hacky
            const bool asMain =
                actionSortingIds[propertyId].contains(serviceId) ?   // already configured?
                mainActionIds[propertyId].contains(serviceId) :
                !service->property(QString::fromLatin1(CategoriesId)).toStringList().isEmpty();       // abuse categories
            (*managerIt)->addActionService(propertyActionService, asMain);
            qDebug() << (asMain ? "Plugin loaded as Main: " : "Plugin loaded: ") << propertyActionService->id() << endl;
        } else {
            qDebug() << "Plugin not loadable: " << serviceId << endl;
        }
    }

    for (auto* manager : qAsConst(propertyManagers)) {
        manager->setActionServiceSorting(actionSortingIds[manager->id()]);
    }
}

void ServicesPrivate::loadDataActionServices(const QMap<QString, QStringList>& dataActionSortingIds,
                                             const QMap<QString, QStringList>& hiddenDataActionIds,
                                             const QMap<QString, QStringList>& mainDataActionIds)
{
    const KService::List serviceOffers = KServiceTypeTrader::self()->query("kontinga/propertydataactionservice");
    for (const auto& service : serviceOffers) {
        const QString propertyId = service->property(QString::fromLatin1("X-KDE-KontingaProperty")).toString();
        const QString serviceId = service->property(QString::fromLatin1("X-KDE-DataActionService")).toString();
        qDebug() << "DataActionService " << serviceId << " found: " << service->library() << " for " << propertyId << endl;

        const QString version = service->property(QString::fromLatin1(ABIVersionId)).toString();
        if (version != QString::fromLatin1(ServiceABIVersion)) {
            qDebug() << "DataActionService version incompatible: "
                     << version << "!=" << QString::fromLatin1(ServiceABIVersion) << endl;
            continue;
        }

        const auto managerIt = propertyManagers.constFind(propertyId);
        if (managerIt == propertyManagers.constEnd()) {
            qDebug() << "Property not loaded: " << propertyId << endl;
            continue;
        }
        if (hiddenDataActionIds[propertyId].contains(serviceId)) {
            qDebug() << "DataActionService hidden: " << serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append(serviceId);
        arguments.append(service->property(QString::fromLatin1(OnlyShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));
        arguments.append(service->property(QString::fromLatin1(NotShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));

        PropertyDataActionService* propertyDataActionService =
            service->createInstance<PropertyDataActionService>(nullptr, arguments);

        if (propertyDataActionService) {
            // hacky
            const bool asMain =
                dataActionSortingIds[propertyId].contains(serviceId) ?   // already configured?
                mainDataActionIds[propertyId].contains(serviceId) :
                !service->property(QString::fromLatin1(CategoriesId)).toStringList().isEmpty();       // abuse categories
            (*managerIt)->addDataActionService(propertyDataActionService, asMain);
            qDebug() << (asMain ? "Plugin loaded as Main: " : "Plugin loaded: ") << propertyDataActionService->id() << endl;
        } else {
            qDebug() << "Plugin not loadable: " << serviceId << endl;
        }
    }

    for (auto* manager : qAsConst(propertyManagers)) {
        manager->setDataActionServiceSorting(dataActionSortingIds[manager->id()]);
    }
}

void ServicesPrivate::loadStatusServices(const QMap<QString, QStringList>& statesSortingIds,
                                         const QMap<QString, QStringList>& hiddenStatesIds)
{
    const KService::List serviceOffers = KServiceTypeTrader::self()->query("kontinga/propertystatusservice");
    for (const auto& service : serviceOffers) {
        const QString propertyId = service->property(QString::fromLatin1("X-KDE-KontingaProperty")).toString();
        const QString serviceId = service->property(QString::fromLatin1("X-KDE-StatusService")).toString();
        qDebug() << "StatusService " << serviceId << " found: " << service->library() << " for " << propertyId << endl;

        const QString version = service->property(QString::fromLatin1(ABIVersionId)).toString();
        if (version != QString::fromLatin1(ServiceABIVersion)) {
            qDebug() << "StatusService version incompatible: "
                     << version << "!=" << QString::fromLatin1(ServiceABIVersion) << endl;
            continue;
        }

        const auto managerIt = propertyManagers.constFind(propertyId);
        if (managerIt == propertyManagers.constEnd()) {
            qDebug() << "Property not loaded: " << propertyId << endl;
            continue;
        }
        if (hiddenStatesIds[propertyId].contains(serviceId)) {
            qDebug() << "StatusService hidden: " << serviceId << endl;
            continue;
        }

        QVariantList arguments;
        arguments.append(serviceId);
        arguments.append(service->property(QString::fromLatin1(OnlyShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));
        arguments.append(service->property(QString::fromLatin1(NotShowInContextId)).toString().split(";", QString::SkipEmptyParts).join(";"));

        PropertyStatusService* propertyStatusService =
            service->createInstance<PropertyStatusService>(nullptr, arguments);

        if (propertyStatusService) {
            qDebug() << "StatusService loaded: " << propertyStatusService->id() << endl;
            (*managerIt)->addStatusService(propertyStatusService);
        } else {
            qDebug() << "StatusService not loadable: " << serviceId << endl;
        }
    }

    for (auto* manager : qAsConst(propertyManagers)) {
        manager->setStatusServiceSorting(statesSortingIds[manager->id()]);
    }
}

void ServicesPrivate::addManager(PropertyManager* manager)
{
    propertyManagers.append(manager);

    for (auto* client : qAsConst(statusServiceClients)) {
        manager->registerClient(client);
    }

    for (auto* client : qAsConst(globalActionServiceClients)) {
        manager->registerClient(client);
    }

    informStatusServiceClients();
    informAllPropertiesGlobalActionServiceClients();
}

void ServicesPrivate::removeManager(const QString& propertyId, bool doDelete)
{
    propertyManagers.remove(propertyId, doDelete);

    informStatusServiceClients();
    informAllPropertiesGlobalActionServiceClients();
}

void ServicesPrivate::informStatusServiceClients()
{
    // inform clients
    for (auto* client : qAsConst(statusServiceClients)) {
        client->onPropertyManagerChange();
    }
}

void ServicesPrivate::informAllPropertiesGlobalActionServiceClients()
{
    // inform clients
    for (auto* client : qAsConst(globalActionServiceClients)) {
        client->onPropertyManagerChange();
    }
}

void ServicesPrivate::onKSyCoCaChange()
{
#if 0
    if (KSycoca::self()->isChanged("services")) {
        // ksycoca has new KService objects for us, make sure to update
        // our 'copies' to be in sync with it. Not important for OK, but
        // important for Apply (how to differentiate those 2?).
        // See BR 35071.
        QValueList<TypesListItem*>::Iterator it = m_itemsModified.begin();
        for (; it != m_itemsModified.end(); ++it) {
            QString name = (*it)->name();
            if (removedList.find(name) == removedList.end()) { // if not deleted meanwhile
                (*it)->refresh();
            }
        }

        m_itemsModified.clear();
    }
#endif
}

}
