/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "propertyadapter.hpp"
#include "propertyadapter_p.hpp"

// KDEPIM
#include <KContacts/AddresseeList>
#include <KContacts/Addressee>

namespace Kontinga {

PropertyAdapter::PropertyAdapter(QObject* parent, const QVariantList& arguments)
    : QObject(parent)
    , d_ptr(new PropertyAdapterPrivate(arguments))
{}

PropertyAdapter::~PropertyAdapter() = default;

QString PropertyAdapter::id() const
{
    Q_D(const PropertyAdapter);

    return d->id();
}

bool PropertyAdapter::haveProperty(const KContacts::AddresseeList& personList) const
{
    bool result = false;
    for (const auto& person : personList) {
        if (numberOfItems(person) != 0) {
            result = true;
            break;
        }
    }

    return result;
}

}
