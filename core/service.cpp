/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "service.hpp"
#include "service_p.hpp"

namespace Kontinga {

Service::Service(QObject* parent, const QVariantList& arguments)
    : QObject(parent)
    , d_ptr(new ServicePrivate(arguments))
{}

Service::Service(QObject* parent, ServicePrivate* d)
    : QObject(parent)
    , d_ptr(d)
{}


Service::~Service() = default;

void Service::reloadConfig() {}

const QString& Service::id()  const
{
    Q_D(const Service);

    return d->id();
}

bool Service::fitsIn(const QString& context) const
{
    Q_D(const Service);

    return d->fitsIn(context);
}

}
