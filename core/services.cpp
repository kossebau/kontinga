/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "services.hpp"
#include "services_p.hpp"

// Qt
#include <QGlobalStatic>

namespace Kontinga {

class ServicesSingleton
{
public:
    Services self;
};

Q_GLOBAL_STATIC(ServicesSingleton, servicesSelf)

Services * Services::self()
{
    return &servicesSelf->self;
}

Services::Services()
    : d_ptr(new ServicesPrivate(this))
{
    Q_D(Services);

    d->load();
}

Services::~Services() = default;

const ServiceManager& Services::serviceManager() const
{
    Q_D(const Services);

    return d->serviceManager();
}

const PropertyManagerList& Services::propertyManagers() const
{
    Q_D(const Services);

    return d->propertyManagers;
}

void Services::registerClient(StatusServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().registerClient(client);
}

void Services::unregisterClient(StatusServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().unregisterClient(client);
}

void Services::registerClient(ActionServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().registerClient(client);
}

void Services::unregisterClient(ActionServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().unregisterClient(client);
}

void Services::registerClient(ListActionServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().registerClient(client);
}

void Services::unregisterClient(ListActionServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().unregisterClient(client);
}

void Services::execute(const KContacts::Addressee& person, const QString& serviceId)
{
    Q_D(Services);

    d->serviceManager().execute(person, serviceId);
}
void Services::execute(const KContacts::AddresseeList& personList, const QString& serviceId)
{
    Q_D(Services);

    d->serviceManager().execute(personList, serviceId);
}

void Services::registerClient(DataActionServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().registerClient(client);
}

void Services::unregisterClient(DataActionServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().unregisterClient(client);
}
void Services::registerClient(ListDataActionServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().registerClient(client);
}

void Services::unregisterClient(ListDataActionServiceClient* client)
{
    Q_D(Services);

    d->serviceManager().unregisterClient(client);
}

void Services::execute(const KContacts::Addressee& person,
                       const QString& serviceId, const QMimeData* data)
{
    Q_D(Services);

    d->serviceManager().execute(person, data, serviceId);
}
void Services::execute(const KContacts::AddresseeList& personList,
                       const QString& serviceId, const QMimeData* data)
{
    Q_D(Services);

    d->serviceManager().execute(personList, data, serviceId);
}

void Services::registerClient(PropertyStatusServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->registerClient(client);
    }

    d->statusServiceClients.append(client);
}

void Services::unregisterClient(PropertyStatusServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->unregisterClient(client);
    }

    d->statusServiceClients.removeOne(client);
}

void Services::registerClient(AllPropertiesGlobalActionServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->registerClient(client);
    }

    d->globalActionServiceClients.append(client);
}

void Services::unregisterClient(AllPropertiesGlobalActionServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->unregisterClient(client);
    }

    d->globalActionServiceClients.removeOne(client);
}

void Services::registerClient(ListAllPropertiesGlobalActionServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->registerClient(client);
    }

    d->listGlobalActionServiceClients.append(client);
}

void Services::unregisterClient(ListAllPropertiesGlobalActionServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->unregisterClient(client);
    }

    d->listGlobalActionServiceClients.removeOne(client);
}

void Services::registerClient(PropertyAllActionServiceClient* client)
{
    Q_D(Services);

    PropertyManager* manager = d->propertyManagers[client->propertyId()];

    if (manager) {
        manager->registerClient(client);
    }
}

void Services::unregisterClient(PropertyAllActionServiceClient* client)
{
    Q_D(Services);

    PropertyManager* manager = d->propertyManagers[client->propertyId()];

    if (manager) {
        manager->unregisterClient(client);
    }
}

void Services::execute(const KContacts::Addressee& person, const QString& propertyId, int itemIndex,
                       const QString& serviceId)
{
    Q_D(Services);

    PropertyManager* manager = d->propertyManagers[propertyId];

    if (manager) {
        manager->execute(person, itemIndex, serviceId);
    }
}

void Services::execute(const KContacts::AddresseeList& personList, const QString& propertyId,
                       const QString& serviceId)
{
    Q_D(Services);

    PropertyManager* manager = d->propertyManagers[propertyId];

    if (manager) {
        manager->execute(personList, serviceId);
    }
}

void Services::registerClient(AllPropertiesGlobalDataActionServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->registerClient(client);
    }

    d->globalDataActionServiceClients.append(client);
}

void Services::unregisterClient(AllPropertiesGlobalDataActionServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->unregisterClient(client);
    }

    d->globalDataActionServiceClients.removeOne(client);
}

void Services::registerClient(ListAllPropertiesGlobalDataActionServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->registerClient(client);
    }

    d->listGlobalDataActionServiceClients.append(client);
}

void Services::unregisterClient(ListAllPropertiesGlobalDataActionServiceClient* client)
{
    Q_D(Services);

    for (auto* manager : qAsConst(d->propertyManagers)) {
        manager->unregisterClient(client);
    }

    d->listGlobalDataActionServiceClients.removeOne(client);
}

void Services::registerClient(PropertyAllDataActionServiceClient* client)
{
    Q_D(Services);

    PropertyManager* manager = d->propertyManagers[client->propertyId()];

    if (manager) {
        manager->registerClient(client);
    }
}

void Services::unregisterClient(PropertyAllDataActionServiceClient* client)
{
    Q_D(Services);

    PropertyManager* manager = d->propertyManagers[client->propertyId()];

    if (manager) {
        manager->unregisterClient(client);
    }
}

void Services::execute(const KContacts::Addressee& person, const QString& propertyId, int itemIndex,
                       const QString& serviceId, const QMimeData* data)
{
    Q_D(Services);

    PropertyManager* manager = d->propertyManagers[propertyId];

    if (manager) {
        manager->execute(person, itemIndex, data, serviceId);
    }
}

void Services::execute(const KContacts::AddresseeList& personList, const QString& propertyId,
                       const QString& serviceId, const QMimeData* data)
{
    Q_D(Services);

    PropertyManager* manager = d->propertyManagers[propertyId];

    if (manager) {
        manager->execute(personList, data, serviceId);
    }
}

}
