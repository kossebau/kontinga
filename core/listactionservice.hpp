/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_LISTACTIONSERVICE_HPP
#define KONTINGA_LISTACTIONSERVICE_HPP

// lib
#include "actionservice.hpp"

namespace KContacts {
class AddresseeList;
}

namespace Kontinga {
class ListActionServiceClient;
class ListActionServicePrivate;

class KONTINGACORE_EXPORT ListActionService : public ActionService
{
    Q_OBJECT

protected:
    ListActionService(QObject* parent, const QVariantList& arguments);

public:
    ~ListActionService() override;

public: // interface
    /** @return action which describes the service */
    virtual ServiceAction action(const KContacts::AddresseeList& personList, int flags = NoFlags) const = 0;
    using ActionService::action;

    /** does the service */
    virtual void execute(const KContacts::AddresseeList& personList) = 0;
    using ActionService::execute;

    /**
     * Reports the general support of the service for the given item of the supported property.
     * Default implementation returns true.
     * @param PersonList
     * @returns if the service is generally possible for the given or any item, otherwise false
     */
    virtual bool supports(const KContacts::AddresseeList& personList) const;
    using ActionService::supports;

    /**
     * Reports the current availability of the service for the given item of the supported property.
     * Default implementation returns true.
     * @param PersonList
     * @returns true if the service is currently available for the item, otherwise false
     */
    virtual bool isAvailableFor(const KContacts::AddresseeList& personList) const;
    using ActionService::isAvailableFor;

    /**
     * only items for which the service is possible are registered
     * if client data changes it has to unregister and register again
     */
    virtual void registerClient(ListActionServiceClient* client);
    virtual void unregisterClient(ListActionServiceClient* client);
    using ActionService::registerClient;
    using ActionService::unregisterClient;

private:
    Q_DECLARE_PRIVATE(ListActionService)
};

}

#endif
