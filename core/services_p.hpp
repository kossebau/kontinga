/*
    This file is part of the Kontinga Core library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_SERVICES_P_HPP
#define KONTINGA_SERVICES_P_HPP

// lib
#include "services.hpp"
#include "propertystatusserviceclient.hpp"
#include "propertyallactionserviceclient.hpp"
#include "propertyalldataactionserviceclient.hpp"
#include "listallpropertiesglobalactionserviceclient.hpp"
#include "listallpropertiesglobaldataactionserviceclient.hpp"
#include "allpropertiesglobalactionserviceclient.hpp"
#include "allpropertiesglobaldataactionserviceclient.hpp"
// Qt
#include <QObject>

class KConfig;

namespace Kontinga {

class ServicesPrivate : public QObject// , virtual public DCOPObject
{
//     K_DCOP
    Q_OBJECT

public:
    ServicesPrivate(Services*);
    ~ServicesPrivate() override;

public:
// k_dcop:
    void onConfigChange();
    void onServiceConfigChange(int serviceType, const QString& propertyId, const QString& serviceId);

public:
    ServiceManager& serviceManager();
    const ServiceManager& serviceManager() const;

public:
    void informStatusServiceClients();
    void informAllPropertiesGlobalActionServiceClients();

public:
    void load();
    void loadActionServices(const QStringList& sortingIds, const QStringList& hiddenIds);
    void loadDataActionServices(const QStringList& sortingIds, const QStringList& hiddenIds);
    void loadStatusServices(const QStringList& sortingIds, const QStringList& hiddenIds);
    void loadProperties(const QStringList& sortedPropertyIds, const QStringList& hiddenPropertyIds);
    void loadActionServices(const QMap<QString, QStringList>& actionSortingIds,
                            const QMap<QString, QStringList>& hiddenActionIds,
                            const QMap<QString, QStringList>& mainActionIds);
    void loadDataActionServices(const QMap<QString, QStringList>& dataActionSortingIds,
                                const QMap<QString, QStringList>& hiddenDataActionIds,
                                const QMap<QString, QStringList>& mainDataActionIds);
    void loadStatusServices(const QMap<QString, QStringList>& statesSortingIds,
                            const QMap<QString, QStringList>& hiddenStatesIds);

    void addManager(PropertyManager* manager);
    void removeManager(const QString& propertyId, bool doDelete = false);

public:
    /** */
    PropertyManagerList propertyManagers;

    PropertyStatusServiceClientList statusServiceClients;
    AllPropertiesGlobalActionServiceClientList globalActionServiceClients;
    ListAllPropertiesGlobalActionServiceClientList listGlobalActionServiceClients;
    AllPropertiesGlobalDataActionServiceClientList globalDataActionServiceClients;
    ListAllPropertiesGlobalDataActionServiceClientList listGlobalDataActionServiceClients;

private Q_SLOTS:
    void onKSyCoCaChange();

private:
    Services* const q_ptr;

    ServiceManager m_manager;
};

inline ServiceManager& ServicesPrivate::serviceManager() { return m_manager; }
inline const ServiceManager& ServicesPrivate::serviceManager() const { return m_manager; }

}

#endif
