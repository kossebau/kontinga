function(_kontinga_add_plugin _name)
    set(options
    )
    set(oneValueArgs
        METADATA
        TYPE
    )
    set(multiValueArgs
        SOURCES
        LINK_LIBRARIES
    )
    cmake_parse_arguments(ARGS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

    if (ARGS_TYPE STREQUAL "PROPERTY")
        set(_target "kontingaproperty_${_name}")
    elseif (ARGS_TYPE STREQUAL "ACTIONSERVICE")
        set(_target "kontingaactionservice_${_name}")
    elseif (ARGS_TYPE STREQUAL "PROPERTYACTIONSERVICE")
        set(_target "kontingapropertyactionservice_${_name}")
    elseif (ARGS_TYPE STREQUAL "PROPERTYDATAACTIONSERVICE")
        set(_target "kontingapropertydataactionservice_${_name}")
    elseif (ARGS_TYPE STREQUAL "PROPERTYSTATUSSERVICE")
        set(_target "kontingapropertystatusservice_${_name}")
    elseif (ARGS_TYPE STREQUAL "PROPERTYACTIONSERVICECONFIG")
        set(_target "kontingapropertyactionserviceconfig_${_name}")
    else()
        message(FATAL_ERROR "_kontinga_add_plugin() called with unknown TYPE \"${ARGS_TYPE}\".")
    endif()

    add_library(${_target} MODULE ${ARGS_SOURCES})
    target_link_libraries( ${_target}
        KontingaCore
        ${ARGS_LINK_LIBRARIES}
    )

    install(TARGETS ${_target}  DESTINATION ${KDE_INSTALL_PLUGINDIR})

    install(FILES ${ARGS_METADATA}  DESTINATION  ${KDE_INSTALL_KSERVICES5DIR})
endfunction()


function(kontinga_add_property _name)
    _kontinga_add_plugin(${_name} TYPE PROPERTY ${ARGN})
endfunction()

function(kontinga_add_actionservice _name)
    _kontinga_add_plugin(${_name} TYPE ACTIONSERVICE ${ARGN})
endfunction()

function(kontinga_add_propertyactionservice _name)
    _kontinga_add_plugin(${_name} TYPE PROPERTYACTIONSERVICE ${ARGN})
endfunction()

function(kontinga_add_propertydataactionservice _name)
    _kontinga_add_plugin(${_name} TYPE PROPERTYDATAACTIONSERVICE ${ARGN})
endfunction()

function(kontinga_add_propertystatusservice _name)
    _kontinga_add_plugin(${_name} TYPE PROPERTYSTATUSSERVICE ${ARGN})
endfunction()

function(kontinga_add_propertyactionserviceconfig _name)
    _kontinga_add_plugin(${_name} TYPE PROPERTYACTIONSERVICECONFIG ${ARGN})
endfunction()
