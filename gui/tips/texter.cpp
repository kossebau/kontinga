/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "texter.hpp"

// lib
#include "tip.hpp"
// #include "person.hpp"
// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/PropertyStatusService>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KIconLoader>
// Qt
#include <QImage>

namespace Kontinga {
static const char PersonPictureProtocol[] = "personpicture";

#if 0
class PersonMimeSourceFactory : public QMimeSourceFactory
{
public:
    const QMimeSource* data(const QString& AbsName) const;
};

const QMimeSource* PersonMimeSourceFactory::data(const QString& AbsName) const
{
    // extract and decode arguments
    QStringList Arguments = QStringList::split(QChar(':'), AbsName);
    QImageDrag* Source = 0;

    if (Arguments[0] == QString::fromLatin1(PersonPictureProtocol)) {
        if (Arguments.size() >= 1) { // 2 )
            KContacts::Addressee Person = KContacts::StdAddressBook::self()->findByUid(Arguments[1]);
            if (!Person.isEmpty()) {
                // get some icon to display in front
                KContacts::Picture ABCPicture = Person.photo();
                if (ABCPicture.data().isNull()) {
                    ABCPicture = Person.logo();
                }

                // any picture?
                if (ABCPicture.isIntern() && !ABCPicture.data().isNull()) {
                    Source = new QImageDrag(ABCPicture.data()); // TODO: Size aus Arguments[2].toInt() lesen!
                } else {
                    Source = new QImageDrag(KGlobal::iconLoader()->loadIcon("personal", KIcon::NoGroup, 50).convertToImage());
                }

            }
        } else {
            kdDebug() << QString::fromLatin1(PersonPictureProtocol) << ": insufficient information in AbsName: " << Arguments << endl;
        }
    }

    return Source;
}
#endif

// static PersonMimeSourceFactory *personMimeSourceFactory = 0;

#if 0
Texter* Texter::self()
{
    if (texter == 0) {
        personTexterDeleter.setObject(texter, new Texter());
    }
    return texter;
}
#endif

Texter::Texter() = default;
Texter::~Texter() = default;

QString Texter::createImageUrl(const KContacts::Addressee& person /*, int Size*/)
{
    // ensure facefactory
//   if( personMimeSourceFactory == 0 )
    {
//     personMimeSourceFactory = new PersonMimeSourceFactory;
//     QMimeSourceFactory::addFactory( personMimeSourceFactory );
    }

    return QString::fromLatin1(PersonPictureProtocol) + QChar(':') + person.uid();// + QChar(':') + QString::number(Size);
}

static const int defaultStyle = ShowFullName | ShowFaceBehind /*| TwoRows*/;

/*
   static QString createName( const KContacts::Addressee &Person, int NameStyle, bool UseTwoRows )
   {
   QString S;

   QString Name = NameStyle&ShowFullName ? User.fullName() : User.loginName();
   QString Other = NameStyle==FirstLoginName ? User.fullName() : NameStyle==FirstFullName ? User.loginName()
                  : QString();
   if( UseTwoRows )
    S += QString::fromLatin1( "<td><nobr>" );

   S += Name;

   if( !Other.isNull() )
   {
    if( UseTwoRows )
      S += QString::fromLatin1("<br>(");
    else
      S += QString::fromLatin1(" (" );
    S += Other;
    S += QChar( ')' );
   }

   if( UseTwoRows )
    S += QString::fromLatin1( "</nobr></td>" );
   return S;
   }
 */
static void fill(Tip* tip, const KContacts::Addressee& person)
{
    const QString& uid = person.uid();
    const PropertyManagerList& managers = Services::self()->propertyManagers();

    for (const auto* manager : managers) {
        const PropertyAdapter* adapter = manager->propertyAdapter();
        const QString& propertyId = adapter->id();
        const PropertyStatusServiceList& statusServices = manager->statusServices();

        // skip entries without status
        if (statusServices.isEmpty()) {
            continue;
        }

        const int itemSize = adapter->numberOfItems(person);
        for (int itemIndex = 0; itemIndex < itemSize; ++itemIndex) {
            const QString itemId = QString::number(itemIndex);
            // get status
            QString statusText;
            for (const auto* statusService : statusServices) {
                if (statusService->supports(person, itemIndex)) {
                    const Status status = statusService->status(person, itemIndex, 0);
                    QString text = status.data(DisplayTextRole).toString();
                    const QImage icon = status.data(DisplayIconRole).value<QImage>();

                    if (!icon.isNull()) {
                        const QString iconId = QString::fromLatin1("%1_tipicon_%2_%3_%4")
                                         .arg(statusService->id(), uid, propertyId, itemId);
//                         QMimeSourceFactory::defaultFactory()->setImage( IconId, Icon );
                        text = QString::fromLatin1("<img src=\"%1\">&nbsp;%2").arg(iconId, text);
                    }

                    if (!text.isEmpty()) {
                        statusText.append(text);
                    }
                }
            }

            // add to card
            if (!statusText.isEmpty()) {
                // get data
                const PropertyItem propertyItem = adapter->propertyItemOf(person, itemIndex);
                const QString text = propertyItem.data(DisplayTextRole).toString();

                tip->appendStatus(text, statusText);
            }
        }
    }
}

QString Texter::createTip(const KContacts::Addressee& person, int style, bool complete)
{
    if (style < 0) {
        style = defaultStyle;
    }

    const QString imageURL = createImageUrl(person);
    /*QString ImageURL = QString::fromLatin1( "person_image_%1" ).arg( Person.uid() );
       KContacts::Picture Photo = Person.photo();
       if ( Photo.isIntern() && !Photo.data().isNull() ) // TODO: care for extern pictures
        QMimeSourceFactory::defaultFactory()->setImage( ImageURL, Photo.data() );
       else
        QMimeSourceFactory::defaultFactory()->setPixmap( ImageURL,
            KGlobal::iconLoader()->loadIcon("personal",KIcon::NoGroup,50) );
     */
    Tip tip;
    tip.initiate(complete);

    tip.appendHeader(imageURL, person.realName(), person.role(), person.organization());

    fill(&tip, person);

    tip.finalize(complete);

    return tip.data();
}
#if 0
QString Texter::createTip(const KContacts::Addressee& person, const QString& text, int Style, bool complete)
{
    const QString imageURL = createImageUrl(person);
    Tip tip;
    tip.initiate(complete);

    tip.appendHeader(imageURL, person.realName(), person.role(), person.organization());

    tip.appendStatus(text);

    tip.finalize(complete);

    return tip.data();
}
#endif
}
