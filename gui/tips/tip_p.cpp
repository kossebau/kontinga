/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "tip_p.hpp"

namespace Kontinga {

void TipPrivate::initiate(bool complete)
{
    if (complete) {
        m_data += QString::fromLatin1("<qt>");
    }

    m_data += QString::fromLatin1(
        "<table cellpadding=\"0\" cellspacing=\"1\">"
        "<tr><td align=\"right\" valign=\"top\">");
}

void TipPrivate::finalize(bool complete)
{
    m_data += QString::fromLatin1(
        "</td></tr>"
        "</table>");

    if (complete) {
        m_data += QString::fromLatin1("</qt>");
    }
}

void TipPrivate::appendHeader(const QString& imageURL, const QString& realName,
                              const QString& role, const QString& organizationName)
{
    const QString headTemplate = QString::fromLatin1(
        "<img src=\"%1\" width=\"50\"></td><td align=\"left\" valign=\"top\">"
        "<font size=\"+1\"><b>%2</b></font>%3<br>");

    const QString lineBreak = QString::fromLatin1("<br>");
    QString text;
    if (!role.isEmpty()) {
        text += lineBreak + role;
    }
    if (!organizationName.isEmpty()) {
        text += lineBreak + organizationName;
    }

    m_data.append(headTemplate.arg(imageURL, realName, text));
}

void TipPrivate::appendStatus(const QString& entry, const QString& status)
{
    const QString text = status.isEmpty() ?
                      QString::fromLatin1("<br>%1").arg(entry) :
                      QString::fromLatin1("<br>%1<br><font size=\"-1\">%2</font>").arg(entry, status);

    m_data.append(text);
}

}
