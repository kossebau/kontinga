/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "person.hpp"

// lib
#include "tip.hpp"
// Kontinga Core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KStaticDeleter>
#include <KIconLoader>
// Qt
#include <QImage>
#include <QMimeData>

static const char* PersonPictureProtocol = "personpicture";

namespace Kontinga {

class PersonMimeSourceFactory : public QMimeSourceFactory
{
public:
    const QMimeSource* data(const QString& AbsName) const;
};

const QMimeSource* PersonMimeSourceFactory::data(const QString& AbsName) const
{
    // extract and decode arguments
    QStringList Arguments = QStringList::split(QChar(':'), AbsName);
    QImageDrag* Source = 0;

    if (Arguments[0] == QString::fromLatin1(PersonPictureProtocol)) {
        if (Arguments.size() >= 1) { // 2 )
            KABC::Addressee Person = KABC::StdAddressBook::self()->findByUid(Arguments[1]);
            if (!Person.isEmpty()) {
                // get some icon to display in front
                KABC::Picture ABCPicture = Person.photo();
                if (ABCPicture.data().isNull()) {
                    ABCPicture = Person.logo();
                }

                // any picture?
                if (ABCPicture.isIntern() && !ABCPicture.data().isNull()) {
                    Source = new QImageDrag(ABCPicture.data()); // TODO: Size aus Arguments[2].toInt() lesen!
                } else {
                    Source = new QImageDrag(KGlobal::iconLoader()->loadIcon("personal", KIcon::NoGroup, 50).convertToImage());
                }

            }
        } else {
            kdDebug() << QString::fromLatin1(PersonPictureProtocol) << ": insufficient information in AbsName: " << Arguments << endl;
        }
    }

    return Source;
}

static PersonMimeSourceFactory* personMimeSourceFactory = 0;

static RichTexter* texter = 0;
static KStaticDeleter<RichTexter> personRichTexterDeleter;

RichTexter* RichTexter::self()
{
    if (texter == 0) {
        personRichTexterDeleter.setObject(texter, new RichTexter());
    }
    return texter;
}

RichTexter::RichTexter() = default;
RichTexter::~RichTexter() = default;

QString RichTexter::createImageURL(const KABC::Addressee& Person /*, int Size*/) const
{
    // ensure facefactory
    if (personMimeSourceFactory == 0) {
        personMimeSourceFactory = new PersonMimeSourceFactory;
        QMimeSourceFactory::addFactory(personMimeSourceFactory);
    }

    return QString::fromLatin1(PersonPictureProtocol) + QChar(':') + Person.uid();// + QChar(':') + QString::number(Size);
}

static const int DefaultStyle = ShowFullName | ShowFaceBehind /*| TwoRows*/;

/*
   static QString createName( const KABC::Addressee &Person, int NameStyle, bool UseTwoRows )
   {
   QString S;

   QString Name = NameStyle&ShowFullName ? User.fullName() : User.loginName();
   QString Other = NameStyle==FirstLoginName ? User.fullName() : NameStyle==FirstFullName ? User.loginName()
                  : QString();
   if( UseTwoRows )
    S += QString::fromLatin1( "<td><nobr>" );

   S += Name;

   if( !Other.isNull() )
   {
    if( UseTwoRows )
      S += QString::fromLatin1("<br>(");
    else
      S += QString::fromLatin1(" (" );
    S += Other;
    S += QChar( ')' );
   }

   if( UseTwoRows )
    S += QString::fromLatin1( "</nobr></td>" );
   return S;
   }
 */
static void fill(Tip* PersonTip, const KABC::Addressee& Person)
{
    const QString& UID = Person.uid();
    const PropertyManagerList& Managers = Services::self()->propertyManagers();

    PropertyManagerList::ConstIterator ManagerIt = Managers.begin();
    for (; ManagerIt != Managers.end(); ++ManagerIt) {
        const PropertyAdapter* Adapter = (*ManagerIt)->propertyAdapter();
        const QString& PropertyId = Adapter->id();
        const PropertyStatusServiceList& StatusServices = (*ManagerIt)->statusServices();

        // skip entries without status
        if (StatusServices.isEmpty()) {
            continue;
        }

        const int ItemSize = Adapter->numberOfItems(Person);
        for (int ItemIndex = 0; ItemIndex < ItemSize; ++ItemIndex) {
            QString ItemId = QString::number(ItemIndex);
            // get status
            QString StatusText;
            PropertyStatusServiceList::ConstIterator StatusServicesIt = StatusServices.begin();
            for (; StatusServicesIt != StatusServices.end(); ++StatusServicesIt) {
                if ((*StatusServicesIt)->supports(Person, ItemIndex)) {
                    Status Status = (*StatusServicesIt)->status(Person, ItemIndex, 0);
                    QString Text = Status.data(DisplayTextRole).asString();
                    QImage Icon = Status.data(DisplayIconRole).asImage();

                    if (!Icon.isNull()) {
                        QString IconId = QString::fromLatin1("%1_tipicon_%2_%3_%4")
                                         .arg((*StatusServicesIt)->id(), UID, PropertyId, ItemId);
                        QMimeSourceFactory::defaultFactory()->setImage(IconId, Icon);
                        Text = QString::fromLatin1("<img src=\"%1\">&nbsp;%2").arg(IconId, Text);
                    }

                    if (!Text.isNull()) {
                        StatusText.append(Text);
                    }
                }
            }

            // add to card
            if (!StatusText.isEmpty()) {
                // get data
                PropertyItem PropertyItem = Adapter->propertyItemOf(Person, ItemIndex);
                QString Entry = PropertyItem.data(DisplayTextRole).asString();

                PersonTip->appendStatus(Entry, StatusText);
            }
        }
    }
}

QString RichTexter::createTip(const KABC::Addressee& Person, int Style, bool Complete) const
{
    if (Style < 0) {
        Style = DefaultStyle;
    }

    QString ImageURL = createImageURL(Person);
    /*QString ImageURL = QString::fromLatin1( "person_image_%1" ).arg( Person.uid() );
       KABC::Picture Photo = Person.photo();
       if ( Photo.isIntern() && !Photo.data().isNull() ) // TODO: care for extern pictures
        QMimeSourceFactory::defaultFactory()->setImage( ImageURL, Photo.data() );
       else
        QMimeSourceFactory::defaultFactory()->setPixmap( ImageURL,
            KGlobal::iconLoader()->loadIcon("personal",KIcon::NoGroup,50) );
     */
    Tip PersonTip;
    PersonTip.initiate(Complete);

    PersonTip.appendHeader(ImageURL, Person.realName(), Person.role(), Person.organization());

    fill(&PersonTip, Person);

    PersonTip.finalize(Complete);

    return PersonTip.data();
}

QString RichTexter::createTip(const KABC::Addressee& Person, const QString& Text, int Style, bool Complete) const
{
    QString ImageURL = createImageURL(Person);
    Tip PersonTip;
    PersonTip.initiate(Complete);

    PersonTip.appendHeader(ImageURL, Person.realName(), Person.role(), Person.organization());

    PersonTip.appendStatus(Text);

    PersonTip.finalize(Complete);

    return PersonTip.data();
}

}
