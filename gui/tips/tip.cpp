/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "tip.hpp"
#include "tip_p.hpp"

namespace Kontinga {

Tip::Tip()
    : d_ptr(new TipPrivate())
{}

Tip::~Tip() = default;

QString Tip::data() const
{
    Q_D(const Tip);

    return d->data();
}

void Tip::initiate(bool complete)
{
    Q_D(Tip);

    d->initiate(complete);
}

void Tip::appendHeader(const QString& imageURL, const QString& realName,
                       const QString& role, const QString& organizationName)
{
    Q_D(Tip);

    d->appendHeader(imageURL, realName, role, organizationName);
}

void Tip::appendStatus(const QString& entry, const QString& status)
{
    Q_D(Tip);

    d->appendStatus(entry, status);
}

void Tip::finalize(bool complete)
{
    Q_D(Tip);

    d->finalize(complete);
}

}
