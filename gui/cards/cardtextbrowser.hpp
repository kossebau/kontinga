/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_CARDTEXTBROWSER_HPP
#define KONTINGA_CARDTEXTBROWSER_HPP

// KDEPIM
#include <KContacts/Addressee>
// Qt
#include <QTextBrowser>

namespace Kontinga {

class GlobalServiceClientCardProxy;
class PropertyAllActionServiceMenuFiller;
class PropertyAllDataActionServiceMenuFiller;
class AllPropertiesGlobalActionServiceMenuFiller;
class AllPropertiesGlobalDataActionServiceMenuFiller;
class StatusClientCardProxy;

class CardTextBrowser : public QTextBrowser
{
    Q_OBJECT

public:
    explicit CardTextBrowser(QWidget* parent = nullptr);
    ~CardTextBrowser() override;

public: // QTextBrowser API
    void setSource(const QUrl& source) override;

public:
    void setPerson(const KContacts::Addressee& person);
    const KContacts::Addressee& person() const;
    QMenu* createStandardContextMenu(QPoint pos);

public Q_SLOTS: // TODO: who uses this?
    void updateView();

protected: // QAbstractScrollArea API
    void dragEnterEvent(QDragEnterEvent* event) override;
    void dragLeaveEvent(QDragLeaveEvent* event) override;
    void dragMoveEvent(QDragMoveEvent* event) override;
    void dropEvent(QDropEvent* event) override;

private:
    KContacts::Addressee m_person;

    GlobalServiceClientCardProxy* m_globalServicesProxy;

    PropertyAllActionServiceMenuFiller* m_propertyServices;
    AllPropertiesGlobalActionServiceMenuFiller* m_services;

    PropertyAllDataActionServiceMenuFiller* m_propertyDropServices;
    AllPropertiesGlobalDataActionServiceMenuFiller* m_dropServices;

    StatusClientCardProxy* m_statusProxy;
};

inline const KContacts::Addressee& CardTextBrowser::person() const { return m_person; }

}

#endif
