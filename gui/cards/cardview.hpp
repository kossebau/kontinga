/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_CARDVIEW_HPP
#define KONTINGA_CARDVIEW_HPP

// lib
#include <kontinga/kontingagui_export.hpp>
// Qt
#include <QWidget>

namespace KContacts {
class Addressee;
}

class QMenu;

namespace Kontinga {
class CardViewPrivate;

/**
 * View of a person with his properties and its' default services.
 * Offers also access to all services for each property
 */
class KONTINGAGUI_EXPORT CardView : public QWidget
{
    Q_OBJECT

public:
    explicit CardView(QWidget* parent = nullptr);
    ~CardView() override;

public:
    void setPerson(const KContacts::Addressee& person);
    const KContacts::Addressee& person() const;

    QMenu* createStandardContextMenu(QPoint pos);

Q_SIGNALS:
    void personChanged();

protected: // QWidget API
    void contextMenuEvent(QContextMenuEvent* event) override;

private:
    const QScopedPointer<class CardViewPrivate> d_ptr;
    Q_DECLARE_PRIVATE(CardView)
};

}
#endif
