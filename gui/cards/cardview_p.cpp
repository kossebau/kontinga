/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "cardview_p.hpp"

// lib
#include "card.hpp"
#include "cardtextbrowser.hpp"
// Qt
#include <QVBoxLayout>
#include <QMenu>
#include <QContextMenuEvent>

namespace Kontinga {

CardViewPrivate::CardViewPrivate(CardView* parent)
    : q_ptr(parent)
{
    Q_Q(CardView);

    auto* layout = new QVBoxLayout(q);
    layout->setContentsMargins(0, 0, 0, 0);
    m_textBrowser = new CardTextBrowser(q);
    m_textBrowser->setContextMenuPolicy(Qt::NoContextMenu);
    layout->addWidget(m_textBrowser);
}

CardViewPrivate::~CardViewPrivate() = default;

const KContacts::Addressee& CardViewPrivate::person() const { return m_textBrowser->person(); }

void CardViewPrivate::setPerson(const KContacts::Addressee& person)
{
    Q_Q(CardView);

    if (m_textBrowser->person() == person) {
        return;
    }

    m_textBrowser->setPerson(person);
    emit q->personChanged();
}

QMenu* CardViewPrivate::createStandardContextMenu(QPoint pos)
{
    return m_textBrowser->createStandardContextMenu(pos);
}

void CardViewPrivate::contextMenuEvent(QContextMenuEvent* event)
{
    Q_Q(CardView);

    QMenu* menu = m_textBrowser->createStandardContextMenu(event->pos());

    menu->exec(event->globalPos());

    delete menu;
}

}
