/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "cardtextbrowser.hpp"

// lib
#include "card.hpp"
#include <propertyallactionservicemenufiller.hpp>
#include <propertyalldataactionservicemenufiller.hpp>
#include <allpropertiesglobalactionservicemenufiller.hpp>
#include <allpropertiesglobaldataactionservicemenufiller.hpp>
// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/AllPropertiesGlobalActionServiceClient>
#include <Kontinga/StatusServiceClient>
#include <Kontinga/PropertyStatusServiceClient>
#include <Kontinga/PropertyActionService>
#include <Kontinga/PropertyStatusService>
// KF
#include <KIconLoader>
// Qt
#include <QMenu>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QDragEnterEvent>

namespace Kontinga {
static const int FaceWidth = 50;
static const int FaceHeight = 70;

class GlobalServiceClientCardProxy : public AllPropertiesGlobalActionServiceClient
{
public:
    explicit GlobalServiceClientCardProxy(CardTextBrowser* view) : m_view(view) {}
    ~GlobalServiceClientCardProxy() override = default;

public: // interface
    /** returns the person for which the services are requested */
    const KContacts::Addressee& person() const override { return m_view->person(); }

public: // slots interface
    /** called if the service switched */
    void onActionServiceStateChange(const PropertyActionService& service, int change, int itemIndex) override;
    void onPropertyManagerChange() override { m_view->updateView(); }
    void onGlobalActionServiceSwitch(const QString& propertyId) override { Q_UNUSED(propertyId); m_view->updateView(); }

private:
    CardTextBrowser* const m_view;
};

void GlobalServiceClientCardProxy::onActionServiceStateChange(const PropertyActionService& service, int change,
                                                              int itemIndex)
{
    Q_UNUSED(service);
    Q_UNUSED(change);
    Q_UNUSED(itemIndex);

    m_view->updateView();
}


class StatusClientCardProxy : public PropertyStatusServiceClient
{
public:
    explicit StatusClientCardProxy(CardTextBrowser* view) : m_view(view) {}
    ~StatusClientCardProxy() override = default;

public: // StatusClient interface
    const KContacts::Addressee& person() const override { return m_view->person(); }
    void onStateChange(const PropertyStatusService& Service, const StatusChange& Change,
                       const Status& NewStatus, int ItemIndex) override;
    void onPropertyManagerChange() override { m_view->updateView(); }

private:
    CardTextBrowser* const m_view;
};

void StatusClientCardProxy::onStateChange(const PropertyStatusService& /*Service*/,
                                          const StatusChange& change, const Status& status, int itemIndex)
{
    Q_UNUSED(change);
    Q_UNUSED(status);
    Q_UNUSED(itemIndex);

    m_view->updateView();
}


CardTextBrowser::CardTextBrowser(QWidget* parent)
    : QTextBrowser(parent)
    , m_globalServicesProxy(new GlobalServiceClientCardProxy(this))
    , m_propertyServices(new PropertyAllActionServiceMenuFiller())
    , m_services(new AllPropertiesGlobalActionServiceMenuFiller())
    , m_propertyDropServices(new PropertyAllDataActionServiceMenuFiller())
    , m_dropServices(new AllPropertiesGlobalDataActionServiceMenuFiller())
    , m_statusProxy(new StatusClientCardProxy(this))
{
    setWordWrapMode(QTextOption::WordWrap);
    // TODO: find replacement for setLinkUnderline( false );
    // setVScrollBarMode( QScrollView::AlwaysOff );
    // setHScrollBarMode( QScrollView::AlwaysOff );

    // styleSheet().item( "a" )->setColor( KGlobalSettings::linkColor() );
    setFrameStyle(QTextBrowser::NoFrame);

    setAcceptDrops(true);
}

CardTextBrowser::~CardTextBrowser()
{
    Services::self()->unregisterClient(m_globalServicesProxy);
    Services::self()->unregisterClient(m_statusProxy);

    delete m_globalServicesProxy;
    delete m_propertyServices;
    delete m_propertyDropServices;
    delete m_services;
    delete m_dropServices;
    delete m_statusProxy;
}

void CardTextBrowser::setPerson(const KContacts::Addressee& person)
{
    Services::self()->unregisterClient(m_globalServicesProxy);
    Services::self()->unregisterClient(m_statusProxy);

    m_person = person;

    Services::self()->registerClient(m_statusProxy);
    Services::self()->registerClient(m_globalServicesProxy);

    updateView();
}

void CardTextBrowser::setSource(const QUrl& source)
{
    const QStringList data = source.toString().split(':', QString::KeepEmptyParts);

    Services::self()->execute(m_person, data[1], data[2].toInt(), data[0]);
}

QMenu* CardTextBrowser::createStandardContextMenu(QPoint pos)
{
    // for card content
    QMenu* menu = QTextBrowser::createStandardContextMenu(pos);

    // add kde icon
//    int MenuId = menu->idAt( 0 );
//    menu->changeItem( MenuId, SmallIconSet("editcopy"), menu->text(MenuId) );

    menu->addSeparator();

    // add type services
    const QString link = anchorAt(pos);

    if (!link.isEmpty()) {
        const QStringList data = link.split(':', QString::KeepEmptyParts);
        m_propertyServices->set(m_person, data[1], data[2].toInt());

        m_propertyServices->fillMenu(menu);
    } else {
        m_services->set(m_person);
        m_services->fillMenu(menu);
    }

    return menu;
}

static QString createLink(const QString& propertyId, const QString& text, int id, const QString& serviceId)
{
    const QString link = QString::fromLatin1("%1:%2:%3").arg(serviceId, propertyId, QString::number(id));
    return QString::fromLatin1("<a %1=\"%2\">%3</a>").arg(
        QString::fromLatin1(serviceId.isEmpty() ? "name" : "href"), link, text);
}

static void fill(Card* card, const KContacts::Addressee& m_person)
{
//     const QString& uid = m_person.uid();
    const PropertyManagerList& managers = Services::self()->propertyManagers();

    for (const auto* manager : managers) {
        const PropertyAdapter* adapter = manager->propertyAdapter();
        const PropertyActionServiceList& actionServices = manager->mainActionServices();
        const PropertyStatusServiceList& statusServices = manager->statusServices();
        const QString& propertyId = adapter->id();

        const int itemSize = adapter->numberOfItems(m_person);
        for (int itemIndex = 0; itemIndex < itemSize; ++itemIndex) {
            const QString itemId = QString::number(itemIndex);
            // get data
            const PropertyItem propertyItem = adapter->propertyItemOf(m_person, itemIndex);
            const QString labelText = propertyItem.data(LabelTextRole).toString();
            QString itemText = propertyItem.data(DisplayTextRole).toString();

            // find first global action
            QString actionServiceId;
#if 0
// TODO: before we searched for the first service that supported the item (better for fax or phone with number)
            if (ActionService->isAvailableFor(m_person, ItemIndex)) {
                ActionServiceId = ActionService->id();
            }
#endif
            for (const auto* actionService : actionServices) {
                if (actionService->supports(m_person, itemIndex)) {
                    // get id if it's available
                    if (actionService->isAvailableFor(m_person, itemIndex)) {
                        actionServiceId = actionService->id();
                    }
                    break;
                }
            }

            itemText = createLink(propertyId, itemText, itemIndex, actionServiceId);

            // get status
            QString statusText;
            for (const auto* statusService : statusServices) {
                if (!statusService->supports(m_person, itemIndex)) {
                    continue;
                }

                const Status status = statusService->status(m_person, itemIndex, 0);
                const QString text = status.data(DisplayTextRole).toString();
                const QImage icon = status.data(DisplayIconRole).value<QImage>();

                // enrich text with icon
                if (!icon.isNull()) {
//                     QString IconId = QString::fromLatin1( "%1_tipicon_%2_%3_%4" )
//                       .arg( statusService->id(), uid, PropertyId, ItemId );
//                     QMimeSourceFactory::defaultFactory()->setImage( IconId, Icon );
//                     Text = QString::fromLatin1( "<img src=\"%1\">&nbsp;%2" ).arg( IconId, Text );
                }

                statusText.append(text);
            }

            // add to card
            card->appendItem(labelText, itemText, statusText);
        }
    }
}

void CardTextBrowser::updateView()
{
    QString text;

    if (!m_person.isEmpty()) {
        Card card;

        card.initiate();

        const QUrl imageUrl(QString(QLatin1String("kontingapersonimage://") + m_person.uid()));
        QVariant resource;
        const KContacts::Picture picture = m_person.photo();
        if (picture.isIntern() && !picture.data().isNull()) { // TODO: care for extern pictures
            resource = picture.data().scaled(FaceWidth, FaceHeight, Qt::KeepAspectRatio);
        } else {
            resource = KIconLoader::global()->loadIcon("personal", KIconLoader::NoGroup, FaceWidth);
        }

        document()->addResource(QTextDocument::ImageResource, imageUrl, resource);

        card.appendHeader(imageUrl.url(), m_person.realName(), m_person.role(), m_person.organization());

        fill(&card, m_person);

        card.finalize();

        text = card.data();
    }

    setText(text);
}

void CardTextBrowser::dragEnterEvent(QDragEnterEvent* event)
{
    // if( CardDropServices->canHandle(Event) )
    event->accept();
}

void CardTextBrowser::dragLeaveEvent(QDragLeaveEvent* event)
{
    Q_UNUSED(event);
}

void CardTextBrowser::dragMoveEvent(QDragMoveEvent* event)
{
    bool isDropable;

    // update drop options
    const QString link = anchorAt(event->pos());
    const QMimeData* data = event->mimeData();
    if (!link.isEmpty()) {
        const QStringList linkData = link.split(':', QString::KeepEmptyParts);
        m_propertyDropServices->set(m_person, linkData[1], linkData[2].toInt(), data);
        isDropable = m_propertyDropServices->serviceAvailableForData();
    } else {
        m_dropServices->set(m_person, data);
        isDropable = m_dropServices->serviceAvailableForData();
    }

    event->setAccepted(isDropable);
}

void CardTextBrowser::dropEvent(QDropEvent* event)
{
    // for card content
    QMenu menu;

    // add type services
    const QString link = anchorAt(event->pos());
    const QMimeData* data = event->mimeData();

    if (!link.isEmpty()) {
        const QStringList linkData = link.split(':', QString::KeepEmptyParts);
        m_propertyDropServices->set(m_person, linkData[1], linkData[2].toInt(), data);
        m_propertyDropServices->fillMenu(&menu);
    } else {
        m_dropServices->set(m_person, data);
        m_dropServices->fillMenu(&menu);
    }

    if (!menu.isEmpty()) {
//         menu.insertSeparator();
//         menu.insertItem( SmallIcon("cancel"), i18n("&Cancel") );

        menu.exec(mapToGlobal(event->pos()));
    }
}

}
