/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_ICON_P_HPP
#define KONTINGA_ICON_P_HPP

// lib
#include "icon.hpp"
// Qt
#include <QPixmap>
#include <QImage>

namespace Kontinga {
static const int maxSymbolCount = 3;
static const int maxSymbolSize = 32;

class IconPrivate
{
public:
    explicit IconPrivate(int size);

public:
    QPixmap data() const;
    int symbolSize() const;

public:
    void appendIcon(const QImage& icon);
    void appendSymbol(const QImage& symbol);
    void fill(const KContacts::Addressee& person);

private:
    QImage m_data;
    /** contains the number of added symbols */
    int m_symbolCount;
    int m_symbolSize;
};

inline IconPrivate::IconPrivate(int size)
    : m_data(size, size, QImage::Format_ARGB32)
    , m_symbolCount(0)
{
    m_data.fill(0);

    m_symbolSize = size / maxSymbolCount;
    if (m_symbolSize > maxSymbolSize) {
        m_symbolSize = maxSymbolSize;
    }
}

inline QPixmap IconPrivate::data() const { return QPixmap::fromImage(m_data); }
inline int IconPrivate::symbolSize() const { return m_symbolSize; }

}

#endif
