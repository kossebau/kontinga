/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "icon.hpp"
#include "icon_p.hpp"

namespace Kontinga {

Icon::Icon(int size)
    : d_ptr(new IconPrivate(size))
{
}

Icon::~Icon() = default;

void Icon::appendIcon(const QImage& image)
{
    Q_D(Icon);

    d->appendIcon(image);
}

void Icon::appendSymbol(const QImage& symbol)
{
    Q_D(Icon);

    d->appendSymbol(symbol);
}

void Icon::fill(const KContacts::Addressee& person)
{
    Q_D(Icon);

    d->fill(person);
}

}
