/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_ICON_HPP
#define KONTINGA_ICON_HPP

// lib
#include <kontinga/kontingagui_export.hpp>
// Qt
#include <QScopedPointer>

namespace KContacts {
class Addressee;
}
class QPixmap;
class QImage;

namespace Kontinga {
class IconPrivate;

class KONTINGAGUI_EXPORT Icon
{
public:
    explicit Icon(int size);
    ~Icon();

public:
    QPixmap data() const;
    int symbolSize() const;

public:
    void appendIcon(const QImage& icon);
    void appendSymbol(const QImage& symbol);
    void fill(const KContacts::Addressee& person);

private:
    const QScopedPointer<class IconPrivate> d_ptr;
    Q_DECLARE_PRIVATE(Icon)
};

}

#endif
