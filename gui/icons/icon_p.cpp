/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "icon_p.hpp"

// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/PropertyStatusService>
// Qt
#include <QPainter>

namespace Kontinga {

void IconPrivate::appendIcon(const QImage& _icon)
{
    QImage icon = _icon;

    if (icon.size() != m_data.size()) {
        icon = icon.scaled(m_data.size(), Qt::KeepAspectRatio);
    }

    const int x = (m_data.width() - icon.width()) / 2;
    const int y = (m_data.height() - icon.height()) / 2;

    QPainter painter(&m_data);
    painter.drawImage(x, y, icon);
}

void IconPrivate::appendSymbol(const QImage& _symbol)
{
    ++m_symbolCount;

    QImage symbol = _symbol;
    if (symbol.size() != QSize(m_symbolSize, m_symbolSize)) {
        symbol = symbol.scaled(m_symbolSize, m_symbolSize, Qt::KeepAspectRatio);
    }

    QPainter painter(&m_data);
    painter.drawImage(m_data.width() - m_symbolSize, m_data.height() - m_symbolCount * m_symbolSize, symbol);
}

void IconPrivate::fill(const KContacts::Addressee& person)
{
    const PropertyManagerList& managers = Services::self()->propertyManagers();

    for (const auto* manager : managers) {
        const PropertyAdapter* adapter = manager->propertyAdapter();
        const PropertyStatusServiceList& statusServices = manager->statusServices();

        // skip entries without status
        if (statusServices.isEmpty()) {
            continue;
        }

        const int itemSize = adapter->numberOfItems(person);
        for (int itemIndex = 0; itemIndex < itemSize; ++itemIndex) {
            // get status
            QString Status;
            for (const auto* statusService : statusServices) {
                if (statusService->supports(person, itemIndex)) {
                    const QImage symbol =
                        statusService->status(person, itemIndex, 0).data(DisplayIconRole).value<QImage>();
                    if (!symbol.isNull()) {
                        appendSymbol(symbol);
                    }
                }
            }
        }
    }
}

}
