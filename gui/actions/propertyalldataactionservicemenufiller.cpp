/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "propertyalldataactionservicemenufiller.hpp"

// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/PropertyDataActionService>
// Qt
#include <QMenu>

namespace Kontinga {

PropertyAllDataActionServiceMenuFiller::PropertyAllDataActionServiceMenuFiller() = default;

PropertyAllDataActionServiceMenuFiller::~PropertyAllDataActionServiceMenuFiller() = default;

void PropertyAllDataActionServiceMenuFiller::set(const KContacts::Addressee& person, const QString& propertyId, int itemIndex,
                                                 const QMimeData* data)
{
    m_person = person;
    m_propertyId = propertyId;
    m_itemIndex = itemIndex;
    m_data = data;
}

int PropertyAllDataActionServiceMenuFiller::itemIndex() const
{
    return m_itemIndex;
}

QString PropertyAllDataActionServiceMenuFiller::propertyId() const
{
    return m_propertyId;
}

const KContacts::Addressee& PropertyAllDataActionServiceMenuFiller::person() const
{
    return m_person;
}

const QMimeData* PropertyAllDataActionServiceMenuFiller::data() const
{
    return m_data;
}

void PropertyAllDataActionServiceMenuFiller::onAllDataActionServiceChange()
{ /*TODO: what to do? update menu*/}

void PropertyAllDataActionServiceMenuFiller::onDataActionServiceStateChange(const PropertyDataActionService& service,
                                                                            int change, int itemIndex)
{
    Q_UNUSED(change);
    Q_UNUSED(itemIndex);

    const QString& serviceId = service.id();

    // find menuid of action
    QAction* menuAction = m_actionServiceIdMap.key(serviceId, nullptr);

    if (menuAction) {
        const ServiceAction action = service.action(m_person, m_itemIndex, m_data, 0);

        const QString text = action.data(DisplayTextRole).toString();
        const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
        const bool enabled = action.data(EnabledRole).toBool();

        menuAction->setText(text);
        menuAction->setIcon(icon);
        menuAction->setEnabled(enabled);
    }
}

void PropertyAllDataActionServiceMenuFiller::fillMenu(QMenu* menu)
{
    m_menu = menu;
    m_actionServiceIdMap.clear();

    connect(m_menu, SIGNAL(aboutToHide()), SLOT(onMenuDestruction()));
    connect(m_menu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)));
    Services::self()->registerClient(this);

    const PropertyManager* manager = Services::self()->propertyManagers()[m_propertyId];
    if (!manager) {
        return;
    }

    const PropertyDataActionServiceList& services = manager->dataActionServices();

    for (const auto* service : services) {
        if (!service->isAvailableFor(m_data)) {
            ;// TODO:
        } else if (service->supports(m_data, m_person, m_itemIndex)) {
            const ServiceAction action = service->action(m_person, m_itemIndex, m_data, 0);

            const QString text = action.data(DisplayTextRole).toString();
            const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
            const bool enabled = action.data(EnabledRole).toBool();

            QAction* menuAction = m_menu->addAction(icon, text);
            if (!enabled) {
                menuAction->setEnabled(false);
            }

            m_actionServiceIdMap[menuAction] = service->id();
        }
    }
}

void PropertyAllDataActionServiceMenuFiller::onActionSelection(QAction* action)
{
    Services::self()->execute(m_person, m_propertyId, m_itemIndex, m_actionServiceIdMap[action], m_data);
}

void PropertyAllDataActionServiceMenuFiller::onMenuDestruction()
{
    Services::self()->unregisterClient(this);
}

}
