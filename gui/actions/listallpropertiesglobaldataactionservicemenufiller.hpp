/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_LISTALLPROPERTIESGLOBALDATAACTIONSERVICEMENUFILLER_HPP
#define KONTINGA_LISTALLPROPERTIESGLOBALDATAACTIONSERVICEMENUFILLER_HPP

// lib
#include <kontinga/kontingagui_export.hpp>
#include "idserviceactionmap.hpp"
// Kontinga Core
#include <Kontinga/ListAllPropertiesGlobalDataActionServiceClient>
// KDEPIM
#include <KContacts/AddresseeList>
// Qt
#include <QObject>
#include <QStringList>

class QMenu;

namespace Kontinga {

class KONTINGAGUI_EXPORT ListAllPropertiesGlobalDataActionServiceMenuFiller
    : public QObject
    , public ListAllPropertiesGlobalDataActionServiceClient
{
    Q_OBJECT

public:
    ListAllPropertiesGlobalDataActionServiceMenuFiller();
    ~ListAllPropertiesGlobalDataActionServiceMenuFiller() override;

public:
    void set(const KContacts::AddresseeList& personList, const QMimeData* data);
    void fillMenu(QMenu* menu);

public: // GlobalDropServiceClient API
    void onGlobalDataActionServiceSwitch(const QString& propertyId) override;
    void onPropertyManagerChange() override;

public: // DropServiceClient API
    const KContacts::AddresseeList& personList() const override;
    const QMimeData* data() const override;
//     void onDataActionServiceStateChange(const PropertyDataActionService &Service, int Change,
//                                         int ItemIndex) override;

private Q_SLOTS:
    void onActionSelection(QAction* action);
    void onMenuDestruction();

private:
    KContacts::AddresseeList m_personList;
    const QMimeData* m_data = nullptr;

    QMenu* m_menu = nullptr;
    IdServiceActionMap m_idActionMap;
};

}

#endif
