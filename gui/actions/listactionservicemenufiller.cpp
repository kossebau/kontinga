/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "listactionservicemenufiller.hpp"

// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/ListActionService>
// Qt
#include <QMenu>

namespace Kontinga {

ListActionServiceMenuFiller::ListActionServiceMenuFiller() = default;

ListActionServiceMenuFiller::~ListActionServiceMenuFiller() = default;

void ListActionServiceMenuFiller::setContext(const char* context)
{
    m_context = QString::fromLatin1(context);
}

void ListActionServiceMenuFiller::setPersonList(const KContacts::AddresseeList& list)
{
    m_personList = list;
}

const KContacts::AddresseeList& ListActionServiceMenuFiller::personList() const
{
    return m_personList;
}

void ListActionServiceMenuFiller::onActionServiceStateChange(const ListActionService& service, int change)
{
    Q_UNUSED(change);

    const QString& serviceId = service.id();

    // find menuid of action
    QAction* menuAction = m_actionServiceIdMap.key(serviceId, nullptr);

    if (menuAction) {
        const ServiceAction action = service.action(m_personList, 0);

        const QString text = action.data(DisplayTextRole).toString();
        const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
        const bool enabled = action.data(EnabledRole).toBool();

        menuAction->setText(text);
        menuAction->setIcon(icon);
        menuAction->setEnabled(enabled);
    }
}

void ListActionServiceMenuFiller::fillMenu(QMenu* menu)
{
    m_menu = menu;
    m_actionServiceIdMap.clear();

    connect(m_menu, SIGNAL(destroyed()), SLOT(onMenuDestruction()));
    connect(m_menu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)));
    Services::self()->registerClient(this);

    const ServiceManager* manager = &Services::self()->serviceManager();

    const ActionServiceList& services = manager->actionServices();

    for (const auto* service : services) {
        auto* listService = qobject_cast<const ListActionService*>(service);
        if (!listService) {
            continue;
        }

        if (!listService->isAvailable()) {
            ;// TODO:
        } else if (listService->supports(m_personList) && listService->fitsIn(m_context)) {
            const ServiceAction action = listService->action(m_personList, 0);

            const QString text = action.data(DisplayTextRole).toString();
            const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
            const bool enabled = action.data(EnabledRole).toBool();

            QAction* menuAction = menu->addAction(icon, text);
            if (!enabled) {
                menuAction->setEnabled(false);
            }
            m_actionServiceIdMap[menuAction] = listService->id();
        }
    }
}

void ListActionServiceMenuFiller::onActionSelection(QAction* action)
{
    Services::self()->execute(m_personList, m_actionServiceIdMap[action]);
}

void ListActionServiceMenuFiller::onMenuDestruction()
{
    m_menu = nullptr;
    Services::self()->unregisterClient(this);
}

}
