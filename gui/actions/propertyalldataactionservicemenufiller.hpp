/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_PROPERTYALLDATAACTIONSERVICEMENUFILLER_HPP
#define KONTINGA_PROPERTYALLDATAACTIONSERVICEMENUFILLER_HPP

// lib
#include <kontinga/kontingagui_export.hpp>
// Kontinga core
#include <Kontinga/PropertyAllDataActionServiceClient>
// KDEPIM
#include <KContacts/Addressee>
// Qt
#include <QObject>
#include <QHash>
#include <QStringList>

class QMenu;
class QAction;

namespace Kontinga {

class KONTINGAGUI_EXPORT PropertyAllDataActionServiceMenuFiller : public QObject
    , public PropertyAllDataActionServiceClient
{
    Q_OBJECT

public:
    PropertyAllDataActionServiceMenuFiller();
    ~PropertyAllDataActionServiceMenuFiller() override;

public:
    void set(const KContacts::Addressee& person, const QString& propertyId, int itemIndex,
             const QMimeData* data);
    void fillMenu(QMenu* menu);

public: // AllServiceClient API
    int itemIndex() const override;
    QString propertyId() const override;
    void onAllDataActionServiceChange() override;

public: // DropServiceClient API
    const KContacts::Addressee& person() const override;
    const QMimeData* data() const override;
    void onDataActionServiceStateChange(const PropertyDataActionService& service, int change,
                                        int itemIndex) override;

private Q_SLOTS:
    void onActionSelection(QAction* action);
    void onMenuDestruction();

private:
    KContacts::Addressee m_person;
    QString m_propertyId;
    int m_itemIndex;
    const QMimeData* m_data = nullptr;

    QMenu* m_menu = nullptr;
    QHash<QAction*, QString> m_actionServiceIdMap;
};

}

#endif
