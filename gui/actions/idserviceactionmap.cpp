/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "idserviceactionmap.hpp"

// Qt
#include <QString>

namespace Kontinga {

void IdServiceActionMap::addProperty(const QString& propertyId, int size)
{
    // previous entry used?
    if (m_propertyMap.isEmpty() || !m_propertyMap.last().serviceMap.isEmpty()) {
        // new entry
        m_propertyMap.append(PropertyServiceItemMap(propertyId));
    } else {
        // reuse previous entry
        m_propertyMap.last() = PropertyServiceItemMap(propertyId);
    }
    m_propertyMap.last().serviceMap.reserve(size);
}

void IdServiceActionMap::addService(const QString& serviceId, int size)
{
    // previous entry used?
    if (m_propertyMap.last().serviceMap.isEmpty() || !m_propertyMap.last().serviceMap.last().itemMap.isEmpty()) {
        // new entry
        m_propertyMap.last().serviceMap.append(ServiceItemMap(serviceId));
    } else {
        // reuse previous entry
        m_propertyMap.last().serviceMap.last() = ServiceItemMap(serviceId);
    }
    m_propertyMap.last().serviceMap.last().itemMap.reserve(size);
}

void IdServiceActionMap::addItem(int itemIndex, QAction* menuAction, int flag)
{
    m_propertyMap.last().serviceMap.last().itemMap.append(ItemMenuMap(itemIndex, menuAction, flag));
}

bool IdServiceActionMap::remap(QString* propertyId, int* itemIndex, QString* serviceId, QAction* menuAction) const
{
    bool remapped = false;
    int p = m_propertyMap.size() - 1;
    // last not filled?
    if (m_propertyMap[p].serviceMap.isEmpty()) {
        --p;
    }
    for (; p >= 0; --p) {
        if (m_propertyMap[p].serviceMap.first().itemMap.isEmpty()) {
            continue;
        }
        if (menuAction >= m_propertyMap[p].serviceMap.first().itemMap.first().menuAction) {
            int s = m_propertyMap[p].serviceMap.size() - 1;
            // last not filled?
            if (m_propertyMap[p].serviceMap[s].itemMap.isEmpty()) {
                --s;
            }
            for (; s >= 0; --s) {
                if (menuAction >= m_propertyMap[p].serviceMap[s].itemMap.first().menuAction) {
                    for (int i = m_propertyMap[p].serviceMap[s].itemMap.size() - 1; i >= 0; --i) {
                        if (menuAction == m_propertyMap[p].serviceMap[s].itemMap[i].menuAction) {
                            *propertyId = m_propertyMap[p].id;
                            *itemIndex =  m_propertyMap[p].serviceMap[s].itemMap[i].index;
                            *serviceId =  m_propertyMap[p].serviceMap[s].id;
                            remapped = true;
                            break;
                        }
                    }

                    break;
                }
            }

            break;
        }
    }

    return remapped;
}

QAction* IdServiceActionMap::menuAction(const QString& propertyId, int itemIndex, const QString& serviceId,
                                        int* flag) const
{
    QAction* result = nullptr;
    for (int p = 0; p < m_propertyMap.size(); ++p) {
        if (propertyId == m_propertyMap[p].id) {
            for (int s = 0; s < m_propertyMap[p].serviceMap.size(); ++s) {
                if (serviceId == m_propertyMap[p].serviceMap[s].id) {
                    for (int i = 0; i < m_propertyMap[p].serviceMap[s].itemMap.size(); ++i) {
                        if (itemIndex == m_propertyMap[p].serviceMap[s].itemMap[i].index) {
                            result = m_propertyMap[p].serviceMap[s].itemMap[i].menuAction;
                            if (flag) {
                                *flag = m_propertyMap[p].serviceMap[s].itemMap[i].flag;
                            }
                            break;
                        }
                    }
                }
            }
        }
    }

    return result;
}

}
