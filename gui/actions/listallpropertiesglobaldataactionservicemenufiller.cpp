/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "listallpropertiesglobaldataactionservicemenufiller.hpp"

// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/ListPropertyDataActionService>
// Qt
#include <QMenu>

namespace Kontinga {

ListAllPropertiesGlobalDataActionServiceMenuFiller::ListAllPropertiesGlobalDataActionServiceMenuFiller() = default;

ListAllPropertiesGlobalDataActionServiceMenuFiller::~ListAllPropertiesGlobalDataActionServiceMenuFiller() = default;

void ListAllPropertiesGlobalDataActionServiceMenuFiller::set(const KContacts::AddresseeList& personList,
                                                             const QMimeData* data)
{
    m_personList = personList;
    m_data = data;
}

const KContacts::AddresseeList& ListAllPropertiesGlobalDataActionServiceMenuFiller::personList() const
{
    return m_personList;
}

const QMimeData* ListAllPropertiesGlobalDataActionServiceMenuFiller::data() const
{
    return m_data;
}

void ListAllPropertiesGlobalDataActionServiceMenuFiller::onGlobalDataActionServiceSwitch(const QString& propertyId)
{
    Q_UNUSED(propertyId);
    //TODO: what to do?
}

void ListAllPropertiesGlobalDataActionServiceMenuFiller::onPropertyManagerChange() { /*TODO: what to do?*/}
#if 0
void ListAllPropertiesGlobalDataActionServiceMenuFiller::onDataActionServiceStateChange(
    const PropertyDataActionService& service, int change, int itemIndex)
{
    Q_UNUSED(change);

    const QString& serviceId = ListService->id();

    int maxItemIndex;
    if (ItemIndex == -1) {
        itemIndex = 0;
        maxItemIndex = ListService->adapter()->numberOfItems(person);
    } else {
        maxItemIndex = itemIndex + 1;
    }

    for (; itemIndex < maxItemIndex; ++itemIndex) {
        // find menuid of action
        int flags;
        const int menuId = m_idActionMap.menuId(listService->adapter()->id(), itemIndex, serviceId, &flags);

        if (menuId != -1) {
            const PropertyServiceAction action = listService->action(person, itemIndex, m_data, flags);

            const QString text = action.data(DisplayTextRole).toString();
            const QIcon icon = Action.data(DisplayIconRole).value<QIcon>();
            const bool enabled = Action.data(EnabledRole).toBool();

            menu->changeItem(menuId, icon, text);
            menu->setItemEnabled(menuId, enabled);
        }
    }
}
#endif

void ListAllPropertiesGlobalDataActionServiceMenuFiller::fillMenu(QMenu* menu)
{
    const PropertyManagerList& managers = Services::self()->propertyManagers();

    m_menu = menu;
    connect(m_menu, SIGNAL(aboutToHide()), SLOT(onMenuDestruction()));
    connect(m_menu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)));
    Services::self()->registerClient(this);

    m_idActionMap.prepare(managers.size());
    for (const auto* manager : managers) {
        const PropertyDataActionServiceList& services = manager->mainDataActionServices();
        if (services.isEmpty()) {
            continue;
        }

        const PropertyAdapter* adapter = manager->propertyAdapter();
        if (!adapter->haveProperty(m_personList)) {
            continue;
        }

        m_idActionMap.addProperty(adapter->id(), services.size());

        for (const auto* service : services) {
            auto* listService = qobject_cast<const ListPropertyDataActionService*>(service);
            if (!listService) {
                continue;
            }

            if (!listService->isAvailableFor(m_data)) {
                ;// TODO:
            } else {
                if (!listService->supports(m_data, m_personList)) {
                    continue;
                }

                const int flags = PropertyService::Always;

                m_idActionMap.addService(listService->id(), 1);

                const ServiceAction action = listService->action(m_personList, m_data, flags);

                const QString text = action.data(DisplayTextRole).toString();
                const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
                const bool enabled = action.data(EnabledRole).toBool();

                QAction* menuAction = menu->addAction(icon, text);
                if (!enabled) {
                    menuAction->setEnabled(false);
                }

                m_idActionMap.addItem(0, menuAction, flags);
            }
        }
    }
}

void ListAllPropertiesGlobalDataActionServiceMenuFiller::onActionSelection(QAction* menuAction)
{
    QString propertyId;
    int itemIndex;
    QString serviceId;

    if (m_idActionMap.remap(&propertyId, &itemIndex, &serviceId, menuAction)) {
        Services::self()->execute(m_personList, propertyId, serviceId, m_data);
    }
}

void ListAllPropertiesGlobalDataActionServiceMenuFiller::onMenuDestruction()
{
    Services::self()->unregisterClient(this);
}

}
