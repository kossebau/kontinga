/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "actionservicemenufiller.hpp"

// Kontinga core
#include <Kontinga/Services>
// Qt
#include <QMenu>

namespace Kontinga {

ActionServiceMenuFiller::ActionServiceMenuFiller() = default;

ActionServiceMenuFiller::~ActionServiceMenuFiller() = default;

void ActionServiceMenuFiller::setContext(const char* context)
{
    m_context = QString::fromLatin1(context);
}

void ActionServiceMenuFiller::set(const KContacts::Addressee& person)
{
    m_person = person;
}

const KContacts::Addressee& ActionServiceMenuFiller::person() const
{
    return m_person;
}

void ActionServiceMenuFiller::onActionServiceStateChange(const ActionService& service, int /*change*/)
{
    const QString& serviceId = service.id();

    // find menuid of action
    QAction* menuAction = m_actionServiceIdMap.key(serviceId, nullptr);

    if (menuAction) {
        const ServiceAction action = service.action(m_person, 0);

        const QString text = action.data(DisplayTextRole).toString();
        const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
        const bool enabled = action.data(EnabledRole).toBool();

        menuAction->setText(text);
        menuAction->setIcon(icon);
        menuAction->setEnabled(enabled);
    }
}

void ActionServiceMenuFiller::fillMenu(QMenu* menu)
{
    m_menu = menu;
    m_actionServiceIdMap.clear();

    connect(m_menu, SIGNAL(destroyed()), SLOT(onMenuDestruction()));
    connect(m_menu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)));
    Services::self()->registerClient(this);

    const ServiceManager* manager = &Services::self()->serviceManager();

    const ActionServiceList& services = manager->actionServices();

    for (auto* service : services) {
        if (!service->isAvailable()) {
            ;// TODO:
        } else if (service->supports(m_person) && service->fitsIn(m_context)) {
            const ServiceAction action = service->action(m_person, 0);

            const QString text = action.data(DisplayTextRole).toString();
            const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
            const bool enabled = action.data(EnabledRole).toBool();

            QAction* menuAction = menu->addAction(icon, text);
            if (!enabled) {
                menuAction->setEnabled(false);
            }
            m_actionServiceIdMap[menuAction] = service->id();
        }
    }
}

void ActionServiceMenuFiller::onActionSelection(QAction* action)
{
    Services::self()->execute(m_person, m_actionServiceIdMap[action]);
}

void ActionServiceMenuFiller::onMenuDestruction()
{
    m_menu = nullptr;
    Services::self()->unregisterClient(this);
}

}
