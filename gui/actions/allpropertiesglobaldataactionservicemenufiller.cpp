/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "allpropertiesglobaldataactionservicemenufiller.hpp"

// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/PropertyDataActionService>
// Qt
#include <QMenu>

namespace Kontinga {

AllPropertiesGlobalDataActionServiceMenuFiller::AllPropertiesGlobalDataActionServiceMenuFiller() = default;

AllPropertiesGlobalDataActionServiceMenuFiller::~AllPropertiesGlobalDataActionServiceMenuFiller() = default;

void AllPropertiesGlobalDataActionServiceMenuFiller::set(const KContacts::Addressee& person,
                                                         const QMimeData* data)
{
    m_person = person;
    m_data = data;
}

const KContacts::Addressee& AllPropertiesGlobalDataActionServiceMenuFiller::person() const
{
    return m_person;
}
const QMimeData* AllPropertiesGlobalDataActionServiceMenuFiller::data() const
{
    return m_data;
}

void AllPropertiesGlobalDataActionServiceMenuFiller::onGlobalDataActionServiceSwitch(const QString& propertyId) { 
    Q_UNUSED(propertyId);
    //TODO: what to do?
}

void AllPropertiesGlobalDataActionServiceMenuFiller::onPropertyManagerChange()
{
    //TODO: what to do?
}

void AllPropertiesGlobalDataActionServiceMenuFiller::onDataActionServiceStateChange(
    const PropertyDataActionService& service, int change, int itemIndex)
{
    Q_UNUSED(change);

    const QString& serviceId = service.id();

    int lastItemIndex;
    if (itemIndex == -1) {
        itemIndex = 0;
        lastItemIndex = service.adapter()->numberOfItems(m_person);
    } else {
        lastItemIndex = itemIndex + 1;
    }

    for (; itemIndex < lastItemIndex; ++itemIndex) {
        // find menuid of action
        int flags;
        QAction* menuAction = m_idActionMap.menuAction(service.adapter()->id(), itemIndex, serviceId, &flags);

        if (menuAction) {
            const ServiceAction action = service.action(m_person, itemIndex, m_data, flags);

            const QString text = action.data(DisplayTextRole).toString();
            const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
            const bool enabled = action.data(EnabledRole).toBool();

            menuAction->setText(text);
            menuAction->setIcon(icon);
            menuAction->setEnabled(enabled);
        }
    }
}

void AllPropertiesGlobalDataActionServiceMenuFiller::fillMenu(QMenu* menu)
{
    const PropertyManagerList& managers = Services::self()->propertyManagers();

    m_menu = menu;
    connect(m_menu, SIGNAL(aboutToHide()), SLOT(onMenuDestruction()));
    connect(m_menu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)));
    Services::self()->registerClient(this);

    m_idActionMap.prepare(managers.size());
    for (const auto* manager : managers) {
        const PropertyDataActionServiceList& services = manager->mainDataActionServices();
        if (services.isEmpty()) {
            continue;
        }

        const PropertyAdapter* adapter = manager->propertyAdapter();
        const int itemsSize = adapter->numberOfItems(m_person);
        if (itemsSize == 0) {
            continue;
        }

        m_idActionMap.addProperty(adapter->id(), services.size());

        for (const auto* service : services) {
            if (!service->isAvailableFor(m_data)) {
                ;// TODO:
            } else {
                // find if properties have to be referenced
                int supportedItemsCount = 0;
                for (int itemIndex = 0; itemIndex < itemsSize; ++itemIndex) {
                    if (service->supports(m_data, m_person, itemIndex)) {
                        supportedItemsCount++;
                    }
                }

                if (supportedItemsCount == 0) {
                    continue;
                }

                int flags = PropertyService::Always;
                if (supportedItemsCount > 1) {
                    flags |= PropertyService::ReferItem;
                }

                m_idActionMap.addService(service->id(), itemsSize);

                // insert service action entries
                for (int itemIndex = 0; itemIndex < itemsSize; ++itemIndex) {
                    if (service->supports(m_data, m_person, itemIndex)) {
                        const ServiceAction action = service->action(m_person, itemIndex, m_data, flags);

                        const QString text = action.data(DisplayTextRole).toString();
                        const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
                        const bool enabled = action.data(EnabledRole).toBool();

                        QAction* menuAction = menu->addAction(icon, text);
                        if (!enabled) {
                            menuAction->setEnabled(false);
                        }

                        m_idActionMap.addItem(itemIndex, menuAction, flags);
                    }
                }
            }
        }
    }
}

void AllPropertiesGlobalDataActionServiceMenuFiller::onActionSelection(QAction* action)
{
    QString propertyId;
    int itemIndex;
    QString serviceId;

    if (m_idActionMap.remap(&propertyId, &itemIndex, &serviceId, action)) {
        Services::self()->execute(m_person, propertyId, itemIndex, serviceId, m_data);
    }
}

void AllPropertiesGlobalDataActionServiceMenuFiller::onMenuDestruction()
{
    Services::self()->unregisterClient(this);
}

}
