/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "propertyallactionservicemenufiller.hpp"

// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/PropertyActionService>
// Qt
#include <QMenu>

namespace Kontinga {

PropertyAllActionServiceMenuFiller::PropertyAllActionServiceMenuFiller() = default;

PropertyAllActionServiceMenuFiller::~PropertyAllActionServiceMenuFiller() = default;

void PropertyAllActionServiceMenuFiller::set(const KContacts::Addressee& person, const QString& propertyId, int itemIndex)
{
    m_person = person;
    m_propertyId = propertyId;
    m_itemIndex = itemIndex;
}

int PropertyAllActionServiceMenuFiller::itemIndex() const
{
    return m_itemIndex;
}

QString PropertyAllActionServiceMenuFiller::propertyId() const
{
    return m_propertyId;
}

const KContacts::Addressee& PropertyAllActionServiceMenuFiller::person() const
{
    return m_person;
}

void PropertyAllActionServiceMenuFiller::onAllActionServiceChange()
{ /*TODO: what to do? update menu*/}

void PropertyAllActionServiceMenuFiller::onActionServiceStateChange(const PropertyActionService& service,
                                                                    int change, int itemIndex)
{
    Q_UNUSED(change);
    Q_UNUSED(itemIndex);

    const QString& serviceId = service.id();

    // find menuid of action
    QAction* menuAction = m_actionServiceIdMap.key(serviceId, nullptr);

    if (menuAction) {
        const ServiceAction action = service.action(m_person, m_itemIndex, 0);

        const QString text = action.data(DisplayTextRole).toString();
        const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
        const bool enabled = action.data(EnabledRole).toBool();

        menuAction->setText(text);
        menuAction->setIcon(icon);
        menuAction->setEnabled(enabled);
    }
}

void PropertyAllActionServiceMenuFiller::fillMenu(QMenu* menu)
{
    m_menu = menu;
    m_actionServiceIdMap.clear();

    connect(m_menu, SIGNAL(destroyed()), SLOT(onMenuDestruction()));
    connect(m_menu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)));
    Services::self()->registerClient(this);

    const PropertyManager* manager = Services::self()->propertyManagers()[m_propertyId];
    if (!manager) {
        return;
    }

    const PropertyActionServiceList& services = manager->actionServices();

    for (const auto* service : services) {
        if (!service->isAvailable()) {
            ;// TODO:
        } else if (service->supports(m_person, m_itemIndex)) {
            const ServiceAction action = service->action(m_person, m_itemIndex, 0);

            const QString text = action.data(DisplayTextRole).toString();
            const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
            const bool enabled = action.data(EnabledRole).toBool();

            QAction* menuAction = menu->addAction(icon, text);
            if (!enabled) {
                menuAction->setEnabled(false);
            }
            m_actionServiceIdMap[menuAction] = service->id();
        }
    }
}

void PropertyAllActionServiceMenuFiller::onActionSelection(QAction* action)
{
    Services::self()->execute(m_person, m_propertyId, m_itemIndex, m_actionServiceIdMap[action]);
}

void PropertyAllActionServiceMenuFiller::onMenuDestruction()
{
    m_menu = nullptr;
    Services::self()->unregisterClient(this);
}

}
