/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "listallpropertiesglobalactionservicexmlguifiller.hpp"

// Kontinga Core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/ListPropertyActionService>
// KF
#include <KXMLGUIClient>
// Qt
#include <QSignalMapper>

namespace Kontinga {

ListAllPropertiesGlobalActionServiceXMLGUIFiller::ListAllPropertiesGlobalActionServiceXMLGUIFiller()
    : m_signalMapper(new QSignalMapper(this))
{
    connect(m_signalMapper, SIGNAL(mapped(int)), SLOT(onActionActivation(int)));
}

ListAllPropertiesGlobalActionServiceXMLGUIFiller::~ListAllPropertiesGlobalActionServiceXMLGUIFiller()
{
    qDeleteAll(m_actions);
}

const KContacts::AddresseeList& ListAllPropertiesGlobalActionServiceXMLGUIFiller::personList() const
{
    return m_personList;
}

void ListAllPropertiesGlobalActionServiceXMLGUIFiller::onGlobalActionServiceSwitch(const QString& propertyId)
{
    Q_UNUSED(propertyId):
    // TODO: what to do?
}
void ListAllPropertiesGlobalActionServiceXMLGUIFiller::onPropertyManagerChange() { /*TODO: what to do?*/}
void ListAllPropertiesGlobalActionServiceXMLGUIFiller::onActionServiceStateChange(const PropertyActionService& /*Service*/,
                                                                                  int /*Change*/, int /*ItemIndex*/)
{
#if 0
    const QString& ServiceId = Service.id();

    int MaxItemIndex;
    if (ItemIndex == -1) {
        ItemIndex = 0;
        MaxItemIndex = Service.adapter()->numberOfItems(Person);
    } else {
        MaxItemIndex = ItemIndex + 1;
    }

    for (; ItemIndex < MaxItemIndex; ++ItemIndex) {
        // find menuid of action
        int Flags;
        int ActionId = m_idActionMap.menuId(Service.adapter()->id(), ItemIndex, ServiceId, &Flags);

        if (ActionId != -1) {
            // TODO: store (refer item) flag in m_idActionMap
            ServiceAction Action = Service.action(Person, ItemIndex, Flags);

            QString Entry = Action.data(DisplayTextRole).asString();
            QIconSet IconSet = Action.data(DisplayIconRole).asIconSet();
            bool Enabled = Action.data(EnabledRole).asBool();

            Menu->changeItem(ActionId, IconSet, Entry);
            Menu->setItemEnabled(ActionId, Enabled);
        }
    }

#endif
}

void ListAllPropertiesGlobalActionServiceXMLGUIFiller::setGUIClient(KXMLGUIClient* client, const QString& listId)
{
    m_client = client;
    m_listId = listId;

    if (!m_client) {
        Services::self()->unregisterClient(this);
    }
}

void ListAllPropertiesGlobalActionServiceXMLGUIFiller::setPersonList(const KContacts::AddresseeList& list)
{
    Services::self()->unregisterClient(this);

    m_personList = list;

    Services::self()->registerClient(this);
    update();
}

void ListAllPropertiesGlobalActionServiceXMLGUIFiller::update()
{
    const PropertyManagerList& managers = Services::self()->propertyManagers();

    m_actions.clear();
    int actionId = 0;
    m_idActionMap.prepare(managers.size());
    for (const auto* manager : managers) {
        const PropertyActionServiceList& services = manager->mainActionServices();
        if (services.isEmpty()) {
            continue;
        }

        const PropertyAdapter* adapter = manager->propertyAdapter();
        if (!adapter->haveProperty(m_personList)) {
            continue;
        }

        m_idActionMap.addProperty(adapter->id(), services.size());

        for (const auto* service : services) {
            auto* listService = qobject_cast<const ListPropertyActionService*>(service);
            if (!listService) {
                continue;
            }

            if (!listService->isAvailable()) {
                ;// TODO:
            } else {
                if (!listService->supports(m_personList)) {
                    continue;
                }

                const int flags = PropertyService::Always;

                m_idActionMap.addService(listService->id(), 1);

                // insert service action entries
                m_idActionMap.addItem(0, actionId, flags);

                const ServiceAction action = listService->action(m_personList, flags);

                const QString text = action.data(DisplayTextRole).toString();
                const QIcon icon = Action.data(DisplayIconRole).value<QIcon>();
                const bool enabled = Action.data(EnabledRole).toBool();

                QAction* guiAction =
                    new QAction(text, icon, KShortcut(), m_signalMapper, SLOT(map()),
                                ( KActionCollection* )0, 0);
                guiAction->setEnabled(enabled);
                m_signalMapper->setMapping(GUIAction, ActionId);
                ++actionId;
                m_actions.append(guiAction);
            }
        }
    }

    m_client->unplugActionList(m_listId);
    m_client->plugActionList(m_listId, m_actions);
}

void ListAllPropertiesGlobalActionServiceXMLGUIFiller::onActionActivation(int actionId)
{
    QString propertyId;
    int itemIndex;
    QString serviceId;

    if (m_idActionMap.remap(&propertyId, &itemIndex, &serviceId, actionId)) {
        Services::self()->execute(m_personList, propertyId, serviceId);
    }
}

}
