/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_IDSERVICEACTIONMAP_HPP
#define KONTINGA_IDSERVICEACTIONMAP_HPP

// Qt
#include <QVector>
#include <QString>

class QAction;

namespace Kontinga {

class ItemMenuMap
{
public:
    int index = -1;
    QAction* menuAction = nullptr;
    int flag = 0;

public:
    ItemMenuMap() = default;
    ItemMenuMap(int index, QAction* menuAction, int flag)
        : index(index)
        , menuAction(menuAction)
        , flag(flag)
    {}
};
class ServiceItemMap
{
public:
    QString id;
    QVector<ItemMenuMap> itemMap;

public:
    ServiceItemMap() = default;
    ServiceItemMap(const QString& id) : id(id) {}
};

class PropertyServiceItemMap
{
public:
    QString id;
    QVector<ServiceItemMap> serviceMap;

public:
    PropertyServiceItemMap() = default;
    PropertyServiceItemMap(const QString& id) : id(id) {}
};

class IdServiceActionMap
{
public:
    IdServiceActionMap();
    ~IdServiceActionMap();

public:
    void prepare(int size);
    void addProperty(const QString& propertyId, int size);
    void addService(const QString& serviceId, int size);
    void addItem(int itemIndex, QAction* menuAction, int flag);

    bool remap(QString* propertyId, int* itemIndex, QString* serviceId, QAction* menuAction) const;
    QAction* menuAction(const QString& propertyId, int itemIndex, const QString& serviceId, int* flag = nullptr) const;

private:
    QVector<PropertyServiceItemMap> m_propertyMap;
};

inline IdServiceActionMap::IdServiceActionMap() = default;
inline IdServiceActionMap::~IdServiceActionMap() = default;

inline void IdServiceActionMap::prepare(int size)
{
    m_propertyMap.clear();
    m_propertyMap.reserve(size);
}

}

#endif
