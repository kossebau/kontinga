/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef KONTINGA_LISTALLPROPERTIESDEFAULTACTIONSERVICEXMLGUIFILLER_HPP
#define KONTINGA_LISTALLPROPERTIESDEFAULTACTIONSERVICEXMLGUIFILLER_HPP

// lib
#include <kontinga/kontingagui_export.hpp>
#include "idserviceactionmap.hpp"
// Kontinga core
#include <Kontinga/ListAllPropertiesGlobalActionServiceClient>
// KDEPIM
#include <KContacts/AddresseeList>
// Qt
#include <QAction>
#include <QObject>
#include <QStringList>
#include <QVector>

class QSignalMapper;
class KXMLGUIClient;

namespace Kontinga {

class KONTINGAGUI_EXPORT ListAllPropertiesGlobalActionServiceXMLGUIFiller : public QObject
    , public ListAllPropertiesGlobalActionServiceClient
{
    Q_OBJECT

public:
    ListAllPropertiesGlobalActionServiceXMLGUIFiller();
    ~ListAllPropertiesGlobalActionServiceXMLGUIFiller() override;

public:
    void setPersonList(const KContacts::AddresseeList& list);
    void setGUIClient(KXMLGUIClient* client, const QString& listId);
    void update();

public: // GlobalServiceClient interface
    void onGlobalActionServiceSwitch(const QString& PropertyId) override;
    void onPropertyManagerChange() override;

public: // ServiceClient interface
    const KContacts::AddresseeList& personList() const override;
    void onActionServiceStateChange(const PropertyActionService& service, int change,
                                    int itemIndex) override;

private Q_SLOTS:
    void onActionActivation(int id);

private:
    KContacts::AddresseeList m_personList;

    KXMLGUIClient* m_client = nullptr;
    QString m_listId;

    QVector<QAction*> m_actions;
    QSignalMapper* m_signalMapper;
    IdServiceActionMap m_idActionMap;
};

}

#endif
