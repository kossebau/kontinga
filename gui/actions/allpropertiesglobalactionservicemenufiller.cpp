/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "allpropertiesglobalactionservicemenufiller.hpp"

// Kontinga Core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/PropertyActionService>
// Qt
#include <QMenu>

namespace Kontinga {

AllPropertiesGlobalActionServiceMenuFiller::AllPropertiesGlobalActionServiceMenuFiller() = default;

AllPropertiesGlobalActionServiceMenuFiller::~AllPropertiesGlobalActionServiceMenuFiller() = default;

const KContacts::Addressee& AllPropertiesGlobalActionServiceMenuFiller::person() const
{
    return m_person;
}

void AllPropertiesGlobalActionServiceMenuFiller::set(const KContacts::Addressee& person)
{
    m_person = person;
}

void AllPropertiesGlobalActionServiceMenuFiller::onGlobalActionServiceSwitch(const QString& /*PropertyId*/) { /*TODO: what to do?*/}
void AllPropertiesGlobalActionServiceMenuFiller::onPropertyManagerChange() { /*TODO: what to do?*/}

void AllPropertiesGlobalActionServiceMenuFiller::onActionServiceStateChange(const PropertyActionService& service,
                                                                            int change, int itemIndex)
{
    Q_UNUSED(change);

    const QString& serviceId = service.id();

    int lastItemIndex;
    if (itemIndex == -1) {
        itemIndex = 0;
        lastItemIndex = service.adapter()->numberOfItems(m_person);
    } else {
        lastItemIndex = itemIndex + 1;
    }

    for (; itemIndex < lastItemIndex; ++itemIndex) {
        // find menuid of action
        int flags;
        QAction* menuAction = m_idActionMap.menuAction(service.adapter()->id(), itemIndex, serviceId, &flags);

        if (menuAction) {
            // TODO: store (refer item) flag in IdActionMap
            const ServiceAction action = service.action(m_person, itemIndex, flags);

            const QString text = action.data(DisplayTextRole).toString();
            const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
            const bool enabled = action.data(EnabledRole).toBool();

            menuAction->setText(text);
            menuAction->setIcon(icon);
            menuAction->setEnabled(enabled);
        }
    }
}

void AllPropertiesGlobalActionServiceMenuFiller::fillMenu(QMenu* menu)
{
    const PropertyManagerList& managers = Services::self()->propertyManagers();

    m_menu = menu;
    connect(m_menu, SIGNAL(aboutToHide()), SLOT(onMenuDestruction()));
    connect(m_menu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)));
    Services::self()->registerClient(this);

    m_idActionMap.prepare(managers.size());
    for (const auto* manager : managers) {
        const PropertyActionServiceList& services = manager->mainActionServices();
        if (services.isEmpty()) {
            continue;
        }

        const PropertyAdapter* adapter = manager->propertyAdapter();
        const int itemsSize = adapter->numberOfItems(m_person);
        if (itemsSize == 0) {
            continue;
        }

        m_idActionMap.addProperty(adapter->id(), services.size());

        for (auto* service : services) {
            if (!service->isAvailable()) {
                ;// TODO:
            } else {
                // find if properties have to be referenced
                int supportedItemsCount = 0;
                for (int itemIndex = 0; itemIndex < itemsSize; ++itemIndex) {
                    if (service->supports(m_person, itemIndex)) {
                        supportedItemsCount++;
                    }
                }

                if (supportedItemsCount == 0) {
                    continue;
                }

                int flags = PropertyService::Always;
                if (supportedItemsCount > 1) {
                    flags |= PropertyService::ReferItem;
                }

                m_idActionMap.addService(service->id(), itemsSize);

                // insert service action entries
                for (int itemIndex = 0; itemIndex < itemsSize; ++itemIndex) {
                    if (service->supports(m_person, itemIndex)) {
                        const ServiceAction action = service->action(m_person, itemIndex, flags);

                        const QString text = action.data(DisplayTextRole).toString();
                        const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
                        const bool enabled = action.data(EnabledRole).toBool();

                        QAction* menuAction = menu->addAction(icon, text);
                        if (!enabled) {
                            menuAction->setEnabled(false);
                        }

                        m_idActionMap.addItem(itemIndex, menuAction, flags);
                    }
                }
            }
        }
    }
}

void AllPropertiesGlobalActionServiceMenuFiller::onActionSelection(QAction* action)
{
    QString propertyId;
    int itemIndex;
    QString serviceId;

    if (m_idActionMap.remap(&propertyId, &itemIndex, &serviceId, action)) {
        Services::self()->execute(m_person, propertyId, itemIndex, serviceId);
    }
}

void AllPropertiesGlobalActionServiceMenuFiller::onMenuDestruction()
{
    m_menu = nullptr;
    Services::self()->unregisterClient(this);
}

}
