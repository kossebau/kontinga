/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "listallpropertiesglobalactionservicemenufiller.hpp"

// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/ListPropertyActionService>
// Qt
#include <QMenu>

namespace Kontinga {

ListAllPropertiesGlobalActionServiceMenuFiller::ListAllPropertiesGlobalActionServiceMenuFiller() = default;

ListAllPropertiesGlobalActionServiceMenuFiller::~ListAllPropertiesGlobalActionServiceMenuFiller() = default;

void ListAllPropertiesGlobalActionServiceMenuFiller::setPersonList(const KContacts::AddresseeList& list)
{
    m_personList = list;
}

const KContacts::AddresseeList& ListAllPropertiesGlobalActionServiceMenuFiller::personList() const
{
    return m_personList;
}

void ListAllPropertiesGlobalActionServiceMenuFiller::onGlobalActionServiceSwitch(const QString& propertyId)
{
    Q_UNUSED(propertyId);
    // TODO: what to do?
}
void ListAllPropertiesGlobalActionServiceMenuFiller::onPropertyManagerChange()
{
    // TODO: what to do?
}

#if 0
void ListAllPropertiesGlobalActionServiceMenuFiller::onActionServiceStateChange(const PropertyActionService& /*Service*/,
                                                                                int /*Change*/, int /*ItemIndex*/)
{
    const QString& ServiceId = Service.id();

    int MaxItemIndex;
    if (ItemIndex == -1) {
        ItemIndex = 0;
        MaxItemIndex = Service.adapter()->numberOfItems(Person);
    } else {
        MaxItemIndex = ItemIndex + 1;
    }

    for (; ItemIndex < MaxItemIndex; ++ItemIndex) {
        // find menuid of action
        int Flags;
        int MenuId = IdActionMap.menuId(Service.adapter()->id(), ItemIndex, ServiceId, &Flags);

        if (MenuId != -1) {
            // TODO: store (refer item) flag in IdActionMap
            ServiceAction Action = Service.action(Person, ItemIndex, Flags);

            QString Entry = Action.data(DisplayTextRole).asString();
            QIconSet IconSet = Action.data(DisplayIconRole).asIconSet();
            bool Enabled = Action.data(EnabledRole).asBool();

            Menu->changeItem(MenuId, IconSet, Entry);
            Menu->setItemEnabled(MenuId, Enabled);
        }
    }
}
#endif

void ListAllPropertiesGlobalActionServiceMenuFiller::fillMenu(QMenu* menu)
{
    m_menu = menu;

    connect(m_menu, SIGNAL(aboutToHide()), SLOT(onMenuDestruction()));
    connect(m_menu, SIGNAL(triggered(QAction*)), SLOT(onActionSelection(QAction*)));
    Services::self()->registerClient(this);

    const PropertyManagerList& managers = Services::self()->propertyManagers();
    m_idActionMap.prepare(managers.size());
    for (const auto* manager : managers) {
        const PropertyActionServiceList& services = manager->mainActionServices();
        if (services.isEmpty()) {
            continue;
        }

        const PropertyAdapter* adapter = manager->propertyAdapter();
        if (!adapter->haveProperty(m_personList)) {
            continue;
        }

        m_idActionMap.addProperty(adapter->id(), services.size());

        for (const auto* service : services) {
            auto* listService = qobject_cast<const ListPropertyActionService*>(service);
            if (!listService) {
                continue;
            }

            if (!listService->isAvailable()) {
                ;// TODO:
            } else {
                if (!listService->supports(m_personList)) {
                    continue;
                }

                const int flags = PropertyService::Always;

                m_idActionMap.addService(listService->id(), 1);

                // insert service action entries
                const ServiceAction action = listService->action(m_personList, flags);

                const QString text = action.data(DisplayTextRole).toString();
                const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
                const bool enabled = action.data(EnabledRole).toBool();

                QAction* menuAction = menu->addAction(icon, text);
                if (!enabled) {
                    menuAction->setEnabled(false);
                }

                m_idActionMap.addItem(0, menuAction, flags);
            }
        }
    }
}

void ListAllPropertiesGlobalActionServiceMenuFiller::onActionSelection(QAction* menuAction)
{
    QString propertyId;
    int itemIndex;
    QString serviceId;

    if (m_idActionMap.remap(&propertyId, &itemIndex, &serviceId, menuAction)) {
        Services::self()->execute(m_personList, propertyId, serviceId);
    }
}

void ListAllPropertiesGlobalActionServiceMenuFiller::onMenuDestruction()
{
    m_menu = nullptr;
    Services::self()->unregisterClient(this);
}

}
