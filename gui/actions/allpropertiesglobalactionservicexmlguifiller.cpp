/*
    This file is part of the Kontinga Gui library, part of the KDE project.

    Copyright 2006-2007,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "allpropertiesglobalactionservicexmlguifiller.hpp"

// Kontinga core
#include <Kontinga/Services>
#include <Kontinga/PropertyAdapter>
// KF
#include <KXMLGUIClient>
// Qt
#include <QSignalMapper>

namespace Kontinga {

AllPropertiesGlobalActionServiceXMLGUIFiller::AllPropertiesGlobalActionServiceXMLGUIFiller()
    : m_signalMapper(new QSignalMapper(this))
{
    connect(m_signalMapper, SIGNAL(mapped(int)), SLOT(onActionActivation(int)));
}

AllPropertiesGlobalActionServiceXMLGUIFiller::~AllPropertiesGlobalActionServiceXMLGUIFiller()
{
    qDeleteAll(m_actions);
}

const KContacts::Addressee& AllPropertiesGlobalActionServiceXMLGUIFiller::person() const
{
    return m_person;
}

void AllPropertiesGlobalActionServiceXMLGUIFiller::onGlobalActionServiceSwitch(const QString& /*PropertyId*/) { /*TODO: what to do?*/}
void AllPropertiesGlobalActionServiceXMLGUIFiller::onPropertyManagerChange() { /*TODO: what to do?*/}

void AllPropertiesGlobalActionServiceXMLGUIFiller::onActionServiceStateChange(const PropertyActionService& service,
                                                                              int change, int itemIndex)
{
    Q_UNUSED(change);

    const QString& serviceId = service.id();

    int lastItemIndex;
    if (itemIndex == -1) {
        itemIndex = 0;
        lastItemIndex = service.adapter()->numberOfItems(m_person);
    } else {
        lastItemIndex = itemIndex + 1;
    }

    for (; itemIndex < lastItemIndex; ++itemIndex) {
        // find menuid of action
        int flags;
        QAction* menuAction = m_idActionMap.menuAction(service.adapter()->id(), itemIndex, serviceId, &flags);

        if (menuAction != 0) {
            // TODO: store (refer item) flag in m_idActionMap
            const ServiceAction action = service.action(m_person, itemIndex, flags);

            const QString text = action.data(DisplayTextRole).toString();
            const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
            const bool enabled = action.data(EnabledRole).toBool();

            menuAction->setText(text);
            menuAction->setIcon(icon);
            menuAction->setEnabled(enabled);
            const ServiceAction action = service.action(m_person, itemIndex, flags);

            const QString text = Action.data(DisplayTextRole).toString();
            cons QIcon icon = Action.data(DisplayIconRole).value<QIcon>();
            const bool enabled = Action.data(EnabledRole).toBool();

            QAction* guiAction = m_actions.at(actionId);

            guiAction->setIcon(icon);
            guiAction->setText(text);
            guiAction->setEnabled(enabled);
        }
    }
}

void AllPropertiesGlobalActionServiceXMLGUIFiller::setGUIClient(KXMLGUIClient* client, const QString& listId)
{
    m_client = client;
    m_listId = listId;

    if (!m_client) {
        Services::self()->unregisterClient(this);
    }
}

void AllPropertiesGlobalActionServiceXMLGUIFiller::setPerson(const KContacts::Addressee& person)
{
    Services::self()->unregisterClient(this);

    m_person = person;

    Services::self()->registerClient(this);

    update();
}

void AllPropertiesGlobalActionServiceXMLGUIFiller::update()
{
    const PropertyManagerList& managers = Services::self()->propertyManagers();

    m_actions.clear();
    int actionId = 0;
    m_idActionMap.prepare(managers.size());
    for (const auto* manager : managers) {
        const PropertyActionServiceList& services = manager->mainActionServices();
        if (services.isEmpty()) {
            continue;
        }

        const PropertyAdapter* adapter = Manager.propertyAdapter();
        const int itemsSize = adapter->numberOfItems(m_person);
        if (itemsSize == 0) {
            continue;
        }

        m_idActionMap.addProperty(adapter->id(), services.size());

        for (auto* service : services) {
            if (!service->isAvailable()) {
                ;// TODO:
            } else {
                // find if properties have to be referenced
                int supportedItemsCount = 0;
                for (int itemIndex = 0; itemIndex < itemsSize; ++itemIndex) {
                    if (service->supports(m_person, itemIndex)) {
                        supportedItemsCount++;
                    }
                }

                if (supportedItemsCount == 0) {
                    continue;
                }

                int flags = PropertyService::Always;
                if (supportedItemsCount > 1) {
                    flags |= PropertyService::ReferItem;
                }

                m_idActionMap.addService(service->id(), itemsSize);

                // insert service action entries
                for (int itemIndex = 0; itemIndex < itemsSize; ++itemIndex) {
                    if (service->supports(m_person, itemIndex)) {
                        m_idActionMap.addItem(itemIndex, actionId, flags);

                        const ServiceAction action = service->action(m_person, itemIndex, flags);

                        const QString text = action.data(DisplayTextRole).toString();
                        const QIcon icon = action.data(DisplayIconRole).value<QIcon>();
                        const bool enabled = action.data(EnabledRole).toBool();

                        auto* guiAction =
                            new QAction(text, icon, KShortcut(), m_signalMapper, SLOT(map()),
                                        ( KActionCollection* )nullptr, 0);
                        guiAction->setEnabled(Enabled);
                        m_signalMapper->setMapping(guiAction, actionId);
                        ++actionId;
                        m_actions.append(guiAction);
                    }
                }
            }
        }
    }

    m_client->unplugActionList(m_listId);
    m_client->plugActionList(m_listId, m_actions);
}

void AllPropertiesGlobalActionServiceXMLGUIFiller::onActionActivation(int actionId)
{
    QString propertyId;
    int itemIndex;
    QString serviceId;

    if (m_idActionMap.remap(&propertyId, &itemIndex, &serviceId, actionId)) {
        Services::self()->execute(m_person, propertyId, itemIndex, serviceId);
    }
}

}
