/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "chatservice.hpp"

// service
#include "chatserviceactionadapter.hpp"
// Kontinga Core
#include <Kontinga/PropertyAdapter>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizdString>
#include <kimproxy.h>

ChatService::ChatService(QObject* Parent, const QVariantList& Arguments)
    : PropertyActionService(Parent, Arguments)

{
    IMProxy = KIMProxy::instance(kapp->dcopClient());
    IMProxy->initialize();

    connect(IMProxy, SIGNAL(sigContactPresenceChanged(const QString&)),
            this,    SLOT(onPresenceChanged(const QString&)));
    connect(IMProxy, SIGNAL(sigPresenceInfoExpired()),
            this,    SLOT(onPresenceInfoExpired()));
}

ChatService::~ChatService() = default;

void ChatService::execute(const KContacts::Addressee& Person, int /*ItemIndex*/)
{
    IMProxy->chatWithContact(Person.uid());
}

ServiceAction ChatService::action(const KContacts::Addressee& Person, int /*ItemIndex*/, int /*Flags*/) const
{
    const QString& UID = Person.uid();

    ChatServiceActionAdapter::KState State =
        !IMProxy->imAppsAvailable() ? ChatServiceActionAdapter::IMNotAvailable :
        !IMProxy->isPresent(UID)  ? ChatServiceActionAdapter::Unregistered :
        ChatServiceActionAdapter::Registered;

    return new ChatServiceActionAdapter(IMProxy, State, UID);
}

void ChatService::registerClient(PropertyActionServiceClient* Client, int I)
{
    Clients.append(PropertyActionServiceClientForItem(Client, I));
}

void ChatService::unregisterClient(PropertyActionServiceClient* Client, int ItemIndex)
{
    Clients.remove(PropertyActionServiceClientForItem(Client, ItemIndex));
}

void ChatService::onPresenceChanged(const QString& UID)
{
    for (PropertyActionServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt) {
        if ((*ClientIt).Client->person().uid() == UID) {
            (*ClientIt).Client->onActionServiceStateChange(*this, PropertyActionServiceClient::Unknown);
        }
    }
}

void ChatService::onPresenceInfoExpired()
{
    for (PropertyActionServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt) {
        (*ClientIt).Client->onActionServiceStateChange(*this, PropertyActionServiceClient::Unknown);
    }
}

K_PLUGIN_FACTORY(ChatServiceFactory, registerPlugin<ChatService>(); )

#include "chatservice.moc"
