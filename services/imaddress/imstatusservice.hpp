/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef IMSTATUSSERVICE_HPP
#define IMSTATUSSERVICE_HPP

// Kontinga Core
#include <Kontinga/PropertyStatusService>
#include <Kontinga/PropertyStatusServiceClientForItemList>
// Qt
#include <QVector>

class KIMProxy;

class IMStatusServiceClientForItem : public PropertyStatusServiceClientForItem
{
public:
    int status = 0;

public:
    IMStatusServiceClientForItem() = default;
    IMStatusServiceClientForItem(PropertyStatusServiceClient* client, int itemIndex, int status)
        : PropertyStatusServiceClientForItem(client, itemIndex)
        , status(status)
    {}
    IMStatusServiceClientForItem(PropertyStatusServiceClient* client, int itemIndex)
        : PropertyStatusServiceClientForItem(client, itemIndex)
    {}
    bool operator==(const IMStatusServiceClientForItem& other) const
    { return PropertyStatusServiceClientForItem::operator==(other); }
};

using IMStatusServiceClientForItemList = QVector<IMStatusServiceClientForItem>;

class IMStatusService : public PropertyStatusService
{
    Q_OBJECT

public:
    IMStatusService(QObject* Parent, const QVariantList& Arguments);
    ~IMStatusService() override;

public: // PropertyActionService API
    Status status(const KContacts::Addressee& Person, int ItemIndex, int Flags) const override;

    void registerClient(PropertyStatusServiceClient* Client, int ItemIndex = -1) override;
    void unregisterClient(PropertyStatusServiceClient* Client, int ItemIndex = -1) override;

protected Q_SLOTS:
    void onPresenceChanged(const QString&);
    void onPresenceInfoExpired();

protected:
    KIMProxy* IMProxy;

protected:
    IMStatusServiceClientForItemList Clients;
};

#endif
