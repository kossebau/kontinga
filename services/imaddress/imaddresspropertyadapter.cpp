/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "imaddresspropertyadapter.hpp"

// property
#include "imaddresspropertyitemadapter.hpp"
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KPluginFactory>

IMAddressPropertyAdapter::IMAddressPropertyAdapter(QObject* Parent, const QVariantList& Arguments)
    : PropertyAdapter(Parent, Arguments)
{
}

IMAddressPropertyAdapter::~IMAddressPropertyAdapter() = default;

int IMAddressPropertyAdapter::numberOfItems(const KContacts::Addressee& Person) const
{
    return Person.custom("KADDRESSBOOK", "X-IMAddress").isEmpty() ? 0 : 1;
}

PropertyItem IMAddressPropertyAdapter::propertyItemOf(const KContacts::Addressee& Person, int /*ItemIndex*/) const
{
    return new IMAddressPropertyItemAdapter(Person.custom("KADDRESSBOOK", "X-IMAddress"));
}

K_PLUGIN_FACTORY(IMAddressPropertyAdapterFactory, registerPlugin<IMAddressPropertyAdapter>(); )

#include "imaddresspropertyadapter.moc"
