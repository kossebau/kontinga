/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "imstatusservice.hpp"

// service
#include "imstatusservicestatusadapter.hpp"
#include "imstatusservicechangeadapter.hpp"
// Kontinga Core
#include <Kontinga/PropertyStatusServiceClient>
#include <Kontinga/StatusChange>
#include <Kontinga/PropertyAdapter>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizdString>
#include <kimproxy.h>

IMStatusService::IMStatusService(QObject* Parent, const QVariantList& Arguments)
    : PropertyStatusService(Parent, Arguments)

{
    IMProxy = KIMProxy::instance(kapp->dcopClient());
    IMProxy->initialize();

    connect(IMProxy, SIGNAL(sigContactPresenceChanged(const QString&)),
            this,    SLOT(onPresenceChanged(const QString&)));
    connect(IMProxy, SIGNAL(sigPresenceInfoExpired()),
            this,    SLOT(onPresenceInfoExpired()));
}

IMStatusService::~IMStatusService() = default;

static StatusAdapter* createStatusAdapter(const KContacts::Addressee& Person, KIMProxy* IMProxy)
{
    const QString& UID = Person.uid();

    IMStatusAdapter::KState State =
        !IMProxy->imAppsAvailable() ? IMStatusAdapter::IMNotAvailable :
        !IMProxy->isPresent(UID)  ? IMStatusAdapter::Unregistered :
        IMStatusAdapter::Registered;

    return new IMStatusAdapter(IMProxy, State, UID);
}

enum
{
    KIMProxyUnknown = 0,
    KIMProxyOffline = 1,
    KIMProxyConnecting = 2,
    KIMProxyAway = 3,
    KIMProxyOnline = 4,
};

static IMStatusChangeAdapter* createChangeAdapter(IMStatusServiceClientForItemList::Iterator& it,
                                                  KIMProxy* IMProxy)
{
    const QString& UID = (*it).Client->person().uid();

    const int OldStatus = (*it).status;
    const int NewStatus = IMProxy->presenceNumeric(UID);
    (*it).status = NewStatus;

    IMStatusChangeAdapter::KStatusChange Change;

    // no change or one state unknown, due to connection problems ?
    if (OldStatus == NewStatus || OldStatus == KIMProxyUnknown || NewStatus == KIMProxyUnknown) {
        Change = IMStatusChangeAdapter::Unknown;
    }
    // we're dealing with a genuine contact state change
    else {
        // from offline to something online?
        if (OldStatus == KIMProxyOffline) {
            Change = IMStatusChangeAdapter::SignedIn;
        }
        // from something online to offline?
        else if (NewStatus == KIMProxyOffline) {
            Change = IMStatusChangeAdapter::SignedOut;
        } else {
            Change = IMStatusChangeAdapter::ChangedStatus;
        }
    }

    return new IMStatusChangeAdapter(IMProxy, Change, UID);
}

Status IMStatusService::status(const KContacts::Addressee& Person, int /*ItemIndex*/,
                               int /*Flags*/) const
{
    return createStatusAdapter(Person, IMProxy);
}

void IMStatusService::registerClient(PropertyStatusServiceClient* Client, int ItemIndex)
{
    // no idea how/which clients are supported so register all for now
    Clients.append(IMStatusServiceClientForItem(Client, ItemIndex, IMProxy->presenceNumeric(Client->person().uid())));
}

void IMStatusService::unregisterClient(PropertyStatusServiceClient* Client, int ItemIndex)
{
    Clients.remove(IMStatusServiceClientForItem(Client, ItemIndex));
}

void IMStatusService::onPresenceChanged(const QString& UID)
{
    for (IMStatusServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt) {
        if ((*ClientIt).Client->person().uid() == UID) {
            StatusAdapter* StateAdapter = createStatusAdapter((*ClientIt).Client->person(), IMProxy);
            IMStatusChangeAdapter* ChangeAdapter = createChangeAdapter(ClientIt, IMProxy);
            (*ClientIt).Client->onStateChange(*this, ChangeAdapter, StateAdapter, 0);
        }
    }
}

void IMStatusService::onPresenceInfoExpired()
{
    for (IMStatusServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt) {
        StatusAdapter* StateAdapter = createStatusAdapter((*ClientIt).Client->person(), IMProxy);
        IMStatusChangeAdapter* ChangeAdapter = createChangeAdapter(ClientIt, IMProxy);
        (*ClientIt).Client->onStateChange(*this, ChangeAdapter, StateAdapter, 0);
    }
}

K_PLUGIN_FACTORY(IMStatusServiceFactory, registerPlugin<IMStatusService>(); )

#include "imstatusservice.moc"
