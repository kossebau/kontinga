/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef SENDTODATAACTIONSERVICE_HPP
#define SENDTODATAACTIONSERVICE_HPP

// Kontinga Core
#include <Kontinga/PropertyDataActionService>
#include <Kontinga/PropertyDataActionServiceClientForItemList>

class KIMProxy;

class SendToDataActionService : public PropertyDataActionService
{
    Q_OBJECT

public:
    SendToDataActionService(QObject* Parent, const QVariantList& Arguments);
    ~SendToDataActionService() override;

public: // PropertyDataActionService API
    ServiceAction action(const KContacts::Addressee& Person, int ItemIndex,
                         const QMimeData* data, int Flags) const override;

    void execute(const KContacts::Addressee& Person, int ItemIndex, const QMimeData* data) override;

    bool supports(const QMimeData* data) const override;
    bool isAvailableFor(const QMimeData* data, const KContacts::Addressee& Person, int ItemIndex) const override;

    void registerClient(PropertyDataActionServiceClient* Client, int ItemIndex = -1) override;
    void unregisterClient(PropertyDataActionServiceClient* Client, int ItemIndex = -1) override;

protected Q_SLOTS:
    void onPresenceChanged(const QString&);
    void onPresenceInfoExpired();

protected:
    KIMProxy* IMProxy;

protected:
    PropertyDataActionServiceClientForItemList Clients;
};

#endif
