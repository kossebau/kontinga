/*
    This file is part of the Kontinga services, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "editserviceactionadapter.hpp"

// Kontinga core
#include <Kontinga/Roles>
// KF
#include <KLocalizedString>
// Qt
#include <QIcon>

namespace Kontinga {

QVariant EditServiceActionAdapter::data(int Role) const
{
    QVariant Result;

    switch (Role)
    {
    case DisplayTextRole:
        Result = Name.isNull() ?
                 i18n("Edit Details...") :
                 i18n("Edit Details of %1...", Name);
        break;
    case DisplayIconRole:
        Result = QIcon("document-properties");
        break;
    case EnabledRole:
        Result = true;
        break;
    default:
        ;
    }

    return Result;
}

}
