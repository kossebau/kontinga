/*
    This file is part of the Kontinga services, part of the KDE project.

    Copyright 2006,2010 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "editservice.hpp"

// plugin
#include "editserviceactionadapter.hpp"
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KToolInvocation>
#include <KPluginFactory>
// Qt
#include <QDBusConnection>
#include <QDBusInterface>

namespace Kontinga {

EditService::EditService(QObject* Parent, const QVariantList& Arguments)
    : ActionService(Parent, Arguments)
{}

EditService::~EditService() = default;

ServiceAction EditService::action(const KContacts::Addressee& Person, int Flags) const
{
    return new EditServiceActionAdapter(Flags & ReferItem ? Person.realName() : QString());
}

void EditService::execute(const KContacts::Addressee& person)
{
    QDBusInterface runningAddressbook("org.kde.pim.kaddressbook", "/", "org.kde.pim.Addressbook");
    if (runningAddressbook.isValid()) {
        runningAddressbook.call("newInstance");
    } else {
        KToolInvocation::startServiceByDesktopName("kaddressbook");
    }

    QDBusInterface addressbook("org.kde.pim.kaddressbook", "/", "org.kde.pim.Addressbook");
    addressbook.call("showContactEditor", person.uid());
}

}

K_PLUGIN_FACTORY(EditServiceFactory, registerPlugin<Kontinga::EditService>(); )

#include "editservice.moc"
