/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "copyhomepageservice.hpp"

// service
#include "copyhomepageserviceactionadapter.hpp"
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QClipboard>
#include <QApplication>
#include <QMimeData>
#include <QUrl>

CopyHomepageService::CopyHomepageService(QObject* Parent, const QVariantList& Arguments)
    : PropertyActionService(Parent, Arguments)
{
}

CopyHomepageService::~CopyHomepageService() = default;

void CopyHomepageService::execute(const KContacts::Addressee& Person, int /*ItemIndex*/)
{
    // Copy text into the clipboard
    auto mimeData = new QMimeData;
    mimeData->setUrls(QList<QUrl> {Person.url().url()});
    QApplication::clipboard()->setMimeData(mimeData, QClipboard::Clipboard);
}

ServiceAction CopyHomepageService::action(const KContacts::Addressee& Person, int /*ItemIndex*/, int Flags) const
{
    return new CopyHomepageServiceActionAdapter(Flags & ReferItem ? Person.url().url() : QUrl());
}

K_PLUGIN_FACTORY(CopyHomepageServiceFactory, registerPlugin<CopyHomepageService>(); )

#include "copyhomepageservice.moc"
