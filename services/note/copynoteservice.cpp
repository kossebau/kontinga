/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "copynoteservice.hpp"

// service
#include "copynoteserviceactionadapter.hpp"
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QClipboard>
#include <QApplication>

CopyNoteService::CopyNoteService(QObject* Parent, const QVariantList& Arguments)
    : PropertyActionService(Parent, Arguments)
{
}

CopyNoteService::~CopyNoteService() = default;

void CopyNoteService::execute(const KContacts::Addressee& Person, int /*ItemIndex*/)
{
    // Copy text into the clipboard
    QApplication::clipboard()->setText(Person.note(), QClipboard::Clipboard);
}

ServiceAction CopyNoteService::action(const KContacts::Addressee& Person, int /*ItemIndex*/, int Flags) const
{
    return new CopyNoteServiceActionAdapter(Flags & ReferItem ? Person.note() : QString());
}

K_PLUGIN_FACTORY(CopyNoteServiceFactory, registerPlugin<CopyNoteService>(); )

#include "copynoteservice.moc"
