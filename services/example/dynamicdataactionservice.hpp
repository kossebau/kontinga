/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef DYNAMICDATAACTIONSERVICE_HPP
#define DYNAMICDATAACTIONSERVICE_HPP

// type
#include "typepropertyadapter.hpp"
// Kontinga Core
#include <Kontinga/PropertyDataActionService>
#include <Kontinga/PropertyDataActionServiceClientForItemList>

class DYNAMICDATAACTION_Service : public PropertyDataActionService
{
    Q_OBJECT

public:
    DYNAMICDATAACTION_Service(QObject* Parent, const char* Name, const QStringList& Arguments);
    ~DYNAMICDATAACTION_Service() override;

public: // PropertyDataActionService API
    ServiceAction action(const KContacts::Addressee& Person, int ItemIndex,
                         const QMimeData* data, int Flags) const override;

    void execute(const KContacts::Addressee& Person, int ItemIndex, const QMimeData* data) override;

    bool supports(const QMimeData* data) const override;
    bool isAvailableFor(const QMimeData* data, const KContacts::Addressee& Person, int ItemIndex) const override;

    void setAdapter(PropertyAdapter* Adapter) override;

    void registerClient(PropertyDataActionServiceClient* Client, int ItemIndex = -1) override;
    void unregisterClient(PropertyDataActionServiceClient* Client, int ItemIndex = -1) override;

protected Q_SLOTS:
    void onEvent();

protected:
    PropertyDataActionServiceClientForItemList Clients;
    TYPE_PropertyAdapter* TypeAdapter;

protected: // example data
    /** if 0 for none available, if 1 for uneven, if 2 for even, if 3 for all */
    int AvailableIndexFlag;
};

#endif
