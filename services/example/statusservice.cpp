/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "statusservice.hpp"

// status
#include "statusservicestatusadapter.hpp"
// Kontinga Core
#include <Kontinga/StatusServiceClient>
#include <Kontinga/PropertyAdapter>
#include <Kontinga/StatusChange>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizdString>
#include <KIconLoader>
#include <KPluginFactory>
// Qt
#include <QTimer>

STATUS_Service::STATUS_Service(QObject* Parent, const char* Name, const QStringList& Arguments)
    : PropertyStatusService(Parent, Name, Arguments)
{
    QTimer* Timer = new QTimer(this);
    connect(Timer, SIGNAL(timeout()), SLOT(onEvent()));
    Timer->start(10000);
}

STATUS_Service::~STATUS_Service() = default;

#include <kdebug.h>
Status STATUS_Service::status(const KContacts::Addressee& Person, int ItemIndex, int Flags) const
{
    QString Data;
    if (TypeAdapter) {
        Data = TypeAdapter->property(Person, ItemIndex).data();
    }

    STATUS_ServiceStatusAdapter::KState State =
        Data.isEmpty()  ? STATUS_ServiceStatusAdapter::Unknown :
        STATUS_ServiceStatusAdapter::Some;

    if (!(Flags & PropertyService::ReferItem)) {
        Data = QString();
    }

    return new STATUS_ServiceStatusAdapter(State, Data);
}

void STATUS_Service::registerClient(PropertyStatusServiceClient* Client, int ItemIndex)
{
    if (supports(Client->person(), ItemIndex)) {
        Clients.append(STATUS_ServiceClientForItem(Client, ItemIndex, 0));
    }
}

void STATUS_Service::unregisterClient(PropertyStatusServiceClient* Client, int ItemIndex)
{
    Clients.remove(STATUS_ServiceClientForItem(Client, ItemIndex));
}

void STATUS_Service::onEvent()
{
    for (STATUS_ServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt) {
        const KContacts::Addressee& Person = (*ClientIt).Client->person();
        int Data = (*ClientIt).Data;
        (*ClientIt).Data = (Data != 0) ? 0 : 1;

        int MaxItemIndex;
        int ItemIndex = (*ClientIt).ItemIndex;
        if (ItemIndex == -1) {
            ItemIndex = 0;
            MaxItemIndex = Adapter->numberOfItems(Person);
        } else {
            MaxItemIndex = ItemIndex + 1;
        }

        for (; ItemIndex < MaxItemIndex; ++ItemIndex) {
            StatusAdapter* StatusAdapter = new StatusAdapter();
            StatusAdapter* ChangeAdapter;
            if (Data > 0) {
                const QString Address = Person.emails()[ItemIndex];
                ChangeAdapter = new STATUS_ServiceStatusAdapter(STATUS_ServiceStatusAdapter::Some, QString::number(Data));
            } else {
                ChangeAdapter = new StatusAdapter();
            }
            (*ClientIt).Client->onStateChange(*this, ChangeAdapter, StatusAdapter, ItemIndex);
        }
    }
}

bool STATUS_Service::supports(const KContacts::Addressee& /*Person*/, int /*ItemIndex*/) const
{
    return TypeAdapter != 0;
}

void STATUS_Service::setAdapter(PropertyAdapter* Adapter)
{
    TypeAdapter = static_cast<TYPE_PropertyAdapter*>(Adapter->qt_cast("TYPE_PropertyAdapter"));
    PropertyStatusService::setAdapter(Adapter);
}

K_PLUGIN_FACTORY(STATUS_ServiceFactory, registerPlugin<STATUS_Service>(); )

#include "statusservice.moc"
