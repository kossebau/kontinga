/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef DATAACTIONSERVICE_HPP
#define DATAACTIONSERVICE_HPP

// type
#include "typepropertyadapter.hpp"
// Kontinga Core
#include <Kontinga/PropertyDataActionService>

class DATAACTION_Service : public PropertyDataActionService
{
public:
    DATAACTION_Service(QObject* Parent, const char* Name, const QStringList& Arguments);
    ~DATAACTION_Service() override;

public: // PropertyDataActionService API
    ServiceAction action(const KContacts::Addressee& Person, int ItemIndex,
                         const QMimeData* data, int Flags) const override;

    void execute(const KContacts::Addressee& Person, int ItemIndex, const QMimeData* data) override;

    bool supports(const QMimeData* data) const override;

    void setAdapter(PropertyAdapter* Adapter) override;

protected:
    TYPE_PropertyAdapter* TypeAdapter;
};

#endif
