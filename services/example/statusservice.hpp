/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef STATUSSERVICE_HPP
#define STATUSSERVICE_HPP

// type
#include "typepropertyadapter.hpp"
// Kontinga Core
#include <Kontinga/PropertyStatusService>
#include <Kontinga/PropertyStatusServiceClientForItem>
// Qt
#include <QVector>

class STATUS_ServiceClientForItem : public PropertyStatusServiceClientForItem
{
public:
    int data = 0;

public:
    STATUS_ServiceClientForItem() = default;
    STATUS_ServiceClientForItem(PropertyStatusServiceClient* client, int itemIndex, int data)
        : PropertyStatusServiceClientForItem(client, itemIndex)
        , data(data)
    {}
    STATUS_ServiceClientForItem(PropertyStatusServiceClient* client, int itemIndex)
        : PropertyStatusServiceClientForItem(client, itemIndex)
    {}
    bool operator==(const STATUS_ServiceClientForItem& other) const
    { return PropertyStatusServiceClientForItem::operator==(other); }
};

using STATUS_ServiceClientForItemList = QVector<STATUS_ServiceClientForItem>;

class STATUS_Service : public PropertyStatusService
{
    Q_OBJECT

public:
    STATUS_Service(QObject* Parent, const char* Name, const QStringList& Arguments);
    ~STATUS_Service() override;

public: // PropertyActionService API
    Status status(const KContacts::Addressee& Person, int ItemIndex, int Flags) const override;

    void registerClient(PropertyStatusServiceClient* Client, int ItemIndex = -1) override;
    void unregisterClient(PropertyStatusServiceClient* Client, int ItemIndex = -1) override;

    bool supports(const KContacts::Addressee& Person, int ItemIndex) const override;

    void setAdapter(PropertyAdapter* Adapter) override;

protected Q_SLOTS:
    void onEvent();

protected:
    STATUS_ServiceClientForItemList Clients;

    TYPE_PropertyAdapter* TypeAdapter;
};

#endif
