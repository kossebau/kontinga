/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "addresspropertyadapter.hpp"

// property
#include "addresspropertyitemadapter.hpp"
// KDEPIM
#include <KContacts/Addressee>
#include <KContacts/Address>
// KF
#include <KPluginFactory>

AddressPropertyAdapter::AddressPropertyAdapter(QObject* Parent, const QVariantList& Arguments)
    : PropertyAdapter(Parent, Arguments)
{}

AddressPropertyAdapter::~AddressPropertyAdapter() = default;

int AddressPropertyAdapter::numberOfItems(const KContacts::Addressee& Person) const
{
    return Person.addresses().size();
}

PropertyItem AddressPropertyAdapter::propertyItemOf(const KContacts::Addressee& Person, int ItemIndex) const
{
    return new AddressPropertyItemAdapter(Person.addresses()[ItemIndex]);
}

K_PLUGIN_FACTORY(AddressPropertyAdapterFactory, registerPlugin<AddressPropertyAdapter>(); )

#include "addresspropertyadapter.moc"
