/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "openinwebmapservice.hpp"

// service
#include "openinwebmapserviceactionadapter.hpp"
// Kontinga
#include <Kontinga/PropertyAdapter>
// KDEPIM
#include <KContacts/Addressee>
#include <KContacts/Address>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QDesktopServices>
#include <QUrl>

OpenInWebMapService::OpenInWebMapService(QObject* Parent, const QVariantList& Arguments)
    : PropertyActionService(Parent, Arguments)
{
    OpenInWebMapService::reloadConfig();
}

OpenInWebMapService::~OpenInWebMapService() = default;

void OpenInWebMapService::reloadConfig()
{
//     TODO: update to latest addressbook
//     KConfig Config( "kaddressbookrc" );
//     Config.setGroup( "General" );
//
//     URLTemplate = Config.readEntry( "LocationMapURL" );
}

ServiceAction OpenInWebMapService::action(const KContacts::Addressee& Person, int ItemIndex, int Flags) const
{
    const KContacts::Address A = Person.addresses()[ItemIndex];

    const QString Address = Flags & ReferItem ? A.formattedAddress() : QString();

    return new OpenInWebMapServiceActionAdapter(Address);
}

bool OpenInWebMapService::supports(const KContacts::Addressee& Person, int ItemIndex) const
{
    bool IsSupported = false;

    int MaxItemIndex;
    if (ItemIndex == -1) {
        ItemIndex = 0;
        MaxItemIndex = adapter()->numberOfItems(Person);
    } else {
        MaxItemIndex = ItemIndex + 1;
    }

    for (; ItemIndex < MaxItemIndex; ++ItemIndex) {
//        if( !PropertyParser::folderName(Kontinga,ItemIndex).isEmpty() )
        {
            IsSupported = true;
            break;
        }
    }

    return IsSupported;
}

bool OpenInWebMapService::isAvailable() const
{
    return !URLTemplate.isEmpty();
}

static QUrl createUrl(const QString& URLTemplate, const KContacts::Address& Address)
{
    return QUrl(QString(URLTemplate).// arg( KGlobal::locale()->country() ).
           replace("%s", Address.street()).
           replace("%r", Address.region()).
           replace("%l", Address.locality()).
           replace("%z", Address.postalCode()).
           replace("%c", Address.countryToISO(Address.country())));
}

void OpenInWebMapService::execute(const KContacts::Addressee& Person, int ItemIndex)
{
    const KContacts::Address Address = Person.addresses()[ItemIndex];

    QDesktopServices::openUrl(createUrl(URLTemplate, Address));
}

K_PLUGIN_FACTORY(OpenInWebMapServiceFactory, registerPlugin<OpenInWebMapService>(); )

#include "openinwebmapservice.moc"
