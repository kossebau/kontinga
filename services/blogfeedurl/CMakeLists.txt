add_definitions(-DTRANSLATION_DOMAIN=\"kontinga_blogfeedurl\")

kontinga_add_property(blogfeedurl
    SOURCES
        blogfeedpropertyadapter.cpp
        blogfeedpropertyitemadapter.cpp
    METADATA
        kontingaproperty_blogfeedurl.desktop
)

kontinga_add_propertyactionservice(copyblogfeedurl
    SOURCES
        copyurlservice.cpp
        copyurlserviceactionadapter.cpp
    METADATA
        kontingapropertyactionservice_copyblogfeedurl.desktop
)

