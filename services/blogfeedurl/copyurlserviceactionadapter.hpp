/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef COPYURLSERVICEACTIONADAPTER_HPP
#define COPYURLSERVICEACTIONADAPTER_HPP

// Kontinga Core
#include <Kontinga/ServiceActionAdapter>

using namespace Kontinga;

class CopyBlogFeedUrlServiceActionAdapter : public ServiceActionAdapter
{
public:
    CopyBlogFeedUrlServiceActionAdapter(const QString& Url);
    ~CopyBlogFeedUrlServiceActionAdapter() override;

public: // Property API
    QVariant data(int Role) const override;

protected:
    const QString Url;
};

inline CopyBlogFeedUrlServiceActionAdapter::CopyBlogFeedUrlServiceActionAdapter(const QString& U)
    : Url(U)
{}

inline CopyBlogFeedUrlServiceActionAdapter::~CopyBlogFeedUrlServiceActionAdapter() = default;

#endif
