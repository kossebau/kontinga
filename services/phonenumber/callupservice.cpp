/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "callupservice.hpp"

// service
#include "callupserviceactionadapter.hpp"
// Kontinga
#include <Kontinga/PropertyAdapter>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KRun>
#include <KLocalizedString>
#include <KPluginFactory>

CallUpService::CallUpService(QObject* Parent, const QVariantList& Arguments)
    : PropertyActionService(Parent, Arguments)
{
    CallUpService::reloadConfig();
}

CallUpService::~CallUpService() = default;

void CallUpService::reloadConfig()
{
//     TODO: update to latest addressbook
//     auto Config = KSharedConfig::open("kaddressbookrc");
//     Config.setGroup( "General" );

//     CommandLine = Config.readEntry( "PhoneHookApplication" );//kdialog --msgbox "%N"
}

ServiceAction CallUpService::action(const KContacts::Addressee& Person, int ItemIndex, int Flags) const
{
    KContacts::PhoneNumber Number;
    if (Flags & ReferItem) {
        Number = Person.phoneNumbers()[ItemIndex];
    }

    return new CallUpServiceActionAdapter(Number, CommandLine.isEmpty());
}

bool CallUpService::supports(const KContacts::Addressee& Person, int ItemIndex) const
{
    bool IsSupported = false;

    int MaxItemIndex;
    if (ItemIndex == -1) {
        ItemIndex = 0;
        MaxItemIndex = adapter()->numberOfItems(Person);
    } else {
        MaxItemIndex = ItemIndex + 1;
    }

    for (; ItemIndex < MaxItemIndex; ++ItemIndex) {
        const KContacts::PhoneNumber Number = Person.phoneNumbers()[ItemIndex];

        // Voice is not set by default home/office, so check for default by nothing else set
        static const int OtherFunctions = KContacts::PhoneNumber::Msg | KContacts::PhoneNumber::Fax | KContacts::PhoneNumber::Bbs
                                          | KContacts::PhoneNumber::Modem | KContacts::PhoneNumber::Pager;
        if (Number.type() & KContacts::PhoneNumber::Voice
            || !(Number.type() & OtherFunctions)) {
            IsSupported = true;
            break;
        }
    }

    return IsSupported;
}

bool CallUpService::isAvailable() const
{
    return !CommandLine.isEmpty();
}

void CallUpService::execute(const KContacts::Addressee& Person, int ItemIndex)
{
    const KContacts::PhoneNumber Number = Person.phoneNumbers()[ItemIndex];

    QString Command = CommandLine;
    Command.replace("%N", Number.number());
    KRun::runCommand(Command, nullptr);
}

K_PLUGIN_FACTORY(CallUpServiceFactory, registerPlugin<CallUpService>(); )

#include "callupservice.moc"
