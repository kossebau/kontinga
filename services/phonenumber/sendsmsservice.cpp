/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "sendsmsservice.hpp"

// service
#include "sendsmsserviceactionadapter.hpp"
// Kontinga
#include <Kontinga/PropertyAdapter>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KRun>
#include <KLocalizedString>
#include <KPluginFactory>

SendSMSService::SendSMSService(QObject* Parent, const QVariantList& Arguments)
    : PropertyActionService(Parent, Arguments)
{
    SendSMSService::reloadConfig();
}

SendSMSService::~SendSMSService() = default;

void SendSMSService::reloadConfig()
{
//     TODO: update to latest addressbook
//     KConfig Config( "kaddressbookrc" );
//     Config.setGroup( "General" );
//
//     CommandLine = Config.readEntry( "SMSHookApplication" );
}

ServiceAction SendSMSService::action(const KContacts::Addressee& Person, int ItemIndex, int Flags) const
{
    KContacts::PhoneNumber Number;
    if (Flags & ReferItem) {
        Number = Person.phoneNumbers()[ItemIndex];
    }

    return new SendSMSServiceActionAdapter(Number, CommandLine.isEmpty());
}

bool SendSMSService::supports(const KContacts::Addressee& Person, int ItemIndex) const
{
    bool IsSupported = false;

    int MaxItemIndex;
    if (ItemIndex == -1) {
        ItemIndex = 0;
        MaxItemIndex = adapter()->numberOfItems(Person);
    } else {
        MaxItemIndex = ItemIndex + 1;
    }

    for (; ItemIndex < MaxItemIndex; ++ItemIndex) {
        const KContacts::PhoneNumber Number = Person.phoneNumbers()[ItemIndex];
        if (Number.type() & KContacts::PhoneNumber::Msg) {
            IsSupported = true;
            break;
        }
    }

    return IsSupported;
}

bool SendSMSService::isAvailableFor(const KContacts::Addressee& Person, int ItemIndex) const
{
    return supports(Person, ItemIndex) && isAvailable();
}

bool SendSMSService::isAvailable() const
{
    return !CommandLine.isEmpty();
}

void SendSMSService::execute(const KContacts::Addressee& Person, int ItemIndex)
{
    const KContacts::PhoneNumber Number = Person.phoneNumbers()[ItemIndex];

    QString Command = CommandLine;
    Command.replace("%N", Number.number());
    KRun::runCommand(Command, nullptr);
}

K_PLUGIN_FACTORY(SendSMSServiceFactory, registerPlugin<SendSMSService>(); )

#include "sendsmsservice.moc"
