/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef CALLUPSERVICEACTIONADAPTER_HPP
#define CALLUPSERVICEACTIONADAPTER_HPP

// Kontinga Core
#include <Kontinga/ServiceActionAdapter>
// KDEPIM
#include <KContacts/PhoneNumber>

using namespace Kontinga;

class CallUpServiceActionAdapter : public ServiceActionAdapter
{
public:
    CallUpServiceActionAdapter(const KContacts::PhoneNumber& Number, bool Unconfigured);
    ~CallUpServiceActionAdapter() override;

public: // ServiceActionAdapter API
    QVariant data(int Role) const override;

protected:
    KContacts::PhoneNumber Number;
    bool Unconfigured;
};

inline CallUpServiceActionAdapter::CallUpServiceActionAdapter(const KContacts::PhoneNumber& N, bool U)
    : Number(N)
    , Unconfigured(U)
{}

inline CallUpServiceActionAdapter::~CallUpServiceActionAdapter() = default;

#endif
