/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "phonenumberpropertyadapter.hpp"

// adapter
#include "phonenumberpropertyitemadapter.hpp"
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KPluginFactory>

PhoneNumberPropertyAdapter::PhoneNumberPropertyAdapter(QObject* Parent, const QVariantList& Arguments)
    : PropertyAdapter(Parent, Arguments)
{}

PhoneNumberPropertyAdapter::~PhoneNumberPropertyAdapter() = default;

int PhoneNumberPropertyAdapter::numberOfItems(const KContacts::Addressee& Person) const
{
    return Person.phoneNumbers().size();
}

PropertyItem PhoneNumberPropertyAdapter::propertyItemOf(const KContacts::Addressee& Person, int ItemIndex) const
{
    return new PhoneNumberPropertyItemAdapter(Person.phoneNumbers()[ItemIndex]);
}

K_PLUGIN_FACTORY(PhoneNumberPropertyAdapterFactory, registerPlugin<PhoneNumberPropertyAdapter>(); )

#include "phonenumberpropertyadapter.moc"
