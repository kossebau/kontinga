/*
    This file is part of the KDE project.
    Copyright (c) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "sendfaxconfig.hpp"

// KF
#include <KConfigGroup>
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QLabel>
#include <QLineEdit>
#include <QToolTip>
#include <QHBoxLayout>

static const char KAddressbookRCFileName[] = "kaddressbookrc";
static const char GeneralGroupId[] = "General";
static const char HookId[] = "FaxHookApplication";
static const char Default[] = "kdeprintfax --phone %N";

SendFaxServiceConfigWidget::SendFaxServiceConfigWidget(QWidget* Parent, const char* Name, const QStringList& Arguments)
    : QWidget(Parent, Name)
{
    Q_UNUSED(Arguments)

    QHBoxLayout* TopLayout = new QHBoxLayout(this, 0, KDialog::spacingHint());

    QLabel* Label = new QLabel(i18n("Execute Command:"), this);

    HookEdit = new QLineEdit(this);
    const QString EditToolTip = i18n(""
                                     "You can use the following macros in the command line:"
                                     "<ul>"
                                     "<li><b>%N</b>: the fax number</li>"
                                     "</ul>");
    QToolTip::add(Label, EditToolTip);
    QToolTip::add(HookEdit, EditToolTip);

    TopLayout->addWidget(Label);
    TopLayout->addWidget(HookEdit);

    KConfig Config(KAddressbookRCFileName);
    Config.setGroup(GeneralGroupId);

    OriginalScript = Config.readEntry(HookId, Default);
    HookEdit->setText(OriginalScript);

    connect(HookEdit, SIGNAL(textChanged(const QString&)), SLOT(updateChanged()));
}

SendFaxServiceConfigWidget::~SendFaxServiceConfigWidget() = default;

void SendFaxServiceConfigWidget::save()
{
    KConfig Config(KAddressbookRCFileName);
    Config.setGroup(GeneralGroupId);
    Config.writeEntry(HookId, HookEdit->text());
}

void SendFaxServiceConfigWidget::defaults()
{
    HookEdit->setText(Default);
}

void SendFaxServiceConfigWidget::updateChanged()
{
    emit changed(HookEdit->text() != OriginalScript);
}

K_PLUGIN_FACTORY(SendFaxServiceConfigWidgetFactory, registerPlugin<SendFaxServiceConfigWidget>(); )

#include "sendfaxconfig.moc"
