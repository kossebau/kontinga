/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef BIRTHDAYSTATUSSERVICE_HPP
#define BIRTHDAYSTATUSSERVICE_HPP

// Kontinga Core
#include <Kontinga/PropertyStatusService>
#include <Kontinga/PropertyStatusServiceClientForItemList>

using namespace Kontinga;

class BirthdayStatusServiceClientForItem : public PropertyStatusServiceClientForItem
{
public:
    int notificationState = -1;

public:
    BirthdayStatusServiceClientForItem() = default;
    BirthdayStatusServiceClientForItem(PropertyStatusServiceClient* client, int itemIndex, int state)
        : PropertyStatusServiceClientForItem(client, itemIndex)
        , notificationState(state)
    {}
    BirthdayStatusServiceClientForItem(PropertyStatusServiceClient* client, int itemIndex)
        : PropertyStatusServiceClientForItem(client, itemIndex)
    {}
    bool operator==(const BirthdayStatusServiceClientForItem& other) const
    { return PropertyStatusServiceClientForItem::operator==(other); }
};

using BirthdayStatusServiceClientForItemList = QVector<BirthdayStatusServiceClientForItem>;

class BirthdayStatusService : public PropertyStatusService
{
    Q_OBJECT

public:
    enum NotificationState
    {
        BirthdayPassive,
        BirthdayActive,
    };

public:
    BirthdayStatusService(QObject* Parent, const QVariantList& Arguments);
    ~BirthdayStatusService() override;

public: // PropertyActionService API
    Status status(const KContacts::Addressee& Person, int ItemIndex, int Flags) const override;

    bool supports(const KContacts::Addressee& Person, int ItemIndex) const override;

    void registerClient(PropertyStatusServiceClient* Client, int ItemIndex = -1) override;
    void unregisterClient(PropertyStatusServiceClient* Client, int ItemIndex = -1) override;

protected:
    void startTimer();

protected Q_SLOTS:
    void onNewDay();

protected:
    BirthdayStatusServiceClientForItemList Clients;
};

#endif
