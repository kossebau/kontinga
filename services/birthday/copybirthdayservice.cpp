/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "copybirthdayservice.hpp"

// service
#include "copybirthdayserviceactionadapter.hpp"
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QClipboard>
#include <QApplication>
#include <QLocale>

CopyBirthdayService::CopyBirthdayService(QObject* Parent, const QVariantList& Arguments)
    : PropertyActionService(Parent, Arguments)
{
}

CopyBirthdayService::~CopyBirthdayService() = default;

void CopyBirthdayService::execute(const KContacts::Addressee& Person, int /*ItemIndex*/)
{
    // Copy text into the clipboard
    const QString DateString = QLocale().toString(Person.birthday().date(), QLocale::LongFormat);
    QApplication::clipboard()->setText(DateString, QClipboard::Clipboard);
}

ServiceAction CopyBirthdayService::action(const KContacts::Addressee& /*Person*/, int /*ItemIndex*/, int /*Flags*/) const
{
    return new CopyBirthdayServiceActionAdapter(/*Flags&ReferItem ? Person.note() : */ QDate());
}

K_PLUGIN_FACTORY(CopyBirthdayServiceFactory, registerPlugin<CopyBirthdayService>(); )

#include "copybirthdayservice.moc"
