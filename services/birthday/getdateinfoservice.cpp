/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "getdateinfoservice.hpp"

// service
#include "getdateinfoserviceactionadapter.hpp"
// Kontinga Core
#include <Kontinga/PropertyAdapter>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KSharedConfig>
#include <KConfigGroup>
#include <KPluginFactory>
#include <KLocalizedString>
// Qt
#include <QLocale>
#include <QDesktopServices>
#include <QUrl>

GetDateInfoService::GetDateInfoService(QObject* Parent, const QVariantList& Arguments)
    : PropertyActionService(Parent, Arguments)
{
}

GetDateInfoService::~GetDateInfoService() = default;

void GetDateInfoService::execute(const KContacts::Addressee& Person, int /*ItemIndex*/)
{
    auto Config = KSharedConfig::openConfig("kontingapropertyactionservice_getdateinforc");
    KConfigGroup generalGroup(Config, "General");

    const QString DateFormat = generalGroup.readEntry("DateFormat");
    const QString URLFormat = generalGroup.readEntry("URLFormat");

    const QDate Date = Person.birthday().date();

    const QString URL = URLFormat.arg(QLocale().toString(Date, DateFormat));

    QDesktopServices::openUrl(QUrl(URL));
}

ServiceAction GetDateInfoService::action(const KContacts::Addressee& /*Person*/, int /*ItemIndex*/,
                                         int /*Flags*/) const
{
    return new GetDateInfoServiceActionAdapter();
}

K_PLUGIN_FACTORY(GetDateInfoServiceFactory, registerPlugin<GetDateInfoService>(); )

#include "getdateinfoservice.moc"
