/*
    This file is part of the KDE project.
    Copyright (c) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "birthdaystatusservice.hpp"

// Kontinga Core
#include <Kontinga/PropertyStatusServiceClient>
#include <Kontinga/StatusChange>
#include <Kontinga/PropertyAdapter>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QIcon>
#include <QDate>
#include <QTimer>

static int daysToMonthDay(const QDate& AncientDate, int MaxDiff = 10)
{
    const QDate CurrentDate = QDate::currentDate();

    QDate RecurrenceDate(CurrentDate.year(), AncientDate.month(), AncientDate.day());
    int Diff = CurrentDate.daysTo(RecurrenceDate);

    return 0 > Diff || Diff > MaxDiff ? -1 : Diff;
}

class BirthdayStatusAdapter : public StatusAdapter
{
public:
    BirthdayStatusAdapter(int Days);

public:
    QVariant data(int Role) const override;

protected:
    int DaysToBirthday;
};

BirthdayStatusAdapter::BirthdayStatusAdapter(int Days) : DaysToBirthday(Days) {}

QVariant BirthdayStatusAdapter::data(int Role) const
{
    QVariant Result;

    switch (Role)
    {
    case DisplayTextRole:
        Result = (DaysToBirthday == 0) ? i18n("Has birthday today.") :
                 i18np("Has birthday in one day.", "Has birthday in %n days.", DaysToBirthday);
        break;
    case DisplayIconRole:  // TODO: how control imagesize?
        if (DaysToBirthday == 0) {
            Result = QIcon("cookie");
        }
        break;
    default:
        ;
    }
    return Result;
}

BirthdayStatusService::BirthdayStatusService(QObject* Parent, const QVariantList& Arguments)
    : PropertyStatusService(Parent, Arguments)
{
    startTimer();
}

BirthdayStatusService::~BirthdayStatusService() = default;

Status BirthdayStatusService::status(const KContacts::Addressee& Person, int /*ItemIndex*/,
                                     int /*Flags*/) const
{
    StatusAdapter* Result;
    const QDate& Birthdate = Person.birthday().date();
    const int DaysToBirthday = daysToMonthDay(Birthdate);

    Result = (Birthdate.isValid() && DaysToBirthday >= 0) ? new BirthdayStatusAdapter(DaysToBirthday) :
             new StatusAdapter();

    return Result;
}

bool BirthdayStatusService::supports(const KContacts::Addressee& Person, int /*ItemIndex*/) const
{
    return Person.birthday().date().isValid();
}

void BirthdayStatusService::registerClient(PropertyStatusServiceClient* Client, int ItemIndex)
{
    const QDate& Birthdate = Client->person().birthday().date();

    // register only valid birthdates
    if (Birthdate.isValid()) {
        int State = daysToMonthDay(Birthdate) >= 0 ? BirthdayActive : BirthdayPassive;

        Clients.append(BirthdayStatusServiceClientForItem(Client, ItemIndex, State));
    }
}

void BirthdayStatusService::unregisterClient(PropertyStatusServiceClient* Client, int ItemIndex)
{
    Clients.removeOne(BirthdayStatusServiceClientForItem(Client, ItemIndex));
}

void BirthdayStatusService::startTimer()
{
    // msecsTo only works between 0 and 24 hours, so add some more to get to next day
    int MilliSecondsToAlmostMidnight = QTime::currentTime().msecsTo(QTime(23, 59, 59, 999));
    const int SomeMilliSecondsIntoNextDay = 1000;
    QTimer::singleShot(MilliSecondsToAlmostMidnight + SomeMilliSecondsIntoNextDay, this, SLOT(onNewDay()));
}

void BirthdayStatusService::onNewDay()
{
    BirthdayStatusServiceClientForItemList::Iterator ClientIt = Clients.begin();
    while (ClientIt != Clients.end()) {
        const QDate& Birthdate = (*ClientIt).client->person().birthday().date();
        const int DaysToBirthday = daysToMonthDay(Birthdate);

        bool GetsDateSignal = DaysToBirthday >= 0;
        if (GetsDateSignal || (*ClientIt).notificationState == BirthdayActive) {
            const bool BirthdayNotPassed = (DaysToBirthday >= 0);
            StatusAdapter* StateAdapter =  BirthdayNotPassed ?
                                          new BirthdayStatusAdapter(DaysToBirthday) :
                                          new StatusAdapter();
            StatusAdapter* ChangeAdapter = BirthdayNotPassed ?
                                           new BirthdayStatusAdapter(DaysToBirthday) :
                                           new StatusAdapter();

            (*ClientIt).client->onStateChange(*this, StateAdapter, ChangeAdapter, 0);
        }
        (*ClientIt).notificationState = GetsDateSignal ? BirthdayActive : BirthdayPassive;

        ++ClientIt;
    }

    startTimer();
}

K_PLUGIN_FACTORY(CopyBirthdayServiceFactory, registerPlugin<BirthdayStatusService>(); )

#include "birthdaystatusservice.moc"
