/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "emailaddresspropertyadapter.hpp"

// property
#include "emailaddresspropertyitemadapter.hpp"
// KDEPIM
#include <KContacts/Addressee>
#include <KContacts/AddresseeList>
// KF
#include <KPluginFactory>

EmailAddressPropertyAdapter::EmailAddressPropertyAdapter(QObject* Parent, const QVariantList& Arguments)
    : PropertyAdapter(Parent, Arguments)
{
}

EmailAddressPropertyAdapter::~EmailAddressPropertyAdapter() = default;

bool EmailAddressPropertyAdapter::haveProperty(const KContacts::AddresseeList& PersonList) const
{
    bool Result = false;
    for (KContacts::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it) {
        if ((*it).emails().size() > 0) {
            Result = true;
            break;
        }
    }

    return Result;
}

int EmailAddressPropertyAdapter::numberOfItems(const KContacts::Addressee& Person) const
{
    return Person.emails().size();
}

PropertyItem EmailAddressPropertyAdapter::propertyItemOf(const KContacts::Addressee& Person, int ItemIndex) const
{
    return new EmailAddressPropertyItemAdapter(Person.emails()[ItemIndex]);
}

K_PLUGIN_FACTORY(EmailAddressPropertyAdapterFactory, registerPlugin<EmailAddressPropertyAdapter>(); )

#include "emailaddresspropertyadapter.moc"
