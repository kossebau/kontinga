/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "mailurlstodropservice.hpp"

// plugin
#include "mailurlstodropserviceactionadapter.hpp"
#include "mailurlstolistdropserviceactionadapter.hpp"
// KDEPIM
#include <KContacts/AddresseeList>
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QDesktopServices>
#include <QUrlQuery>
#include <QUrl>
#include <QMimeData>

static void invokeMailer(const QStringList& emailAddresses, const QString& subject, const QString& body)
{
    QUrl url;
    url.setScheme(QStringLiteral("mailto"));

    QUrlQuery query;
    query.addQueryItem(QStringLiteral("to"), emailAddresses.join(','));
    query.addQueryItem(QStringLiteral("subject"), subject);
    query.addQueryItem(QStringLiteral("body"), body);
    url.setQuery(query);

    QDesktopServices::openUrl(url);
}

MailURLsToDataActionService::MailURLsToDataActionService(QObject* Parent, const QVariantList& Arguments)
    : ListPropertyDataActionService(Parent, Arguments)
{
}

MailURLsToDataActionService::~MailURLsToDataActionService() = default;

ServiceAction MailURLsToDataActionService::action(const KContacts::Addressee& Person, int ItemIndex,
                                                  const QMimeData* data, int Flags) const
{
    const int urlCount = data->urls().size();
    const QString Address = Flags & ReferItem ? Person.emails()[ItemIndex] : QString();

    return new MailURLsToDataActionServiceActionAdapter(Address, urlCount);
}
ServiceAction MailURLsToDataActionService::action(const KContacts::AddresseeList& PersonList,
                                                  const QMimeData* data, int /*Flags*/) const
{
    const int urlCount = data->urls().size();

    int NoOfEmails = 0;
    for (KContacts::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it) {
        if ((*it).emails().size() > 0) {
            ++NoOfEmails;
        }
    }

    return new MailURLsToListDataActionServiceActionAdapter(NoOfEmails, PersonList.size(), urlCount);
}

static void composeEmail(QString* Subject, QString* Body, const QList<QUrl>& urls)
{
    for (const auto& url : urls) {
        if (!Subject->isEmpty()) {
            *Subject += ", ";
        }
        *Subject += url.fileName();
        if (!Body->isEmpty()) {
            *Body += '\n';
        }
        *Body += url.toDisplayString();
    }
}

void MailURLsToDataActionService::execute(const KContacts::Addressee& Person, int ItemIndex, const QMimeData* data)
{
    const QList<QUrl> urls = data->urls();

    if (!urls.isEmpty()) {
        QString Subject;
        QString Body;
        composeEmail(&Subject, &Body, urls);
        invokeMailer(QStringList(Person.fullEmail(Person.emails()[ItemIndex])), Subject, Body);
    }
}

void MailURLsToDataActionService::execute(const KContacts::AddresseeList& PersonList, const QMimeData* data)
{
    const QList<QUrl> urls = data->urls();

    if (!urls.isEmpty()) {
        QString Subject;
        QString Body;
        composeEmail(&Subject, &Body, urls);

        QStringList emailAddresses;
        for (const auto& person : PersonList) {
            const QString preferredEmail = person.preferredEmail();
            if (!preferredEmail.isEmpty()) {
                emailAddresses.append(person.fullEmail(preferredEmail));
            }
        }

        invokeMailer(emailAddresses, Subject, Body);
    }
}

bool MailURLsToDataActionService::supports(const QMimeData* data) const
{
    return data->hasUrls();
}

K_PLUGIN_FACTORY(MailURLsToDataActionServiceeFactory, registerPlugin<MailURLsToDataActionService>(); )

#include "mailurlstodropservice.moc"
