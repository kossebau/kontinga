/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "openemailfolderservice.hpp"

// service
#include "addresstokmailfoldermapparser.hpp"
#include "openemailfolderserviceactionadapter.hpp"
// Kontinga Core
#include <Kontinga/PropertyAdapter>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
#include <KWindowSystem>
#include <KWindowInfo>
#include <KToolInvocation>
// Qt
#include <QDBusInterface>
#include <QDBusReply>

OpenEmailFolderService::OpenEmailFolderService(QObject* Parent, const QVariantList& Arguments)
    : PropertyActionService(Parent, Arguments)
{
}

OpenEmailFolderService::~OpenEmailFolderService() = default;

void OpenEmailFolderService::execute(const KContacts::Addressee& Person, int ItemIndex)
{
    const QString FolderName = PropertyParser::folderName(Person, ItemIndex);
    if (FolderName.isEmpty()) {
        return;
    }

    QDBusInterface runningKMail("org.kde.kmail2", "/kmail2/kmail_mainwindow_1", "org.kde.KMainWindow");
    if (runningKMail.isValid()) {
        QDBusReply<int> reply = runningKMail.call("windId");
        const int winId = reply.value();
        const int desktopId = KWindowInfo(winId, NET::WMDesktop).desktop();
        KWindowSystem::setCurrentDesktop(desktopId);
        KWindowSystem::forceActiveWindow(winId);
    } else {
        KToolInvocation::startServiceByDesktopName("org.kde.kmail2");
    }

    QDBusInterface kmail("org.kde.kmail2", "/KMail", "org.kde.kmail.kmail");
    kmail.call("selectFolder", FolderName);
}

ServiceAction OpenEmailFolderService::action(const KContacts::Addressee& Person, int ItemIndex, int Flags) const
{
    const QString Address = Flags & ReferItem ? Person.emails()[ItemIndex] : QString();
    return new OpenEmailFolderServiceActionAdapter(Address);
}

bool OpenEmailFolderService::supports(const KContacts::Addressee& Person, int ItemIndex) const
{
    bool IsSupported = false;

    int MaxItemIndex;
    if (ItemIndex == -1) {
        ItemIndex = 0;
        MaxItemIndex = adapter()->numberOfItems(Person);
    } else {
        MaxItemIndex = ItemIndex + 1;
    }

    for (; ItemIndex < MaxItemIndex; ++ItemIndex) {
        if (!PropertyParser::folderName(Person, ItemIndex).isEmpty()) {
            IsSupported = true;
            break;
        }
    }

    return IsSupported;
}

K_PLUGIN_FACTORY(OpenEmailFolderServiceFactory, registerPlugin<OpenEmailFolderService>(); )

#include "openemailfolderservice.moc"
