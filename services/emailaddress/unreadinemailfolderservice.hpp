/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef UNREADINEMAILFOLDERSERVICE_HPP
#define UNREADINEMAILFOLDERSERVICE_HPP

// Kontinga Core
#include <Kontinga/PropertyStatusService>
#include <Kontinga/PropertyStatusServiceClientForItemList>
// KF
#include <dcopobject.h>
// Qt
#include <QVector>

class QTimer;
using namespace Kontinga;

class UnreadInEmailFolderServiceClientForItem : public PropertyStatusServiceClientForItem
{
public:
    QVector<int> unreadEmails;

public:
    UnreadInEmailFolderServiceClientForItem() = default;
    UnreadInEmailFolderServiceClientForItem(PropertyStatusServiceClient* client, int itemIndex, const QVector<int>& unreadEmails)
        : PropertyStatusServiceClientForItem(client, itemIndex)
        , unreadEmails(unreadEmails)
    {}
    UnreadInEmailFolderServiceClientForItem(PropertyStatusServiceClient* client, int itemIndex)
        : PropertyStatusServiceClientForItem(client, itemIndex)
    {}
    bool operator==(const UnreadInEmailFolderServiceClientForItem& other) const
    { return PropertyStatusServiceClientForItem::operator==(other); }
};

using UnreadInEmailFolderServiceClientForItemList = QVector<UnreadInEmailFolderServiceClientForItem>;

/** With KMail of KDE 3.5.3 we get informed about new incoming messages, but not unread becoming read
 * So we have to pull for those folders which have unread emails.
 */
class UnreadInEmailFolderService : public PropertyStatusService
    , public DCOPObject
{
    Q_OBJECT
    K_DCOP

public:
    UnreadInEmailFolderService(QObject* Parent, const char* Name, const QStringList& Arguments);
    ~UnreadInEmailFolderService() override;

public: // PropertyStatusService API
    Status status(const KContacts::Addressee& Person, int ItemIndex, int Flags) const override;

    void registerClient(PropertyStatusServiceClient* Client, int ItemIndex = -1) override;
    void unregisterClient(PropertyStatusServiceClient* Client, int ItemIndex = -1) override;

    bool supports(const KContacts::Addressee& Person, int ItemIndex) const override;

k_dcop:
    void onUnreadCountChanged();

protected Q_SLOTS:
    void onUpdateTimer();

protected:
    UnreadInEmailFolderServiceClientForItemList Clients;

    /** true if there are any unread known */
    bool AnyUnread;
    int TimeOfLastMessageCountUpdate;
    QTimer* UpdateTimer;
};

#endif
