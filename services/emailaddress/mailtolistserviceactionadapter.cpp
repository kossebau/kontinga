/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "mailtolistserviceactionadapter.hpp"

// plugin
#include <Kontinga/Roles>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
// Qt
#include <QIcon>

QVariant MailToListServiceActionAdapter::data(int Role) const
{
    QVariant Result;

    switch (Role)
    {
    case DisplayTextRole:
        Result = (NoOfEmails == NoOfAll) ?
                 i18n("Send E-mail...") :
                 i18n("Send E-mail (to %1 of %2)...", NoOfEmails, NoOfAll);
        break;
    case DisplayIconRole:
        Result = QIcon("mail_send");
        break;
    case EnabledRole:
        Result = true;
        break;
    default:
        ;
    }

    return Result;
}
