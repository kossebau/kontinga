/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "unreadinemailfolderservicestatusadapter.hpp"

// Kontinga Core
#include <Kontinga/Roles>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
// Qt
#include <QIcon>

QVariant UnreadInEmailFolderServiceStatusAdapter::data(int Role) const
{
    QVariant Result;

    switch (Role)
    {
    case DisplayTextRole:
        Result = Address.isEmpty() ?
                 (NumberOfUnread == 0 ?
                  i18n("No unread E-mail.") :
                  i18np("1 unread E-mail.", "%1 unread E-mails.", NumberOfUnread)) :
                 (NumberOfUnread == 0 ?
                  i18n("No unread E-mail from %1.", Address) :
                  i18np("1 unread E-mail from %2.", "%1 unread E-mails from %2.", NumberOfUnread, Address));
        break;
    case DisplayIconRole:
        if (NumberOfUnread > 0) {
            Result = QIcon("email");
        }
        break;
    case EnabledRole:
        Result = NumberOfUnread != -1;
        break;
    default:
        ;
    }

    return Result;
}
