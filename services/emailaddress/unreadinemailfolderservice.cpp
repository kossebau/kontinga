/*
    This file is part of the KDE project.
    Copyright (C) 2006-2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "unreadinemailfolderservice.hpp"

// status
#include "addresstokmailfoldermapparser.hpp"
#include "unreadinemailfolderchangestatusadapter.hpp"
#include "unreadinemailfolderservicestatusadapter.hpp"
// Kontinga Core
#include <Kontinga/PropertyAdapter>
#include <Kontinga/StatusServiceClient>
#include <Kontinga/StatusChange>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QTimer>
// Std
#include <ctime>

static const int UpdateMilliSeconds = 5 * 1000;

TODO: needs porting to talk to akonadi instead

UnreadInEmailFolderService::UnreadInEmailFolderService(QObject* Parent, const char* Name, const QStringList& Arguments)
    : PropertyStatusService(Parent, Name, Arguments)
    , DCOPObject(QCString("UnreadInEmailFolderService"))
    , TimeOfLastMessageCountUpdate(0)

{
    connectDCOPSignal(0, 0, "unreadCountChanged()", "onUnreadCountChanged()",
                      false);
    UpdateTimer = new QTimer(this);
    connect(UpdateTimer, SIGNAL(timeout()), SLOT(onUpdateTimer()));
    UpdateTimer->start(UpdateMilliSeconds);
}

UnreadInEmailFolderService::~UnreadInEmailFolderService() = default;

Status UnreadInEmailFolderService::status(const KContacts::Addressee& Person, int ItemIndex, int Flags) const
{
    const QString FolderName = PropertyParser::folderName(Person, ItemIndex);
// TODO: care for running state of kmail, but never start ourself
//    if( !kapp->dcopClient()->isApplicationRegistered("kmail") )
//        KApplication::startServiceByDesktopName( "kmail" );

    DCOPRef KMail("kmail", "KMailIface");

    DCOPRef FolderRef = KMail.call("getFolder(QString)", FolderName);
    int NumberOfUnread = FolderRef.call("unreadMessages");

    const QString Address = Flags & ReferItem ? Person.emails()[ItemIndex] : QString();
    return new UnreadInEmailFolderServiceStatusAdapter(NumberOfUnread, Address);
}

void UnreadInEmailFolderService::registerClient(PropertyStatusServiceClient* Client, int ItemIndex)
{
    const KContacts::Addressee& Person = Client->person();
    if (supports(Person, ItemIndex)) {
        int NumberOfItems = 1;
//         if( ItemIndex == -1 ) TODO: right now we create a vector for all, even for clients for only one item
        NumberOfItems = Adapter->numberOfItems(Person);
        QValueVector<int> NumberOfUnreadEmails(NumberOfItems);

        DCOPRef KMail("kmail", "KMailIface");
        int MaxItemIndex;
        int i = ItemIndex;
        if (i == -1) {
            i = 0;
            MaxItemIndex = Adapter->numberOfItems(Person);
        } else {
            MaxItemIndex = i + 1;
        }

        for (; i < MaxItemIndex; ++i) {
            // get current number in folder
            const QString FolderName = PropertyParser::folderName(Person, i);
            DCOPRef FolderRef = KMail.call("getFolder(QString)", FolderName);
            NumberOfUnreadEmails[i] = FolderRef.call("unreadMessages");
        }

        Clients.append(UnreadInEmailFolderServiceClientForItem(Client, ItemIndex, NumberOfUnreadEmails));
    }
}

void UnreadInEmailFolderService::unregisterClient(PropertyStatusServiceClient* Client, int ItemIndex)
{
    Clients.remove(UnreadInEmailFolderServiceClientForItem(Client, ItemIndex));
}

void UnreadInEmailFolderService::onUnreadCountChanged()
{
    TimeOfLastMessageCountUpdate = ::time(0);

    DCOPRef KMail("kmail", "KMailIface");

    for (UnreadInEmailFolderServiceClientForItemList::Iterator ClientIt = Clients.begin();
         ClientIt != Clients.end(); ++ClientIt) {
        const KContacts::Addressee& Person = (*ClientIt).Client->person();
        int MaxItemIndex;
        int ItemIndex = (*ClientIt).ItemIndex;
        if (ItemIndex == -1) {
            ItemIndex = 0;
            MaxItemIndex = Adapter->numberOfItems(Person);
        } else {
            MaxItemIndex = ItemIndex + 1;
        }

        // use address in reports only if there is more than one registered per person
        const bool UseAddress = (MaxItemIndex != ItemIndex + 1);

        for (; ItemIndex < MaxItemIndex; ++ItemIndex) {
            // get current number in folder
            const QString FolderName = PropertyParser::folderName(Person, ItemIndex);
            DCOPRef FolderRef = KMail.call("getFolder(QString)", FolderName);
            const int NumberOfUnread = FolderRef.call("unreadMessages");

            const int NumberOfNewUnread = NumberOfUnread - (*ClientIt).unreadEmails[ItemIndex];

            if (NumberOfNewUnread != 0) {
                (*ClientIt).unreadEmails[ItemIndex] = NumberOfUnread;
                const QString Address = UseAddress ? Person.emails()[ItemIndex] : QString();

                StatusAdapter* ChangeAdapter = (NumberOfNewUnread > 0) ?
                                               new UnreadInEmailFolderChangeStatusAdapter(NumberOfNewUnread, Address) :
                                               new StatusAdapter();
                StatusAdapter* StatusAdapter =
                    new UnreadInEmailFolderServiceStatusAdapter(NumberOfUnread, Address);
                (*ClientIt).Client->onStateChange(*this, ChangeAdapter, StatusAdapter, ItemIndex);
            }
        }
    }

    UpdateTimer->start(UpdateMilliSeconds);
}

void UnreadInEmailFolderService::onUpdateTimer()
{
    int TimeOfLastMessageCountChange = DCOPRef("kmail", "KMailIface").call("timeOfLastMessageCountChange()");
// kdDebug()<<"onUpdateTimer:"<<TimeOfLastMessageCountChange<<" for "<<TimeOfLastMessageCountUpdate<<endl;
    if (TimeOfLastMessageCountChange > TimeOfLastMessageCountUpdate) {
        onUnreadCountChanged();
    }
}

bool UnreadInEmailFolderService::supports(const KContacts::Addressee& Person, int ItemIndex) const
{
    bool IsSupported = false;

    int MaxItemIndex;
    if (ItemIndex == -1) {
        ItemIndex = 0;
        MaxItemIndex = Adapter->numberOfItems(Person);
    } else {
        MaxItemIndex = ItemIndex + 1;
    }

    for (; ItemIndex < MaxItemIndex; ++ItemIndex) {

        if (!PropertyParser::folderName(Person, ItemIndex).isEmpty()) {
            IsSupported = true;
            break;
        }
    }

    return IsSupported;
}

K_PLUGIN_FACTORY(UnreadInEmailFolderServiceFactory, registerPlugin<UnreadInEmailFolderService>(); )

#include "unreadinemailfolderservice.moc"
