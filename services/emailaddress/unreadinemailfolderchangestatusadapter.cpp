/*
    This file is part of the KDE project.
    Copyright (C) 2007 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "unreadinemailfolderchangestatusadapter.hpp"

// Kontinga Core
#include <Kontinga/Roles>
// KDEPIM
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
// Qt
#include <QIcon>

QVariant UnreadInEmailFolderChangeStatusAdapter::data(int Role) const
{
    QVariant Result;

    switch (Role)
    {
    case IdRole:
        Result = QString::fromLatin1("new_mail_arrived");
        break;
    case DisplayTextRole:
        Result = Address.isEmpty() ?
                 i18np("1 new E-mail.", "%1 new E-mails.", NumberOfNewUnread) :
                 i18np("1 new E-mail from %2.", "%1 new E-mails from %2.", NumberOfNewUnread, Address);
        break;
    case DisplayIconRole:
        Result = QIcon("email");
        break;
    case EnabledRole:
        Result = true;
    default:
        ;
    }

    return Result;
}
