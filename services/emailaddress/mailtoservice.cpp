/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "mailtoservice.hpp"

// service
#include "mailtoserviceactionadapter.hpp"
#include "mailtolistserviceactionadapter.hpp"
// KDEPIM
#include <KContacts/AddresseeList>
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
// Qt
#include <QDesktopServices>
#include <QUrlQuery>
#include <QUrl>

static void invokeMailer(const QStringList& emailAddresses)
{
    QUrl url;
    url.setScheme(QStringLiteral("mailto"));

    QUrlQuery query;
    query.addQueryItem(QStringLiteral("to"), emailAddresses.join(','));

    url.setQuery(query);

    QDesktopServices::openUrl(url);
}

MailToService::MailToService(QObject* Parent, const QVariantList& Arguments)
    : ListPropertyActionService(Parent, Arguments)
{
}

MailToService::~MailToService() = default;

ServiceAction MailToService::action(const KContacts::Addressee& Person, int ItemIndex, int Flags) const
{
    return new MailToServiceActionAdapter(Flags & ReferItem ? Person.emails()[ItemIndex] : QString());
}

// TODO: is flags needed?
ServiceAction MailToService::action(const KContacts::AddresseeList& PersonList, int /*Flags*/) const
{
    int NoOfEmails = 0;
    for (KContacts::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it) {
        if ((*it).emails().size() > 0) {
            ++NoOfEmails;
        }
    }

    return new MailToListServiceActionAdapter(NoOfEmails, PersonList.size());
}

void MailToService::execute(const KContacts::Addressee& Person, int ItemIndex)
{
    invokeMailer(QStringList(Person.fullEmail(Person.emails()[ItemIndex])));
}

void MailToService::execute(const KContacts::AddresseeList& PersonList)
{
    QStringList emailAddresses;
    for (const auto& person : PersonList) {
        const QString preferredEmail = person.preferredEmail();
        if (!preferredEmail.isEmpty()) {
            emailAddresses.append(person.fullEmail(preferredEmail));
        }
    }

    invokeMailer(emailAddresses);
}

K_PLUGIN_FACTORY(MailToServiceFactory, registerPlugin<MailToService>(); )

#include "mailtoservice.moc"
