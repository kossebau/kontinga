/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#include "mailtodropservice.hpp"

// service
#include "mailtodropserviceactionadapter.hpp"
#include "mailtolistdropserviceactionadapter.hpp"
// KDEPIM
#include <KContacts/AddresseeList>
#include <KContacts/Addressee>
// KF
#include <KLocalizedString>
#include <KPluginFactory>
#include <KZip>
// Qt
#include <QDesktopServices>
#include <QUrlQuery>
#include <QUrl>
#include <QMimeData>
#include <QTemporaryDir>
#include <QFileInfo>

static void invokeMailer(const QStringList& emailAddresses, const QString& subject, const QStringList& attachUrls)
{
    QUrl url;
    url.setScheme(QStringLiteral("mailto"));

    QUrlQuery query;
    query.addQueryItem(QStringLiteral("to"), emailAddresses.join(','));
    query.addQueryItem(QStringLiteral("subject"), subject);
    for (const QString& attachURL : attachUrls) {
        query.addQueryItem(QStringLiteral("attach"), attachURL);
    }

    url.setQuery(query);

    QDesktopServices::openUrl(url);
}

MailToDataActionService::MailToDataActionService(QObject* Parent, const QVariantList& Arguments)
    : ListPropertyDataActionService(Parent, Arguments)
{
}

MailToDataActionService::~MailToDataActionService() = default;

ServiceAction MailToDataActionService::action(const KContacts::Addressee& Person, int ItemIndex,
                                              const QMimeData* data, int Flags) const
{
    const int urlCount = data->urls().size();
    const QString address = Flags & ReferItem ? Person.emails()[ItemIndex] : QString();

    return new MailToDataActionServiceActionAdapter(address, urlCount);
}

ServiceAction MailToDataActionService::action(const KContacts::AddresseeList& PersonList,
                                              const QMimeData* data, int /*Flags*/) const
{
    const int urlCount = data->urls().size();

    int NoOfEmails = 0;
    for (KContacts::AddresseeList::ConstIterator it = PersonList.begin(); it != PersonList.end(); ++it) {
        if (it->emails().size() > 0) {
            ++NoOfEmails;
        }
    }

    return new MailToListDataActionServiceActionAdapter(NoOfEmails, PersonList.size(), urlCount);
}

static void createFileNames(QStringList* URLNames, QString* FileNames, const QList<QUrl>& urls)
{
    for (const auto& url : urls) {
        if (!FileNames->isEmpty()) {
            *FileNames += ", ";
        }
        // zip directory if local
        if (url.isLocalFile() && QFileInfo(url.toLocalFile()).isDir()) {
            QTemporaryDir zipDir;
            zipDir.setAutoRemove(false); // TODO: care for cleaning up once job is done?

            const QString ZipFileName = zipDir.filePath(url.fileName() + ".zip");
            KZip Zipper(ZipFileName);
            if (!Zipper.open(QIODevice::WriteOnly)) {
                continue; // TODO: error message
            }
            Zipper.addLocalDirectory(url.path(), QString());
            Zipper.close();
            *FileNames += url.fileName() + ".zip";
            URLNames->append(ZipFileName);
        } else {
            *FileNames += url.fileName();
            URLNames->append(url.url());
        }
    }
}

void MailToDataActionService::execute(const KContacts::Addressee& Person, int ItemIndex, const QMimeData* data)
{
    const QList<QUrl> urls = data->urls();

    if (!urls.isEmpty()) {
        QStringList URLNames;
        QString FileNames;
        createFileNames(&URLNames, &FileNames, urls);

        invokeMailer(QStringList(Person.fullEmail(Person.emails()[ItemIndex])), FileNames, URLNames);
    }
}
void MailToDataActionService::execute(const KContacts::AddresseeList& PersonList, const QMimeData* data)
{
    const QList<QUrl> urls = data->urls();

    if (!urls.isEmpty()) {
        QStringList URLNames;
        QString FileNames;
        createFileNames(&URLNames, &FileNames, urls);

        QStringList emailAddresses;
        for (const auto& person : PersonList) {
            const QString preferredEmail = person.preferredEmail();
            if (!preferredEmail.isEmpty()) {
                emailAddresses.append(person.fullEmail(preferredEmail));
            }
        }

        invokeMailer(emailAddresses, FileNames, URLNames);
    }
}

bool MailToDataActionService::supports(const QMimeData* data) const
{
    return data->hasUrls();
}

K_PLUGIN_FACTORY(MailToDataActionServiceFactory, registerPlugin<MailToDataActionService>(); )

#include "mailtodropservice.moc"
