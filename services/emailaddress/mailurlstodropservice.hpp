/*
    This file is part of the KDE project.
    Copyright (c) 2006 Friedrich W. H. Kossebau <kossebau@kde.org>

    This library is free software; you can redistribute it and/or
    modify it under the terms of the GNU Library General Public
    License version 2 as published by the Free Software Foundation.

    This library is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
    Library General Public License for more details.

    You should have received a copy of the GNU Library General Public License
    along with this library; see the file COPYING.LIB.  If not, write to
    the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
    Boston, MA 02110-1301  USA
 */

#ifndef MAILURLSTODROPSERVICE_HPP
#define MAILURLSTODROPSERVICE_HPP

// Kontinga Core
#include <Kontinga/ListPropertyDataActionService>

using namespace Kontinga;

class MailURLsToDataActionService : public ListPropertyDataActionService
{
public:
    MailURLsToDataActionService(QObject* Parent, const QVariantList& Arguments);
    ~MailURLsToDataActionService() override;

public: // KontingaPropertyDataActionService API
    ServiceAction action(const KContacts::Addressee& Person, int ItemIndex,
                         const QMimeData* data, int Flags) const override;
    ServiceAction action(const KContacts::AddresseeList& PersonList,
                         const QMimeData* data, int Flags) const override;

    void execute(const KContacts::Addressee& Person, int ItemIndex, const QMimeData* data) override;
    void execute(const KContacts::AddresseeList& PersonList, const QMimeData* data) override;

    bool supports(const QMimeData* data) const override;
    using ListPropertyDataActionService::supports;
    using PropertyDataActionService::supports;
};

#endif
